﻿namespace Emlak
{
    partial class Form_GirisEkrani
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_GirisEkrani));
            this.Kutu_UyeEposta = new System.Windows.Forms.TextBox();
            this.Buton_Giris = new System.Windows.Forms.Button();
            this.girisSekmeleri = new System.Windows.Forms.TabControl();
            this.Sekme_uyeGirisi = new System.Windows.Forms.TabPage();
            this.Buton_ProfilKurtar = new System.Windows.Forms.Button();
            this.Buton_UyeOlSekmesi = new System.Windows.Forms.Button();
            this.Kutu_BaglantiYazisi = new System.Windows.Forms.Label();
            this.Kutu_BaglantiOnayResmi = new System.Windows.Forms.PictureBox();
            this.Buton_OtoGirisDoldur = new System.Windows.Forms.Button();
            this.Kutu_SifreUyariBilgiResmi = new System.Windows.Forms.PictureBox();
            this.Kutu_EpostaUyariBilgiResmi = new System.Windows.Forms.PictureBox();
            this.Kutu_SifreUyari = new System.Windows.Forms.Label();
            this.Kutu_EpostaUyari = new System.Windows.Forms.Label();
            this.Kutu_IsimSifreGrubu = new System.Windows.Forms.GroupBox();
            this.Kutu_SifreUyariResmi = new System.Windows.Forms.PictureBox();
            this.Buton_UyeSifreGorunurluk = new System.Windows.Forms.Button();
            this.Kutu_EpostaUyariResmi = new System.Windows.Forms.PictureBox();
            this.Kutu_UyeSifre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Sekme_uyelik = new System.Windows.Forms.TabPage();
            this.Kutu_KayitIlceOnayResmi = new System.Windows.Forms.PictureBox();
            this.ListeKutu_Ilce = new System.Windows.Forms.ComboBox();
            this.Tik_Admin = new System.Windows.Forms.CheckBox();
            this.ListeKutu_Il = new System.Windows.Forms.ComboBox();
            this.Buton_UyeKontrol = new System.Windows.Forms.Button();
            this.Kutu_KayitResmiOnayResmi = new System.Windows.Forms.PictureBox();
            this.Kutu_KayitResmi = new System.Windows.Forms.PictureBox();
            this.Buton_KayitResmiSec = new System.Windows.Forms.Button();
            this.Buton_GirisSekmesi = new System.Windows.Forms.Button();
            this.Buton_OtoKayitDoldur = new System.Windows.Forms.Button();
            this.Kutu_KayitIlOnayResmi = new System.Windows.Forms.PictureBox();
            this.Kutu_KayitSoyadOnayResmi = new System.Windows.Forms.PictureBox();
            this.Kutu_KayitAdOnayResmi = new System.Windows.Forms.PictureBox();
            this.Kutu_KayitSifreOnayResmi = new System.Windows.Forms.PictureBox();
            this.Kutu_KayitEpostaOnayResmi = new System.Windows.Forms.PictureBox();
            this.Kutu_KayitEpostaResmi = new System.Windows.Forms.PictureBox();
            this.Kutu_KayitUyarisi = new System.Windows.Forms.Label();
            this.Kutu_KayitSoyad = new System.Windows.Forms.TextBox();
            this.Kutu_KayitAd = new System.Windows.Forms.TextBox();
            this.Buton_KayitSifreGorunurluk = new System.Windows.Forms.Button();
            this.Kutu_KayitSifre = new System.Windows.Forms.TextBox();
            this.Buton_UyeOl = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.Kutu_KayitEposta = new System.Windows.Forms.TextBox();
            this.Ipucu_KayitEposta = new System.Windows.Forms.ToolTip(this.components);
            this.girisSekmeleri.SuspendLayout();
            this.Sekme_uyeGirisi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_BaglantiOnayResmi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_SifreUyariBilgiResmi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_EpostaUyariBilgiResmi)).BeginInit();
            this.Kutu_IsimSifreGrubu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_SifreUyariResmi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_EpostaUyariResmi)).BeginInit();
            this.Sekme_uyelik.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitIlceOnayResmi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitResmiOnayResmi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitResmi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitIlOnayResmi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitSoyadOnayResmi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitAdOnayResmi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitSifreOnayResmi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitEpostaOnayResmi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitEpostaResmi)).BeginInit();
            this.SuspendLayout();
            // 
            // Kutu_UyeEposta
            // 
            this.Kutu_UyeEposta.Location = new System.Drawing.Point(7, 25);
            this.Kutu_UyeEposta.Margin = new System.Windows.Forms.Padding(4);
            this.Kutu_UyeEposta.Name = "Kutu_UyeEposta";
            this.Kutu_UyeEposta.Size = new System.Drawing.Size(159, 25);
            this.Kutu_UyeEposta.TabIndex = 0;
            this.Kutu_UyeEposta.Text = "E-posta: uye@email.com";
            this.Kutu_UyeEposta.TextChanged += new System.EventHandler(this.Kutu_UyeEposta_TextChanged);
            this.Kutu_UyeEposta.Enter += new System.EventHandler(this.Kutu_UyeEposta_Enter);
            this.Kutu_UyeEposta.Leave += new System.EventHandler(this.Kutu_UyeEposta_Leave);
            // 
            // Buton_Giris
            // 
            this.Buton_Giris.Enabled = false;
            this.Buton_Giris.Location = new System.Drawing.Point(59, 171);
            this.Buton_Giris.Margin = new System.Windows.Forms.Padding(4);
            this.Buton_Giris.Name = "Buton_Giris";
            this.Buton_Giris.Size = new System.Drawing.Size(130, 30);
            this.Buton_Giris.TabIndex = 2;
            this.Buton_Giris.Text = "Giriş Yap";
            this.Buton_Giris.UseVisualStyleBackColor = true;
            this.Buton_Giris.Click += new System.EventHandler(this.Buton_Giris_Click);
            // 
            // girisSekmeleri
            // 
            this.girisSekmeleri.Controls.Add(this.Sekme_uyeGirisi);
            this.girisSekmeleri.Controls.Add(this.Sekme_uyelik);
            this.girisSekmeleri.Location = new System.Drawing.Point(-6, -28);
            this.girisSekmeleri.Name = "girisSekmeleri";
            this.girisSekmeleri.SelectedIndex = 0;
            this.girisSekmeleri.Size = new System.Drawing.Size(333, 453);
            this.girisSekmeleri.TabIndex = 3;
            // 
            // Sekme_uyeGirisi
            // 
            this.Sekme_uyeGirisi.Controls.Add(this.Buton_ProfilKurtar);
            this.Sekme_uyeGirisi.Controls.Add(this.Buton_UyeOlSekmesi);
            this.Sekme_uyeGirisi.Controls.Add(this.Kutu_BaglantiYazisi);
            this.Sekme_uyeGirisi.Controls.Add(this.Kutu_BaglantiOnayResmi);
            this.Sekme_uyeGirisi.Controls.Add(this.Buton_OtoGirisDoldur);
            this.Sekme_uyeGirisi.Controls.Add(this.Kutu_SifreUyariBilgiResmi);
            this.Sekme_uyeGirisi.Controls.Add(this.Kutu_EpostaUyariBilgiResmi);
            this.Sekme_uyeGirisi.Controls.Add(this.Kutu_SifreUyari);
            this.Sekme_uyeGirisi.Controls.Add(this.Kutu_EpostaUyari);
            this.Sekme_uyeGirisi.Controls.Add(this.Kutu_IsimSifreGrubu);
            this.Sekme_uyeGirisi.Controls.Add(this.Buton_Giris);
            this.Sekme_uyeGirisi.Controls.Add(this.label1);
            this.Sekme_uyeGirisi.Location = new System.Drawing.Point(4, 26);
            this.Sekme_uyeGirisi.Name = "Sekme_uyeGirisi";
            this.Sekme_uyeGirisi.Padding = new System.Windows.Forms.Padding(3);
            this.Sekme_uyeGirisi.Size = new System.Drawing.Size(325, 423);
            this.Sekme_uyeGirisi.TabIndex = 0;
            this.Sekme_uyeGirisi.Text = "Üye Girişi";
            this.Sekme_uyeGirisi.UseVisualStyleBackColor = true;
            // 
            // Buton_ProfilKurtar
            // 
            this.Buton_ProfilKurtar.Location = new System.Drawing.Point(214, 16);
            this.Buton_ProfilKurtar.Margin = new System.Windows.Forms.Padding(4);
            this.Buton_ProfilKurtar.Name = "Buton_ProfilKurtar";
            this.Buton_ProfilKurtar.Size = new System.Drawing.Size(104, 33);
            this.Buton_ProfilKurtar.TabIndex = 26;
            this.Buton_ProfilKurtar.Text = "Profil Kurtar";
            this.Buton_ProfilKurtar.UseVisualStyleBackColor = true;
            this.Buton_ProfilKurtar.Click += new System.EventHandler(this.Buton_ProfilKurtar_Click);
            // 
            // Buton_UyeOlSekmesi
            // 
            this.Buton_UyeOlSekmesi.Enabled = false;
            this.Buton_UyeOlSekmesi.Location = new System.Drawing.Point(197, 171);
            this.Buton_UyeOlSekmesi.Margin = new System.Windows.Forms.Padding(4);
            this.Buton_UyeOlSekmesi.Name = "Buton_UyeOlSekmesi";
            this.Buton_UyeOlSekmesi.Size = new System.Drawing.Size(70, 30);
            this.Buton_UyeOlSekmesi.TabIndex = 23;
            this.Buton_UyeOlSekmesi.Text = "Üye Ol";
            this.Buton_UyeOlSekmesi.UseVisualStyleBackColor = true;
            this.Buton_UyeOlSekmesi.Click += new System.EventHandler(this.Buton_UyeOlSekmesi_Click);
            // 
            // Kutu_BaglantiYazisi
            // 
            this.Kutu_BaglantiYazisi.Font = new System.Drawing.Font("Nirmala UI", 12F, System.Drawing.FontStyle.Bold);
            this.Kutu_BaglantiYazisi.Location = new System.Drawing.Point(97, 395);
            this.Kutu_BaglantiYazisi.Name = "Kutu_BaglantiYazisi";
            this.Kutu_BaglantiYazisi.Size = new System.Drawing.Size(153, 25);
            this.Kutu_BaglantiYazisi.TabIndex = 22;
            this.Kutu_BaglantiYazisi.Text = "Bağlantı Kuruldu";
            this.Kutu_BaglantiYazisi.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Kutu_BaglantiOnayResmi
            // 
            this.Kutu_BaglantiOnayResmi.Cursor = System.Windows.Forms.Cursors.Default;
            this.Kutu_BaglantiOnayResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_BaglantiOnayResmi.Image")));
            this.Kutu_BaglantiOnayResmi.Location = new System.Drawing.Point(63, 395);
            this.Kutu_BaglantiOnayResmi.Name = "Kutu_BaglantiOnayResmi";
            this.Kutu_BaglantiOnayResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_BaglantiOnayResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_BaglantiOnayResmi.TabIndex = 21;
            this.Kutu_BaglantiOnayResmi.TabStop = false;
            this.Kutu_BaglantiOnayResmi.Tag = "";
            // 
            // Buton_OtoGirisDoldur
            // 
            this.Buton_OtoGirisDoldur.FlatAppearance.BorderSize = 0;
            this.Buton_OtoGirisDoldur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_OtoGirisDoldur.Location = new System.Drawing.Point(297, 0);
            this.Buton_OtoGirisDoldur.Margin = new System.Windows.Forms.Padding(4);
            this.Buton_OtoGirisDoldur.Name = "Buton_OtoGirisDoldur";
            this.Buton_OtoGirisDoldur.Size = new System.Drawing.Size(28, 25);
            this.Buton_OtoGirisDoldur.TabIndex = 20;
            this.Buton_OtoGirisDoldur.UseVisualStyleBackColor = true;
            this.Buton_OtoGirisDoldur.Click += new System.EventHandler(this.Buton_OtoGirisDoldur_Click);
            // 
            // Kutu_SifreUyariBilgiResmi
            // 
            this.Kutu_SifreUyariBilgiResmi.Cursor = System.Windows.Forms.Cursors.Cross;
            this.Kutu_SifreUyariBilgiResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_SifreUyariBilgiResmi.Image")));
            this.Kutu_SifreUyariBilgiResmi.Location = new System.Drawing.Point(22, 299);
            this.Kutu_SifreUyariBilgiResmi.Name = "Kutu_SifreUyariBilgiResmi";
            this.Kutu_SifreUyariBilgiResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_SifreUyariBilgiResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_SifreUyariBilgiResmi.TabIndex = 11;
            this.Kutu_SifreUyariBilgiResmi.TabStop = false;
            this.Kutu_SifreUyariBilgiResmi.Visible = false;
            // 
            // Kutu_EpostaUyariBilgiResmi
            // 
            this.Kutu_EpostaUyariBilgiResmi.Cursor = System.Windows.Forms.Cursors.Cross;
            this.Kutu_EpostaUyariBilgiResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_EpostaUyariBilgiResmi.Image")));
            this.Kutu_EpostaUyariBilgiResmi.Location = new System.Drawing.Point(22, 205);
            this.Kutu_EpostaUyariBilgiResmi.Name = "Kutu_EpostaUyariBilgiResmi";
            this.Kutu_EpostaUyariBilgiResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_EpostaUyariBilgiResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_EpostaUyariBilgiResmi.TabIndex = 7;
            this.Kutu_EpostaUyariBilgiResmi.TabStop = false;
            this.Kutu_EpostaUyariBilgiResmi.Visible = false;
            // 
            // Kutu_SifreUyari
            // 
            this.Kutu_SifreUyari.ForeColor = System.Drawing.Color.Black;
            this.Kutu_SifreUyari.Location = new System.Drawing.Point(56, 299);
            this.Kutu_SifreUyari.Name = "Kutu_SifreUyari";
            this.Kutu_SifreUyari.Size = new System.Drawing.Size(210, 94);
            this.Kutu_SifreUyari.TabIndex = 10;
            // 
            // Kutu_EpostaUyari
            // 
            this.Kutu_EpostaUyari.Location = new System.Drawing.Point(56, 205);
            this.Kutu_EpostaUyari.Name = "Kutu_EpostaUyari";
            this.Kutu_EpostaUyari.Size = new System.Drawing.Size(210, 94);
            this.Kutu_EpostaUyari.TabIndex = 9;
            // 
            // Kutu_IsimSifreGrubu
            // 
            this.Kutu_IsimSifreGrubu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Kutu_IsimSifreGrubu.Controls.Add(this.Kutu_UyeEposta);
            this.Kutu_IsimSifreGrubu.Controls.Add(this.Kutu_SifreUyariResmi);
            this.Kutu_IsimSifreGrubu.Controls.Add(this.Buton_UyeSifreGorunurluk);
            this.Kutu_IsimSifreGrubu.Controls.Add(this.Kutu_EpostaUyariResmi);
            this.Kutu_IsimSifreGrubu.Controls.Add(this.Kutu_UyeSifre);
            this.Kutu_IsimSifreGrubu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Kutu_IsimSifreGrubu.Location = new System.Drawing.Point(59, 47);
            this.Kutu_IsimSifreGrubu.Name = "Kutu_IsimSifreGrubu";
            this.Kutu_IsimSifreGrubu.Size = new System.Drawing.Size(207, 100);
            this.Kutu_IsimSifreGrubu.TabIndex = 8;
            this.Kutu_IsimSifreGrubu.TabStop = false;
            // 
            // Kutu_SifreUyariResmi
            // 
            this.Kutu_SifreUyariResmi.Cursor = System.Windows.Forms.Cursors.Cross;
            this.Kutu_SifreUyariResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_SifreUyariResmi.Image")));
            this.Kutu_SifreUyariResmi.Location = new System.Drawing.Point(173, 56);
            this.Kutu_SifreUyariResmi.Name = "Kutu_SifreUyariResmi";
            this.Kutu_SifreUyariResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_SifreUyariResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_SifreUyariResmi.TabIndex = 6;
            this.Kutu_SifreUyariResmi.TabStop = false;
            this.Kutu_SifreUyariResmi.Visible = false;
            // 
            // Buton_UyeSifreGorunurluk
            // 
            this.Buton_UyeSifreGorunurluk.Location = new System.Drawing.Point(138, 58);
            this.Buton_UyeSifreGorunurluk.Margin = new System.Windows.Forms.Padding(4);
            this.Buton_UyeSifreGorunurluk.Name = "Buton_UyeSifreGorunurluk";
            this.Buton_UyeSifreGorunurluk.Size = new System.Drawing.Size(28, 25);
            this.Buton_UyeSifreGorunurluk.TabIndex = 4;
            this.Buton_UyeSifreGorunurluk.Text = "G";
            this.Buton_UyeSifreGorunurluk.UseVisualStyleBackColor = true;
            this.Buton_UyeSifreGorunurluk.Click += new System.EventHandler(this.Buton_UyeSifreGorunurluk_Click);
            // 
            // Kutu_EpostaUyariResmi
            // 
            this.Kutu_EpostaUyariResmi.Cursor = System.Windows.Forms.Cursors.Cross;
            this.Kutu_EpostaUyariResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_EpostaUyariResmi.Image")));
            this.Kutu_EpostaUyariResmi.Location = new System.Drawing.Point(173, 25);
            this.Kutu_EpostaUyariResmi.Name = "Kutu_EpostaUyariResmi";
            this.Kutu_EpostaUyariResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_EpostaUyariResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_EpostaUyariResmi.TabIndex = 5;
            this.Kutu_EpostaUyariResmi.TabStop = false;
            this.Kutu_EpostaUyariResmi.Visible = false;
            // 
            // Kutu_UyeSifre
            // 
            this.Kutu_UyeSifre.Location = new System.Drawing.Point(7, 58);
            this.Kutu_UyeSifre.Margin = new System.Windows.Forms.Padding(4);
            this.Kutu_UyeSifre.Name = "Kutu_UyeSifre";
            this.Kutu_UyeSifre.Size = new System.Drawing.Size(132, 25);
            this.Kutu_UyeSifre.TabIndex = 3;
            this.Kutu_UyeSifre.Text = "Şifre";
            this.Kutu_UyeSifre.TextChanged += new System.EventHandler(this.Kutu_UyeSifre_TextChanged);
            this.Kutu_UyeSifre.Enter += new System.EventHandler(this.Kutu_UyeSifre_Enter);
            this.Kutu_UyeSifre.Leave += new System.EventHandler(this.Kutu_UyeSifre_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Nirmala UI", 16F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(95, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Üye Girişi";
            // 
            // Sekme_uyelik
            // 
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitIlceOnayResmi);
            this.Sekme_uyelik.Controls.Add(this.ListeKutu_Ilce);
            this.Sekme_uyelik.Controls.Add(this.Tik_Admin);
            this.Sekme_uyelik.Controls.Add(this.ListeKutu_Il);
            this.Sekme_uyelik.Controls.Add(this.Buton_UyeKontrol);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitResmiOnayResmi);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitResmi);
            this.Sekme_uyelik.Controls.Add(this.Buton_KayitResmiSec);
            this.Sekme_uyelik.Controls.Add(this.Buton_GirisSekmesi);
            this.Sekme_uyelik.Controls.Add(this.Buton_OtoKayitDoldur);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitIlOnayResmi);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitSoyadOnayResmi);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitAdOnayResmi);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitSifreOnayResmi);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitEpostaOnayResmi);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitEpostaResmi);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitUyarisi);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitSoyad);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitAd);
            this.Sekme_uyelik.Controls.Add(this.Buton_KayitSifreGorunurluk);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitSifre);
            this.Sekme_uyelik.Controls.Add(this.Buton_UyeOl);
            this.Sekme_uyelik.Controls.Add(this.label2);
            this.Sekme_uyelik.Controls.Add(this.Kutu_KayitEposta);
            this.Sekme_uyelik.Location = new System.Drawing.Point(4, 26);
            this.Sekme_uyelik.Name = "Sekme_uyelik";
            this.Sekme_uyelik.Padding = new System.Windows.Forms.Padding(3);
            this.Sekme_uyelik.Size = new System.Drawing.Size(325, 423);
            this.Sekme_uyelik.TabIndex = 1;
            this.Sekme_uyelik.Text = "Üyelik";
            this.Sekme_uyelik.UseVisualStyleBackColor = true;
            // 
            // Kutu_KayitIlceOnayResmi
            // 
            this.Kutu_KayitIlceOnayResmi.Cursor = System.Windows.Forms.Cursors.Default;
            this.Kutu_KayitIlceOnayResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_KayitIlceOnayResmi.Image")));
            this.Kutu_KayitIlceOnayResmi.Location = new System.Drawing.Point(242, 222);
            this.Kutu_KayitIlceOnayResmi.Name = "Kutu_KayitIlceOnayResmi";
            this.Kutu_KayitIlceOnayResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_KayitIlceOnayResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_KayitIlceOnayResmi.TabIndex = 28;
            this.Kutu_KayitIlceOnayResmi.TabStop = false;
            this.Kutu_KayitIlceOnayResmi.Tag = "";
            this.Kutu_KayitIlceOnayResmi.Visible = false;
            // 
            // ListeKutu_Ilce
            // 
            this.ListeKutu_Ilce.BackColor = System.Drawing.SystemColors.Window;
            this.ListeKutu_Ilce.FormattingEnabled = true;
            this.ListeKutu_Ilce.Location = new System.Drawing.Point(75, 223);
            this.ListeKutu_Ilce.Name = "ListeKutu_Ilce";
            this.ListeKutu_Ilce.Size = new System.Drawing.Size(160, 25);
            this.ListeKutu_Ilce.Sorted = true;
            this.ListeKutu_Ilce.TabIndex = 27;
            this.ListeKutu_Ilce.Tag = "İl/İlçe/Semt";
            this.ListeKutu_Ilce.SelectedIndexChanged += new System.EventHandler(this.ListeKutu_Ilce_SelectedIndexChanged);
            // 
            // Tik_Admin
            // 
            this.Tik_Admin.Appearance = System.Windows.Forms.Appearance.Button;
            this.Tik_Admin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Tik_Admin.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Tik_Admin.FlatAppearance.BorderSize = 0;
            this.Tik_Admin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Tik_Admin.Location = new System.Drawing.Point(15, 70);
            this.Tik_Admin.Name = "Tik_Admin";
            this.Tik_Admin.Size = new System.Drawing.Size(10, 10);
            this.Tik_Admin.TabIndex = 26;
            this.Tik_Admin.UseVisualStyleBackColor = true;
            this.Tik_Admin.CheckedChanged += new System.EventHandler(this.Tik_Admin_CheckedChanged);
            // 
            // ListeKutu_Il
            // 
            this.ListeKutu_Il.BackColor = System.Drawing.SystemColors.Window;
            this.ListeKutu_Il.FormattingEnabled = true;
            this.ListeKutu_Il.Location = new System.Drawing.Point(76, 192);
            this.ListeKutu_Il.Name = "ListeKutu_Il";
            this.ListeKutu_Il.Size = new System.Drawing.Size(160, 25);
            this.ListeKutu_Il.Sorted = true;
            this.ListeKutu_Il.TabIndex = 25;
            this.ListeKutu_Il.Tag = "İl/İlçe/Semt";
            this.ListeKutu_Il.SelectedIndexChanged += new System.EventHandler(this.ListeKutu_Il_SelectedIndexChanged);
            // 
            // Buton_UyeKontrol
            // 
            this.Buton_UyeKontrol.FlatAppearance.BorderSize = 0;
            this.Buton_UyeKontrol.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_UyeKontrol.Location = new System.Drawing.Point(0, 0);
            this.Buton_UyeKontrol.Margin = new System.Windows.Forms.Padding(4);
            this.Buton_UyeKontrol.Name = "Buton_UyeKontrol";
            this.Buton_UyeKontrol.Size = new System.Drawing.Size(28, 25);
            this.Buton_UyeKontrol.TabIndex = 24;
            this.Buton_UyeKontrol.UseVisualStyleBackColor = true;
            this.Buton_UyeKontrol.Click += new System.EventHandler(this.Buton_UyeKontrol_Click);
            // 
            // Kutu_KayitResmiOnayResmi
            // 
            this.Kutu_KayitResmiOnayResmi.Cursor = System.Windows.Forms.Cursors.Default;
            this.Kutu_KayitResmiOnayResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_KayitResmiOnayResmi.Image")));
            this.Kutu_KayitResmiOnayResmi.Location = new System.Drawing.Point(242, 280);
            this.Kutu_KayitResmiOnayResmi.Name = "Kutu_KayitResmiOnayResmi";
            this.Kutu_KayitResmiOnayResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_KayitResmiOnayResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_KayitResmiOnayResmi.TabIndex = 23;
            this.Kutu_KayitResmiOnayResmi.TabStop = false;
            this.Kutu_KayitResmiOnayResmi.Tag = "";
            this.Kutu_KayitResmiOnayResmi.Visible = false;
            // 
            // Kutu_KayitResmi
            // 
            this.Kutu_KayitResmi.Location = new System.Drawing.Point(75, 254);
            this.Kutu_KayitResmi.Name = "Kutu_KayitResmi";
            this.Kutu_KayitResmi.Size = new System.Drawing.Size(76, 76);
            this.Kutu_KayitResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_KayitResmi.TabIndex = 22;
            this.Kutu_KayitResmi.TabStop = false;
            // 
            // Buton_KayitResmiSec
            // 
            this.Buton_KayitResmiSec.Location = new System.Drawing.Point(162, 269);
            this.Buton_KayitResmiSec.Margin = new System.Windows.Forms.Padding(4);
            this.Buton_KayitResmiSec.Name = "Buton_KayitResmiSec";
            this.Buton_KayitResmiSec.Size = new System.Drawing.Size(72, 45);
            this.Buton_KayitResmiSec.TabIndex = 21;
            this.Buton_KayitResmiSec.Text = "Resim Yükle";
            this.Buton_KayitResmiSec.UseVisualStyleBackColor = true;
            this.Buton_KayitResmiSec.Click += new System.EventHandler(this.Buton_KayitResmiSec_Click);
            // 
            // Buton_GirisSekmesi
            // 
            this.Buton_GirisSekmesi.Location = new System.Drawing.Point(162, 337);
            this.Buton_GirisSekmesi.Margin = new System.Windows.Forms.Padding(4);
            this.Buton_GirisSekmesi.Name = "Buton_GirisSekmesi";
            this.Buton_GirisSekmesi.Size = new System.Drawing.Size(72, 30);
            this.Buton_GirisSekmesi.TabIndex = 20;
            this.Buton_GirisSekmesi.Text = "Giriş Yap";
            this.Buton_GirisSekmesi.UseVisualStyleBackColor = true;
            this.Buton_GirisSekmesi.Click += new System.EventHandler(this.Buton_GirisSekmesi_Click);
            // 
            // Buton_OtoKayitDoldur
            // 
            this.Buton_OtoKayitDoldur.FlatAppearance.BorderSize = 0;
            this.Buton_OtoKayitDoldur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_OtoKayitDoldur.Location = new System.Drawing.Point(297, 0);
            this.Buton_OtoKayitDoldur.Margin = new System.Windows.Forms.Padding(4);
            this.Buton_OtoKayitDoldur.Name = "Buton_OtoKayitDoldur";
            this.Buton_OtoKayitDoldur.Size = new System.Drawing.Size(28, 25);
            this.Buton_OtoKayitDoldur.TabIndex = 19;
            this.Buton_OtoKayitDoldur.UseVisualStyleBackColor = true;
            this.Buton_OtoKayitDoldur.Click += new System.EventHandler(this.Buton_OtoKayitDoldur_Click);
            // 
            // Kutu_KayitIlOnayResmi
            // 
            this.Kutu_KayitIlOnayResmi.Cursor = System.Windows.Forms.Cursors.Default;
            this.Kutu_KayitIlOnayResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_KayitIlOnayResmi.Image")));
            this.Kutu_KayitIlOnayResmi.Location = new System.Drawing.Point(242, 192);
            this.Kutu_KayitIlOnayResmi.Name = "Kutu_KayitIlOnayResmi";
            this.Kutu_KayitIlOnayResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_KayitIlOnayResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_KayitIlOnayResmi.TabIndex = 18;
            this.Kutu_KayitIlOnayResmi.TabStop = false;
            this.Kutu_KayitIlOnayResmi.Tag = "";
            this.Kutu_KayitIlOnayResmi.Visible = false;
            // 
            // Kutu_KayitSoyadOnayResmi
            // 
            this.Kutu_KayitSoyadOnayResmi.Cursor = System.Windows.Forms.Cursors.Default;
            this.Kutu_KayitSoyadOnayResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_KayitSoyadOnayResmi.Image")));
            this.Kutu_KayitSoyadOnayResmi.Location = new System.Drawing.Point(242, 161);
            this.Kutu_KayitSoyadOnayResmi.Name = "Kutu_KayitSoyadOnayResmi";
            this.Kutu_KayitSoyadOnayResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_KayitSoyadOnayResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_KayitSoyadOnayResmi.TabIndex = 17;
            this.Kutu_KayitSoyadOnayResmi.TabStop = false;
            this.Kutu_KayitSoyadOnayResmi.Tag = "";
            this.Kutu_KayitSoyadOnayResmi.Visible = false;
            // 
            // Kutu_KayitAdOnayResmi
            // 
            this.Kutu_KayitAdOnayResmi.Cursor = System.Windows.Forms.Cursors.Default;
            this.Kutu_KayitAdOnayResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_KayitAdOnayResmi.Image")));
            this.Kutu_KayitAdOnayResmi.Location = new System.Drawing.Point(242, 128);
            this.Kutu_KayitAdOnayResmi.Name = "Kutu_KayitAdOnayResmi";
            this.Kutu_KayitAdOnayResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_KayitAdOnayResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_KayitAdOnayResmi.TabIndex = 16;
            this.Kutu_KayitAdOnayResmi.TabStop = false;
            this.Kutu_KayitAdOnayResmi.Tag = "";
            this.Kutu_KayitAdOnayResmi.Visible = false;
            // 
            // Kutu_KayitSifreOnayResmi
            // 
            this.Kutu_KayitSifreOnayResmi.Cursor = System.Windows.Forms.Cursors.Default;
            this.Kutu_KayitSifreOnayResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_KayitSifreOnayResmi.Image")));
            this.Kutu_KayitSifreOnayResmi.Location = new System.Drawing.Point(242, 95);
            this.Kutu_KayitSifreOnayResmi.Name = "Kutu_KayitSifreOnayResmi";
            this.Kutu_KayitSifreOnayResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_KayitSifreOnayResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_KayitSifreOnayResmi.TabIndex = 15;
            this.Kutu_KayitSifreOnayResmi.TabStop = false;
            this.Kutu_KayitSifreOnayResmi.Tag = "";
            this.Kutu_KayitSifreOnayResmi.Visible = false;
            // 
            // Kutu_KayitEpostaOnayResmi
            // 
            this.Kutu_KayitEpostaOnayResmi.Cursor = System.Windows.Forms.Cursors.Default;
            this.Kutu_KayitEpostaOnayResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_KayitEpostaOnayResmi.Image")));
            this.Kutu_KayitEpostaOnayResmi.Location = new System.Drawing.Point(242, 62);
            this.Kutu_KayitEpostaOnayResmi.Name = "Kutu_KayitEpostaOnayResmi";
            this.Kutu_KayitEpostaOnayResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_KayitEpostaOnayResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_KayitEpostaOnayResmi.TabIndex = 14;
            this.Kutu_KayitEpostaOnayResmi.TabStop = false;
            this.Kutu_KayitEpostaOnayResmi.Tag = "";
            this.Kutu_KayitEpostaOnayResmi.Visible = false;
            // 
            // Kutu_KayitEpostaResmi
            // 
            this.Kutu_KayitEpostaResmi.Cursor = System.Windows.Forms.Cursors.Default;
            this.Kutu_KayitEpostaResmi.Image = ((System.Drawing.Image)(resources.GetObject("Kutu_KayitEpostaResmi.Image")));
            this.Kutu_KayitEpostaResmi.Location = new System.Drawing.Point(41, 62);
            this.Kutu_KayitEpostaResmi.Name = "Kutu_KayitEpostaResmi";
            this.Kutu_KayitEpostaResmi.Size = new System.Drawing.Size(28, 25);
            this.Kutu_KayitEpostaResmi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_KayitEpostaResmi.TabIndex = 13;
            this.Kutu_KayitEpostaResmi.TabStop = false;
            this.Kutu_KayitEpostaResmi.Tag = "";
            this.Ipucu_KayitEposta.SetToolTip(this.Kutu_KayitEpostaResmi, "Bu E-Posta zaten kullanımda, Giriş yapmayı deneyebilirsiniz.");
            this.Kutu_KayitEpostaResmi.Visible = false;
            // 
            // Kutu_KayitUyarisi
            // 
            this.Kutu_KayitUyarisi.Location = new System.Drawing.Point(41, 371);
            this.Kutu_KayitUyarisi.Name = "Kutu_KayitUyarisi";
            this.Kutu_KayitUyarisi.Size = new System.Drawing.Size(229, 46);
            this.Kutu_KayitUyarisi.TabIndex = 12;
            // 
            // Kutu_KayitSoyad
            // 
            this.Kutu_KayitSoyad.Location = new System.Drawing.Point(76, 161);
            this.Kutu_KayitSoyad.Margin = new System.Windows.Forms.Padding(4);
            this.Kutu_KayitSoyad.Name = "Kutu_KayitSoyad";
            this.Kutu_KayitSoyad.Size = new System.Drawing.Size(159, 25);
            this.Kutu_KayitSoyad.TabIndex = 11;
            this.Kutu_KayitSoyad.Text = "Soyad";
            this.Kutu_KayitSoyad.Enter += new System.EventHandler(this.Kutu_KayitSoyad_Enter);
            this.Kutu_KayitSoyad.Leave += new System.EventHandler(this.Kutu_KayitSoyad_Leave);
            // 
            // Kutu_KayitAd
            // 
            this.Kutu_KayitAd.Location = new System.Drawing.Point(76, 128);
            this.Kutu_KayitAd.Margin = new System.Windows.Forms.Padding(4);
            this.Kutu_KayitAd.Name = "Kutu_KayitAd";
            this.Kutu_KayitAd.Size = new System.Drawing.Size(159, 25);
            this.Kutu_KayitAd.TabIndex = 10;
            this.Kutu_KayitAd.Text = "Ad";
            this.Kutu_KayitAd.Enter += new System.EventHandler(this.Kutu_KayitAd_Enter);
            this.Kutu_KayitAd.Leave += new System.EventHandler(this.Kutu_KayitAd_Leave);
            // 
            // Buton_KayitSifreGorunurluk
            // 
            this.Buton_KayitSifreGorunurluk.Location = new System.Drawing.Point(207, 95);
            this.Buton_KayitSifreGorunurluk.Margin = new System.Windows.Forms.Padding(4);
            this.Buton_KayitSifreGorunurluk.Name = "Buton_KayitSifreGorunurluk";
            this.Buton_KayitSifreGorunurluk.Size = new System.Drawing.Size(28, 25);
            this.Buton_KayitSifreGorunurluk.TabIndex = 8;
            this.Buton_KayitSifreGorunurluk.Text = "G";
            this.Buton_KayitSifreGorunurluk.UseVisualStyleBackColor = true;
            this.Buton_KayitSifreGorunurluk.Click += new System.EventHandler(this.Buton_KayitSifreGorunurluk_Click);
            // 
            // Kutu_KayitSifre
            // 
            this.Kutu_KayitSifre.Location = new System.Drawing.Point(76, 95);
            this.Kutu_KayitSifre.Margin = new System.Windows.Forms.Padding(4);
            this.Kutu_KayitSifre.Name = "Kutu_KayitSifre";
            this.Kutu_KayitSifre.Size = new System.Drawing.Size(132, 25);
            this.Kutu_KayitSifre.TabIndex = 7;
            this.Kutu_KayitSifre.Text = "Şifre";
            this.Kutu_KayitSifre.Enter += new System.EventHandler(this.Kutu_KayitSifre_Enter);
            this.Kutu_KayitSifre.Leave += new System.EventHandler(this.Kutu_KayitSifre_Leave);
            // 
            // Buton_UyeOl
            // 
            this.Buton_UyeOl.Location = new System.Drawing.Point(75, 337);
            this.Buton_UyeOl.Margin = new System.Windows.Forms.Padding(4);
            this.Buton_UyeOl.Name = "Buton_UyeOl";
            this.Buton_UyeOl.Size = new System.Drawing.Size(79, 30);
            this.Buton_UyeOl.TabIndex = 6;
            this.Buton_UyeOl.Text = "Üye Ol";
            this.Buton_UyeOl.UseVisualStyleBackColor = true;
            this.Buton_UyeOl.Click += new System.EventHandler(this.Buton_UyeOl_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Nirmala UI", 16F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(120, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 30);
            this.label2.TabIndex = 5;
            this.label2.Text = "Üyelik";
            // 
            // Kutu_KayitEposta
            // 
            this.Kutu_KayitEposta.Location = new System.Drawing.Point(76, 62);
            this.Kutu_KayitEposta.Margin = new System.Windows.Forms.Padding(4);
            this.Kutu_KayitEposta.Name = "Kutu_KayitEposta";
            this.Kutu_KayitEposta.Size = new System.Drawing.Size(159, 25);
            this.Kutu_KayitEposta.TabIndex = 4;
            this.Kutu_KayitEposta.Text = "E-posta: uye@mail.com";
            this.Kutu_KayitEposta.Enter += new System.EventHandler(this.Kutu_KayitEposta_Enter);
            this.Kutu_KayitEposta.Leave += new System.EventHandler(this.Kutu_KayitEposta_Leave);
            // 
            // Ipucu_KayitEposta
            // 
            this.Ipucu_KayitEposta.IsBalloon = true;
            this.Ipucu_KayitEposta.Tag = "";
            this.Ipucu_KayitEposta.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // Form_GirisEkrani
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 420);
            this.Controls.Add(this.girisSekmeleri);
            this.Font = new System.Drawing.Font("Nirmala UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Form_GirisEkrani";
            this.Text = "Giriş Ekranı";
            this.Load += new System.EventHandler(this.Form_GirisEkrani_Load);
            this.girisSekmeleri.ResumeLayout(false);
            this.Sekme_uyeGirisi.ResumeLayout(false);
            this.Sekme_uyeGirisi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_BaglantiOnayResmi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_SifreUyariBilgiResmi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_EpostaUyariBilgiResmi)).EndInit();
            this.Kutu_IsimSifreGrubu.ResumeLayout(false);
            this.Kutu_IsimSifreGrubu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_SifreUyariResmi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_EpostaUyariResmi)).EndInit();
            this.Sekme_uyelik.ResumeLayout(false);
            this.Sekme_uyelik.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitIlceOnayResmi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitResmiOnayResmi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitResmi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitIlOnayResmi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitSoyadOnayResmi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitAdOnayResmi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitSifreOnayResmi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitEpostaOnayResmi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_KayitEpostaResmi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox Kutu_UyeEposta;
        private System.Windows.Forms.Button Buton_Giris;
        private System.Windows.Forms.TabControl girisSekmeleri;
        private System.Windows.Forms.TabPage Sekme_uyeGirisi;
        private System.Windows.Forms.TabPage Sekme_uyelik;
        private System.Windows.Forms.TextBox Kutu_UyeSifre;
        private System.Windows.Forms.Button Buton_UyeSifreGorunurluk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Buton_KayitSifreGorunurluk;
        private System.Windows.Forms.TextBox Kutu_KayitSifre;
        private System.Windows.Forms.Button Buton_UyeOl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Kutu_KayitEposta;
        private System.Windows.Forms.GroupBox Kutu_IsimSifreGrubu;
        private System.Windows.Forms.PictureBox Kutu_SifreUyariResmi;
        private System.Windows.Forms.PictureBox Kutu_EpostaUyariResmi;
        private System.Windows.Forms.Label Kutu_EpostaUyari;
        private System.Windows.Forms.Label Kutu_SifreUyari;
        private System.Windows.Forms.PictureBox Kutu_SifreUyariBilgiResmi;
        private System.Windows.Forms.PictureBox Kutu_EpostaUyariBilgiResmi;
        private System.Windows.Forms.TextBox Kutu_KayitSoyad;
        private System.Windows.Forms.TextBox Kutu_KayitAd;
        private System.Windows.Forms.Label Kutu_KayitUyarisi;
        private System.Windows.Forms.PictureBox Kutu_KayitEpostaResmi;
        private System.Windows.Forms.ToolTip Ipucu_KayitEposta;
        private System.Windows.Forms.PictureBox Kutu_KayitEpostaOnayResmi;
        private System.Windows.Forms.PictureBox Kutu_KayitIlOnayResmi;
        private System.Windows.Forms.PictureBox Kutu_KayitSoyadOnayResmi;
        private System.Windows.Forms.PictureBox Kutu_KayitAdOnayResmi;
        private System.Windows.Forms.PictureBox Kutu_KayitSifreOnayResmi;
        private System.Windows.Forms.Button Buton_OtoKayitDoldur;
        private System.Windows.Forms.Button Buton_OtoGirisDoldur;
        private System.Windows.Forms.Label Kutu_BaglantiYazisi;
        private System.Windows.Forms.PictureBox Kutu_BaglantiOnayResmi;
        private System.Windows.Forms.Button Buton_UyeOlSekmesi;
        private System.Windows.Forms.Button Buton_GirisSekmesi;
        private System.Windows.Forms.PictureBox Kutu_KayitResmiOnayResmi;
        private System.Windows.Forms.PictureBox Kutu_KayitResmi;
        private System.Windows.Forms.Button Buton_KayitResmiSec;
        private System.Windows.Forms.Button Buton_UyeKontrol;
        private System.Windows.Forms.Button Buton_ProfilKurtar;
        private System.Windows.Forms.ComboBox ListeKutu_Il;
        private System.Windows.Forms.CheckBox Tik_Admin;
        private System.Windows.Forms.PictureBox Kutu_KayitIlceOnayResmi;
        private System.Windows.Forms.ComboBox ListeKutu_Ilce;
    }
}