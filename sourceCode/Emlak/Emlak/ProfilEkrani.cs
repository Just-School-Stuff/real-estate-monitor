﻿/*
    * Copyright (c) 2018, MUHAMMED SAID BILGEHAN
    * All rights reserved.

    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
    * 1. Redistributions of source code must retain the above copyright
    *    notice, this list of conditions and the following disclaimer.
    * 2. Redistributions in binary form must reproduce the above copyright
    *    notice, this list of conditions and the following disclaimer in the
    *    documentation and/or other materials provided with the distribution.
    * 3. All advertising materials mentioning features or use of this software
    *    must take permission from MUHAMMED SAID BILGEHAN and must display the 
    *	   following acknowledgement:
    *    This product includes software developed by the MUHAMMED SAID BILGEHAN.
    * 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
    *    names of its contributors may be used to endorse or promote products
    *    derived from this software without specific prior written permission.

    * THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
    * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    * DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
    * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    * (INCLUDING NEGLİGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBLITY OF SUCH DAMAGE.
 */

using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace Emlak
{
    public partial class ProfilEkrani : Form
    {
        public ProfilEkrani()
        {
            InitializeComponent();
        }

        private readonly string B = KullaniciVerisi.Anahtar;
        private readonly string T = "Tablo_Uye";
        private readonly string TT = "Tablo_Konum";

        DataTable IlListesi;
        DataTable IlceListesi;

        private bool Kontrol_EpostaDegisti = false;
        private bool Kontrol_AdDegisti = false;
        private bool Kontrol_SoyadDegisti = false;
        private bool Kontrol_ResimDegisti = false;
        private bool Kontrol_IlDegisti = false;
        private bool Kontrol_IlceDegisti = false;
        private bool Kontrol_SifreDegisti = false;
        

        private void ProfilEkrani_Load(object sender, EventArgs e)
        {
            KonumAl();
            if (!KullaniciVerisi.ProfilKontrol())
                try
                {
                    KullaniciVerisi.ProfilOlustur(KullaniciVerisi.ProfilBilgisi('e').ToString());
                }
                catch (Exception HATA)
                {
                    MessageBox.Show(HATA.Message, "0024 Profil Oluşturma Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Dispose(true);
                }
            else
            {
                Kutu_ProfilResim.Image = Image.FromStream(new MemoryStream(KullaniciVerisi.ProfilResmi()));

                object[] Bilgiler = KullaniciVerisi.ProfilBilgisi('e', 'a', 's', 'I', 'i');

                Kutu_ProfilEposta.Text = Bilgiler[0].ToString();

                Kutu_ProfilAd.Text = Bilgiler[1].ToString();
                Kutu_ProfilSoyad.Text = Bilgiler[2].ToString();
                ListeKutu_Il.SelectedValue = (int)Bilgiler[3];
                
                ListeKutu_Ilce.SelectedValue = (int)Bilgiler[4];
                ListeKutu_Ilce.DisplayMember = "Ilce";

                Kontrol_IlDegisti = false;
                Kontrol_IlceDegisti = false;
            }
        }

        private void KonumAl()
        {
            if (KullaniciVerisi.KonumListesiKontrol())
            {
                IlListesi = KullaniciVerisi.KonumListesi(true);
                ListeKutu_Il.DisplayMember = "Il";
                ListeKutu_Il.ValueMember = "IlNo";
                ListeKutu_Il.Sorted = false;
                ListeKutu_Il.DataSource = IlListesi;

                IlceListesi = KullaniciVerisi.KonumListesi(false);
                IlceListesi.DefaultView.RowFilter = $"Bagli_IlNo = {ListeKutu_Il.SelectedValue}";
                ListeKutu_Ilce.DisplayMember = "Ilce";
                ListeKutu_Ilce.ValueMember = "IlceNo";
                ListeKutu_Ilce.Sorted = false;
                ListeKutu_Ilce.DataSource = IlceListesi;
            }
            else
            {
                if (KullaniciVerisi.KonumListesiOlustur()) KonumAl();
                else MessageBox.Show(this, "Bir hatadan dolay konum listesi alınamadı. Lütfen daha sonra tekrar deneyiniz.", "0032 Konum Listesi Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Buton_ResimDegistir_Click(object sender, EventArgs e)
        {
            OpenFileDialog ResimSec = new OpenFileDialog();
            ResimSec.AddExtension = false;
            ResimSec.Filter = "Resim Dosyası|*.png;*.jpg;*.jpeg;";
            ResimSec.Multiselect = false;

            DialogResult ResimSecimi = ResimSec.ShowDialog(this);
            if(ResimSecimi == DialogResult.OK)
            {
                Kutu_ProfilResim.ImageLocation = ResimSec.FileName;
                Kutu_ProfilResim.Load();
                Kontrol_ResimDegisti = true;
                Kutu_Cikti.Text = "Profil Resmi Değiştirildi";
            }
            Kutu_Cikti.Text = "Profil Resmi Değiştirilmedi";
        }

        private void Buton_ProfilKaydet_Click(object sender, EventArgs e)
        {
            if( DegisiklikKontrol() )
            {
                try
                {
                    using (SqlConnection Bag = new SqlConnection(B))
                    {
                        Bag.Open();
                        if (Kontrol_EpostaDegisti)
                        {
                            string simdikiEposta = KullaniciVerisi.ProfilBilgisi('e').ToString();

                            SqlCommand UyeEpostaGuncelle = new SqlCommand(SQLKomut.Guncelle(T, $"Uye_EPosta='{Kutu_ProfilEposta.Text}'", $"Uye_EPosta='{simdikiEposta}'"), Bag);
                            SqlCommand KonumEpostaGuncelle = new SqlCommand(SQLKomut.Guncelle(TT, $"Uye_Eposta='{Kutu_ProfilEposta.Text}'", $"Uye_Eposta='{simdikiEposta}'"), Bag);
                            SqlCommand FK_Kapat = new SqlCommand(SQLKomut.FKOzellikSil(TT, T), Bag);
                            SqlCommand FK_Ac = new SqlCommand(SQLKomut.FKOzellikEkle(TT, T, "Uye_Eposta", "Uye_EPosta"), Bag);

                            FK_Kapat.ExecuteNonQuery();
                            FK_Kapat.Dispose();

                            KonumEpostaGuncelle.ExecuteNonQuery();
                            KonumEpostaGuncelle.Dispose();

                            UyeEpostaGuncelle.ExecuteNonQuery();
                            UyeEpostaGuncelle.Dispose();

                            FK_Ac.ExecuteNonQuery();
                            FK_Ac.Dispose();

                            KullaniciVerisi.ProfilOlustur(Kutu_ProfilEposta.Text);
                        }

                        SqlCommand Guncelle;
                        if (Kontrol_ResimDegisti)
                        {   //Resim ile beraber Ad, Soyad guncelleme
                            MemoryStream Resim = new MemoryStream();
                            Kutu_ProfilResim.Image.Save(Resim, ImageFormat.Png);
                            byte[] ResimByte = Resim.GetBuffer();

                            Guncelle = new SqlCommand(SQLKomut.Guncelle(T, $"Uye_Ad='{Kutu_ProfilAd.Text}', Uye_Soyad='{Kutu_ProfilSoyad.Text}'", $"Uye_EPosta='{Kutu_ProfilEposta.Text}'", true), Bag);
                            Guncelle.Parameters.Add(new SqlParameter("@RESIM", ResimByte));
                        }
                        else
                        {   //Resimsiz Ad, Soyad guncelleme
                            Guncelle = new SqlCommand(SQLKomut.Guncelle(T, $"Uye_Ad='{Kutu_ProfilAd.Text}', Uye_Soyad='{Kutu_ProfilSoyad.Text}'", $"Uye_EPosta='{Kutu_ProfilEposta.Text}'"), Bag);
                        }
                        Guncelle.ExecuteNonQuery();
                        Guncelle.Dispose();

                        if (Kontrol_IlDegisti || Kontrol_IlceDegisti)
                        {
                            SqlCommand KonumGuncelle = new SqlCommand(SQLKomut.Guncelle(TT, $"IlNo='{ListeKutu_Il.SelectedValue}', IlceNo='{ListeKutu_Ilce.SelectedValue}'", $"Uye_EPosta='{Kutu_ProfilEposta.Text}'"), Bag);

                            KonumGuncelle.ExecuteNonQuery();
                            KonumGuncelle.Dispose();
                        }

                        if (Kontrol_SifreDegisti)
                        {
                            SqlCommand SifreGuncelle = new SqlCommand(SQLKomut.Guncelle(T, $"Uye_Sifre='{Kutu_ProfilSifre.Text}'", $"Uye_EPosta='{Kutu_ProfilEposta.Text}'"), Bag);
                            SifreGuncelle.ExecuteNonQuery();
                            SifreGuncelle.Dispose();
                        }
                    }

                    KullaniciVerisi.ProfilGuncelle();

                    Kutu_Cikti.Text = "Değişiklikler Kaydedildi";

                    Kontrol_EpostaDegisti = false;
                    Kontrol_AdDegisti = false;
                    Kontrol_SoyadDegisti = false;
                    Kontrol_ResimDegisti = false;
                    Kontrol_IlDegisti = false;
                    Kontrol_IlceDegisti = false;
                    Kontrol_SifreDegisti = false;
                }
                catch (SqlException VT_HATA)
                {
                    MessageBox.Show(VT_HATA.Message, "0025 VeriTabanı Hatası Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Kutu_Cikti.Text = "0025 VeriTabanı Hatası Oluştu";
                }
                catch (Exception HATA)
                {
                    MessageBox.Show(HATA.Message, "0024 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Kutu_Cikti.Text = "0024 Hata Oluştu";
                }
            }
            else
            {
                Kutu_Cikti.Text = "Herhangi Bir Değişiklik Yapılmadı";
            }
        }

        private void Buton_Varsayılanlar_Click(object sender, EventArgs e)
        {
            object[] vars = KullaniciVerisi.ProfilBilgisi('e', 'a', 's', 'I', 'i');

            Kutu_ProfilEposta.Text = vars[0].ToString();
            Kutu_ProfilAd.Text = vars[1].ToString();
            Kutu_ProfilSoyad.Text = vars[2].ToString();
            ListeKutu_Il.SelectedValue = (int)vars[3];
            
            ListeKutu_Ilce.SelectedValue = (int)vars[4];

            Kutu_ProfilResim.Image = Image.FromStream( new MemoryStream(KullaniciVerisi.ProfilResmi() ) );

            Kutu_Cikti.Text = "Profil Varsayılanlarına Döndürüldü";
        }

        private void Buton_ResimVars_Click(object sender, EventArgs e)
        {
            Kutu_ProfilResim.Image = Image.FromStream( new MemoryStream(KullaniciVerisi.ProfilResmi() ) );
            Kontrol_ResimDegisti = false;
            Kutu_Cikti.Text = "Profil Resmi Varsayılana Döndürüldü";
        }

        private void Buton_EpostaVars_Click(object sender, EventArgs e)
        {
            Kutu_ProfilEposta.Text = KullaniciVerisi.ProfilBilgisi('e').ToString();
            Kontrol_EpostaDegisti = false;
            Kutu_Cikti.Text = "Profil Epostası Varsayılana Döndürüldü";
        }

        private void Buton_AdVars_Click(object sender, EventArgs e)
        {
            Kutu_ProfilAd.Text = KullaniciVerisi.ProfilBilgisi('a').ToString();
            Kontrol_AdDegisti = false;
            Kutu_Cikti.Text = "Profil Adı Varsayılana Döndürüldü";
        }

        private void Buton_SoyadVars_Click(object sender, EventArgs e)
        {
            Kutu_ProfilSoyad.Text = KullaniciVerisi.ProfilBilgisi('s').ToString();
            Kontrol_SoyadDegisti = false;
            Kutu_Cikti.Text = "Profil Soyadı Varsayılana Döndürüldü";
        }

        private void Buton_IlVars_Click(object sender, EventArgs e)
        {
            ListeKutu_Il.SelectedValue = KullaniciVerisi.ProfilBilgisi('I');
            Kontrol_IlDegisti = false;
            Kutu_Cikti.Text = "Profil İl ve İlçe Konumu Varsayılana Döndürüldü";
            IlceVars();
        }
        private void IlceVars()
        {
            ListeKutu_Ilce.SelectedValue = KullaniciVerisi.ProfilBilgisi('i');

            Kontrol_IlceDegisti = false;
        }
        private void Buton_IlceVars_Click(object sender, EventArgs e)
        {
            ListeKutu_Ilce.SelectedValue = KullaniciVerisi.ProfilBilgisi('i');

            Kontrol_IlceDegisti = false;
            Kutu_Cikti.Text = "Profil İlçe Konumu Varsayılana Döndürüldü";
        }

        private void Buton_Geri_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool DegisiklikKontrol()
        {
            if
            (
            Kontrol_EpostaDegisti ||
            Kontrol_AdDegisti ||
            Kontrol_SoyadDegisti ||
            Kontrol_ResimDegisti ||
            Kontrol_IlDegisti ||
            Kontrol_IlceDegisti ||
            Kontrol_SifreDegisti
            )
            {
                return true;
            }
            else return false;
        }

        private void Buton_ProfiliSil_Click(object sender, EventArgs e)
        {
            ProfilSilEkrani PS = new ProfilSilEkrani();
            PS.ShowDialog();
            if(!KullaniciVerisi.ProfilKontrol(Kutu_ProfilEposta.Text))
                Close();
        }

        private void Kutu_ProfilEposta_TextChanged(object sender, EventArgs e)
        {
            if(!KullaniciVerisi.ProfilKontrol(Kutu_ProfilEposta.Text))
                Kontrol_EpostaDegisti = true;
            else
                Kontrol_EpostaDegisti = false;
        }

        private void Kutu_ProfilSifre_TextChanged(object sender, EventArgs e)
        {
            Kontrol_SifreDegisti = true;
        }

        private void Kutu_ProfilAd_TextChanged(object sender, EventArgs e)
        {
            if(KullaniciVerisi.ProfilBilgisi('a').ToString() != Kutu_ProfilAd.Text)
                Kontrol_AdDegisti = true;
            else
                Kontrol_AdDegisti = false;
        }

        private void Kutu_ProfilSoyad_TextChanged(object sender, EventArgs e)
        {
            if (KullaniciVerisi.ProfilBilgisi('s').ToString() != Kutu_ProfilSoyad.Text)
                Kontrol_SoyadDegisti = true;
            else
                Kontrol_SoyadDegisti = false;
        }

        private void ListeKutu_Il_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                IlceListesi.DefaultView.RowFilter = $"Bagli_IlNo = {ListeKutu_Il.SelectedValue}";
                if ((int)KullaniciVerisi.ProfilBilgisi('I') != (int)ListeKutu_Il.SelectedValue)
                    Kontrol_IlDegisti = true;
                else
                    Kontrol_IlDegisti = false;
            }
            catch
            {

            }
        }
        private void ListeKutu_Ilce_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((int)KullaniciVerisi.ProfilBilgisi('i') != (int)ListeKutu_Ilce.SelectedValue)
                Kontrol_IlceDegisti = true;
            else
                Kontrol_IlceDegisti = false;
        }

        private void ProfilEkrani_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DegisiklikKontrol())
            {
                if (MessageBox.Show("Bazı değişiklikler yaptığınızı görüyoruz. Kaydetmeden çıkmak istediğinize emin misiniz?", "Kaydetmek istemez misiniz?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
