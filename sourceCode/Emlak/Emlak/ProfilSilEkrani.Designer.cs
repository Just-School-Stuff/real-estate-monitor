﻿namespace Emlak
{
    partial class ProfilSilEkrani
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Kutu_Aciklama = new System.Windows.Forms.Label();
            this.Kutu_Sifre = new System.Windows.Forms.TextBox();
            this.Buton_Sil = new System.Windows.Forms.Button();
            this.Buton_Iptal = new System.Windows.Forms.Button();
            this.Kutu_Eposta = new System.Windows.Forms.TextBox();
            this.Kutu_EpostaAciklama = new System.Windows.Forms.Label();
            this.Kutu_SifreAciklama = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Kutu_Aciklama
            // 
            this.Kutu_Aciklama.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Kutu_Aciklama.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Kutu_Aciklama.Location = new System.Drawing.Point(12, 9);
            this.Kutu_Aciklama.Name = "Kutu_Aciklama";
            this.Kutu_Aciklama.Size = new System.Drawing.Size(405, 67);
            this.Kutu_Aciklama.TabIndex = 0;
            this.Kutu_Aciklama.Text = "Profilinizi Silmek için Epostanızı ve Şifrenizi giriniz:";
            this.Kutu_Aciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_Sifre
            // 
            this.Kutu_Sifre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Kutu_Sifre.Location = new System.Drawing.Point(139, 112);
            this.Kutu_Sifre.MinimumSize = new System.Drawing.Size(179, 27);
            this.Kutu_Sifre.Name = "Kutu_Sifre";
            this.Kutu_Sifre.Size = new System.Drawing.Size(238, 26);
            this.Kutu_Sifre.TabIndex = 1;
            // 
            // Buton_Sil
            // 
            this.Buton_Sil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Buton_Sil.Location = new System.Drawing.Point(234, 145);
            this.Buton_Sil.Name = "Buton_Sil";
            this.Buton_Sil.Size = new System.Drawing.Size(95, 54);
            this.Buton_Sil.TabIndex = 2;
            this.Buton_Sil.Text = "Profili Sil";
            this.Buton_Sil.UseVisualStyleBackColor = true;
            this.Buton_Sil.Click += new System.EventHandler(this.Buton_Sil_Click);
            // 
            // Buton_Iptal
            // 
            this.Buton_Iptal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.Buton_Iptal.Location = new System.Drawing.Point(91, 145);
            this.Buton_Iptal.Name = "Buton_Iptal";
            this.Buton_Iptal.Size = new System.Drawing.Size(95, 54);
            this.Buton_Iptal.TabIndex = 3;
            this.Buton_Iptal.Text = "İptal";
            this.Buton_Iptal.UseVisualStyleBackColor = true;
            this.Buton_Iptal.Click += new System.EventHandler(this.Buton_Iptal_Click);
            // 
            // Kutu_Eposta
            // 
            this.Kutu_Eposta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Kutu_Eposta.Location = new System.Drawing.Point(139, 79);
            this.Kutu_Eposta.MinimumSize = new System.Drawing.Size(179, 27);
            this.Kutu_Eposta.Name = "Kutu_Eposta";
            this.Kutu_Eposta.Size = new System.Drawing.Size(238, 26);
            this.Kutu_Eposta.TabIndex = 4;
            // 
            // Kutu_EpostaAciklama
            // 
            this.Kutu_EpostaAciklama.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Kutu_EpostaAciklama.Location = new System.Drawing.Point(60, 79);
            this.Kutu_EpostaAciklama.Name = "Kutu_EpostaAciklama";
            this.Kutu_EpostaAciklama.Size = new System.Drawing.Size(73, 27);
            this.Kutu_EpostaAciklama.TabIndex = 5;
            this.Kutu_EpostaAciklama.Text = "Eposta:";
            this.Kutu_EpostaAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_SifreAciklama
            // 
            this.Kutu_SifreAciklama.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Kutu_SifreAciklama.Location = new System.Drawing.Point(80, 112);
            this.Kutu_SifreAciklama.Name = "Kutu_SifreAciklama";
            this.Kutu_SifreAciklama.Size = new System.Drawing.Size(53, 27);
            this.Kutu_SifreAciklama.TabIndex = 6;
            this.Kutu_SifreAciklama.Text = "Şifre:";
            this.Kutu_SifreAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ProfilSilEkrani
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 206);
            this.ControlBox = false;
            this.Controls.Add(this.Kutu_SifreAciklama);
            this.Controls.Add(this.Kutu_EpostaAciklama);
            this.Controls.Add(this.Kutu_Eposta);
            this.Controls.Add(this.Buton_Iptal);
            this.Controls.Add(this.Buton_Sil);
            this.Controls.Add(this.Kutu_Sifre);
            this.Controls.Add(this.Kutu_Aciklama);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProfilSilEkrani";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "ProfilSilEkrani";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Kutu_Aciklama;
        private System.Windows.Forms.TextBox Kutu_Sifre;
        private System.Windows.Forms.Button Buton_Sil;
        private System.Windows.Forms.Button Buton_Iptal;
        private System.Windows.Forms.TextBox Kutu_Eposta;
        private System.Windows.Forms.Label Kutu_EpostaAciklama;
        private System.Windows.Forms.Label Kutu_SifreAciklama;
    }
}