﻿namespace Emlak
{
    partial class AdminEkrani
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminEkrani));
            this.VeriTablosu_VT = new System.Windows.Forms.DataGridView();
            this.Buton_Guncelle = new System.Windows.Forms.Button();
            this.ResimKutusu_SecilenUye = new System.Windows.Forms.PictureBox();
            this.Buton_Sil = new System.Windows.Forms.Button();
            this.Buton_Düzenle = new System.Windows.Forms.Button();
            this.Buton_Kaydet = new System.Windows.Forms.Button();
            this.Grup_VeriTabaniIslemleri = new System.Windows.Forms.GroupBox();
            this.Kutu_VTICikti = new System.Windows.Forms.Label();
            this.Grup_UyeIslemleri = new System.Windows.Forms.GroupBox();
            this.Tik_SoruSor = new System.Windows.Forms.CheckBox();
            this.ListeKutu_Ilce = new System.Windows.Forms.ComboBox();
            this.ListeKutu_Il = new System.Windows.Forms.ComboBox();
            this.Kutu_Cikti = new System.Windows.Forms.Label();
            this.Grup_UyeEklemeIslemi = new System.Windows.Forms.GroupBox();
            this.Tik_YeniUyeAdminMi = new System.Windows.Forms.CheckBox();
            this.Buton_YeniUyeEkle = new System.Windows.Forms.Button();
            this.Kutu_UyeEkleIlceAciklama = new System.Windows.Forms.Label();
            this.Kutu_UyeEkleIlAciklama = new System.Windows.Forms.Label();
            this.Kutu_UyeEkleSifreAciklama = new System.Windows.Forms.Label();
            this.Kutu_YeniUyeSifre = new System.Windows.Forms.TextBox();
            this.Kutu_UyeEkleEpostaAciklama = new System.Windows.Forms.Label();
            this.Kutu_YeniUyeEposta = new System.Windows.Forms.TextBox();
            this.Kutu_UyeEkleSoyadAciklama = new System.Windows.Forms.Label();
            this.Kutu_YeniUyeSoyad = new System.Windows.Forms.TextBox();
            this.Kutu_UyeEkleAdAciklama = new System.Windows.Forms.Label();
            this.Kutu_YeniUyeAd = new System.Windows.Forms.TextBox();
            this.ListeKutu_YeniUyeIlce = new System.Windows.Forms.ComboBox();
            this.ListeKutu_YeniUyeIl = new System.Windows.Forms.ComboBox();
            this.ResimKutusu_YeniUyeResim = new System.Windows.Forms.PictureBox();
            this.Grup_UyeAramaIslemleri = new System.Windows.Forms.GroupBox();
            this.Kutu_UyeEpostaAramaAciklamasi = new System.Windows.Forms.Label();
            this.Kutu_EpostaArama = new System.Windows.Forms.TextBox();
            this.Kutu_UyeSoyadAramaAciklamasi = new System.Windows.Forms.Label();
            this.Kutu_SoyadArama = new System.Windows.Forms.TextBox();
            this.Kutu_UyeAdAramaAciklamasi = new System.Windows.Forms.Label();
            this.Kutu_AdArama = new System.Windows.Forms.TextBox();
            this.Buton_AramaFiltreleriniTemizle = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.VeriTablosu_VT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResimKutusu_SecilenUye)).BeginInit();
            this.Grup_VeriTabaniIslemleri.SuspendLayout();
            this.Grup_UyeIslemleri.SuspendLayout();
            this.Grup_UyeEklemeIslemi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResimKutusu_YeniUyeResim)).BeginInit();
            this.Grup_UyeAramaIslemleri.SuspendLayout();
            this.SuspendLayout();
            // 
            // VeriTablosu_VT
            // 
            this.VeriTablosu_VT.AllowUserToAddRows = false;
            this.VeriTablosu_VT.AllowUserToDeleteRows = false;
            this.VeriTablosu_VT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VeriTablosu_VT.Location = new System.Drawing.Point(12, 73);
            this.VeriTablosu_VT.MultiSelect = false;
            this.VeriTablosu_VT.Name = "VeriTablosu_VT";
            this.VeriTablosu_VT.ReadOnly = true;
            this.VeriTablosu_VT.RowTemplate.ReadOnly = true;
            this.VeriTablosu_VT.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.VeriTablosu_VT.Size = new System.Drawing.Size(638, 530);
            this.VeriTablosu_VT.TabIndex = 0;
            this.VeriTablosu_VT.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.VeriTablosu_VT_CellValueChanged);
            this.VeriTablosu_VT.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.VeriTablosu_VT_RowEnter);
            this.VeriTablosu_VT.SelectionChanged += new System.EventHandler(this.VeriTablosu_VT_SelectionChanged);
            // 
            // Buton_Guncelle
            // 
            this.Buton_Guncelle.Location = new System.Drawing.Point(162, 14);
            this.Buton_Guncelle.Name = "Buton_Guncelle";
            this.Buton_Guncelle.Size = new System.Drawing.Size(72, 72);
            this.Buton_Guncelle.TabIndex = 1;
            this.Buton_Guncelle.Text = "Güncelle";
            this.Buton_Guncelle.UseVisualStyleBackColor = true;
            this.Buton_Guncelle.Click += new System.EventHandler(this.Buton_Guncelle_Click);
            // 
            // ResimKutusu_SecilenUye
            // 
            this.ResimKutusu_SecilenUye.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ResimKutusu_SecilenUye.Enabled = false;
            this.ResimKutusu_SecilenUye.ErrorImage = ((System.Drawing.Image)(resources.GetObject("ResimKutusu_SecilenUye.ErrorImage")));
            this.ResimKutusu_SecilenUye.Location = new System.Drawing.Point(6, 23);
            this.ResimKutusu_SecilenUye.Margin = new System.Windows.Forms.Padding(7);
            this.ResimKutusu_SecilenUye.Name = "ResimKutusu_SecilenUye";
            this.ResimKutusu_SecilenUye.Padding = new System.Windows.Forms.Padding(7);
            this.ResimKutusu_SecilenUye.Size = new System.Drawing.Size(228, 227);
            this.ResimKutusu_SecilenUye.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ResimKutusu_SecilenUye.TabIndex = 2;
            this.ResimKutusu_SecilenUye.TabStop = false;
            this.ResimKutusu_SecilenUye.Click += new System.EventHandler(this.ResimKutusu_SecilenUye_Click);
            this.ResimKutusu_SecilenUye.MouseEnter += new System.EventHandler(this.ResimKutusu_SecilenUye_MouseEnter);
            // 
            // Buton_Sil
            // 
            this.Buton_Sil.Enabled = false;
            this.Buton_Sil.Location = new System.Drawing.Point(6, 260);
            this.Buton_Sil.Name = "Buton_Sil";
            this.Buton_Sil.Size = new System.Drawing.Size(72, 72);
            this.Buton_Sil.TabIndex = 3;
            this.Buton_Sil.Text = "Üyeyi Sil";
            this.Buton_Sil.UseVisualStyleBackColor = true;
            this.Buton_Sil.Click += new System.EventHandler(this.Buton_Sil_Click);
            // 
            // Buton_Düzenle
            // 
            this.Buton_Düzenle.Location = new System.Drawing.Point(6, 14);
            this.Buton_Düzenle.Name = "Buton_Düzenle";
            this.Buton_Düzenle.Size = new System.Drawing.Size(72, 72);
            this.Buton_Düzenle.TabIndex = 4;
            this.Buton_Düzenle.Text = "Düzenle";
            this.Buton_Düzenle.UseVisualStyleBackColor = true;
            this.Buton_Düzenle.Click += new System.EventHandler(this.Buton_Düzenle_Click);
            // 
            // Buton_Kaydet
            // 
            this.Buton_Kaydet.Location = new System.Drawing.Point(84, 14);
            this.Buton_Kaydet.Name = "Buton_Kaydet";
            this.Buton_Kaydet.Size = new System.Drawing.Size(72, 72);
            this.Buton_Kaydet.TabIndex = 5;
            this.Buton_Kaydet.Text = "Kaydet";
            this.Buton_Kaydet.UseVisualStyleBackColor = true;
            this.Buton_Kaydet.Click += new System.EventHandler(this.Buton_Kaydet_Click);
            // 
            // Grup_VeriTabaniIslemleri
            // 
            this.Grup_VeriTabaniIslemleri.Controls.Add(this.Kutu_VTICikti);
            this.Grup_VeriTabaniIslemleri.Controls.Add(this.Buton_Düzenle);
            this.Grup_VeriTabaniIslemleri.Controls.Add(this.Buton_Guncelle);
            this.Grup_VeriTabaniIslemleri.Controls.Add(this.Buton_Kaydet);
            this.Grup_VeriTabaniIslemleri.Location = new System.Drawing.Point(662, 73);
            this.Grup_VeriTabaniIslemleri.Name = "Grup_VeriTabaniIslemleri";
            this.Grup_VeriTabaniIslemleri.Size = new System.Drawing.Size(240, 180);
            this.Grup_VeriTabaniIslemleri.TabIndex = 9;
            this.Grup_VeriTabaniIslemleri.TabStop = false;
            this.Grup_VeriTabaniIslemleri.Text = "VeriTabanı İşlemleri";
            // 
            // Kutu_VTICikti
            // 
            this.Kutu_VTICikti.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Kutu_VTICikti.Location = new System.Drawing.Point(6, 130);
            this.Kutu_VTICikti.Name = "Kutu_VTICikti";
            this.Kutu_VTICikti.Size = new System.Drawing.Size(228, 47);
            this.Kutu_VTICikti.TabIndex = 12;
            this.Kutu_VTICikti.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Grup_UyeIslemleri
            // 
            this.Grup_UyeIslemleri.Controls.Add(this.Tik_SoruSor);
            this.Grup_UyeIslemleri.Controls.Add(this.ListeKutu_Ilce);
            this.Grup_UyeIslemleri.Controls.Add(this.ListeKutu_Il);
            this.Grup_UyeIslemleri.Controls.Add(this.ResimKutusu_SecilenUye);
            this.Grup_UyeIslemleri.Controls.Add(this.Buton_Sil);
            this.Grup_UyeIslemleri.Location = new System.Drawing.Point(662, 259);
            this.Grup_UyeIslemleri.Name = "Grup_UyeIslemleri";
            this.Grup_UyeIslemleri.Size = new System.Drawing.Size(240, 344);
            this.Grup_UyeIslemleri.TabIndex = 10;
            this.Grup_UyeIslemleri.TabStop = false;
            this.Grup_UyeIslemleri.Text = "Üye İşlemleri";
            // 
            // Tik_SoruSor
            // 
            this.Tik_SoruSor.AutoSize = true;
            this.Tik_SoruSor.Checked = true;
            this.Tik_SoruSor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Tik_SoruSor.Location = new System.Drawing.Point(83, 315);
            this.Tik_SoruSor.Name = "Tik_SoruSor";
            this.Tik_SoruSor.Size = new System.Drawing.Size(129, 17);
            this.Tik_SoruSor.TabIndex = 11;
            this.Tik_SoruSor.Text = "Üye Silmede Soru Sor";
            this.Tik_SoruSor.UseVisualStyleBackColor = true;
            // 
            // ListeKutu_Ilce
            // 
            this.ListeKutu_Ilce.BackColor = System.Drawing.SystemColors.Window;
            this.ListeKutu_Ilce.Enabled = false;
            this.ListeKutu_Ilce.FormattingEnabled = true;
            this.ListeKutu_Ilce.Location = new System.Drawing.Point(83, 287);
            this.ListeKutu_Ilce.Name = "ListeKutu_Ilce";
            this.ListeKutu_Ilce.Size = new System.Drawing.Size(151, 21);
            this.ListeKutu_Ilce.Sorted = true;
            this.ListeKutu_Ilce.TabIndex = 31;
            this.ListeKutu_Ilce.Tag = "İl/İlçe/Semt";
            this.ListeKutu_Ilce.SelectedValueChanged += new System.EventHandler(this.ListeKutu_Ilce_SelectedValueChanged);
            // 
            // ListeKutu_Il
            // 
            this.ListeKutu_Il.BackColor = System.Drawing.SystemColors.Window;
            this.ListeKutu_Il.Enabled = false;
            this.ListeKutu_Il.FormattingEnabled = true;
            this.ListeKutu_Il.Location = new System.Drawing.Point(84, 260);
            this.ListeKutu_Il.Name = "ListeKutu_Il";
            this.ListeKutu_Il.Size = new System.Drawing.Size(150, 21);
            this.ListeKutu_Il.Sorted = true;
            this.ListeKutu_Il.TabIndex = 30;
            this.ListeKutu_Il.Tag = "İl/İlçe/Semt";
            this.ListeKutu_Il.SelectedIndexChanged += new System.EventHandler(this.ListeKutu_Il_SelectedIndexChanged);
            // 
            // Kutu_Cikti
            // 
            this.Kutu_Cikti.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Kutu_Cikti.Location = new System.Drawing.Point(662, 609);
            this.Kutu_Cikti.Name = "Kutu_Cikti";
            this.Kutu_Cikti.Size = new System.Drawing.Size(240, 181);
            this.Kutu_Cikti.TabIndex = 11;
            this.Kutu_Cikti.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Grup_UyeEklemeIslemi
            // 
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Tik_YeniUyeAdminMi);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Buton_YeniUyeEkle);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Kutu_UyeEkleIlceAciklama);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Kutu_UyeEkleIlAciklama);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Kutu_UyeEkleSifreAciklama);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Kutu_YeniUyeSifre);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Kutu_UyeEkleEpostaAciklama);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Kutu_YeniUyeEposta);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Kutu_UyeEkleSoyadAciklama);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Kutu_YeniUyeSoyad);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Kutu_UyeEkleAdAciklama);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.Kutu_YeniUyeAd);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.ListeKutu_YeniUyeIlce);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.ListeKutu_YeniUyeIl);
            this.Grup_UyeEklemeIslemi.Controls.Add(this.ResimKutusu_YeniUyeResim);
            this.Grup_UyeEklemeIslemi.Location = new System.Drawing.Point(12, 609);
            this.Grup_UyeEklemeIslemi.Name = "Grup_UyeEklemeIslemi";
            this.Grup_UyeEklemeIslemi.Size = new System.Drawing.Size(638, 181);
            this.Grup_UyeEklemeIslemi.TabIndex = 32;
            this.Grup_UyeEklemeIslemi.TabStop = false;
            this.Grup_UyeEklemeIslemi.Text = "Üye Ekleme İşlemi";
            // 
            // Tik_YeniUyeAdminMi
            // 
            this.Tik_YeniUyeAdminMi.Location = new System.Drawing.Point(444, 158);
            this.Tik_YeniUyeAdminMi.Name = "Tik_YeniUyeAdminMi";
            this.Tik_YeniUyeAdminMi.Size = new System.Drawing.Size(72, 17);
            this.Tik_YeniUyeAdminMi.TabIndex = 42;
            this.Tik_YeniUyeAdminMi.Text = "Admin";
            this.Tik_YeniUyeAdminMi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Tik_YeniUyeAdminMi.UseVisualStyleBackColor = true;
            this.Tik_YeniUyeAdminMi.CheckedChanged += new System.EventHandler(this.Tik_YeniUyeAdminMi_CheckedChanged);
            // 
            // Buton_YeniUyeEkle
            // 
            this.Buton_YeniUyeEkle.Location = new System.Drawing.Point(434, 98);
            this.Buton_YeniUyeEkle.Name = "Buton_YeniUyeEkle";
            this.Buton_YeniUyeEkle.Size = new System.Drawing.Size(94, 44);
            this.Buton_YeniUyeEkle.TabIndex = 32;
            this.Buton_YeniUyeEkle.Text = "Yeni Üye Ekle";
            this.Buton_YeniUyeEkle.UseVisualStyleBackColor = true;
            this.Buton_YeniUyeEkle.Click += new System.EventHandler(this.Buton_YeniUyeEkle_Click);
            // 
            // Kutu_UyeEkleIlceAciklama
            // 
            this.Kutu_UyeEkleIlceAciklama.Location = new System.Drawing.Point(353, 71);
            this.Kutu_UyeEkleIlceAciklama.Name = "Kutu_UyeEkleIlceAciklama";
            this.Kutu_UyeEkleIlceAciklama.Size = new System.Drawing.Size(50, 21);
            this.Kutu_UyeEkleIlceAciklama.TabIndex = 41;
            this.Kutu_UyeEkleIlceAciklama.Text = "İlçe:";
            this.Kutu_UyeEkleIlceAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_UyeEkleIlAciklama
            // 
            this.Kutu_UyeEkleIlAciklama.Location = new System.Drawing.Point(353, 44);
            this.Kutu_UyeEkleIlAciklama.Name = "Kutu_UyeEkleIlAciklama";
            this.Kutu_UyeEkleIlAciklama.Size = new System.Drawing.Size(50, 21);
            this.Kutu_UyeEkleIlAciklama.TabIndex = 40;
            this.Kutu_UyeEkleIlAciklama.Text = "İl:";
            this.Kutu_UyeEkleIlAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_UyeEkleSifreAciklama
            // 
            this.Kutu_UyeEkleSifreAciklama.Location = new System.Drawing.Point(157, 69);
            this.Kutu_UyeEkleSifreAciklama.Name = "Kutu_UyeEkleSifreAciklama";
            this.Kutu_UyeEkleSifreAciklama.Size = new System.Drawing.Size(50, 21);
            this.Kutu_UyeEkleSifreAciklama.TabIndex = 39;
            this.Kutu_UyeEkleSifreAciklama.Text = "Şifre:";
            this.Kutu_UyeEkleSifreAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_YeniUyeSifre
            // 
            this.Kutu_YeniUyeSifre.Location = new System.Drawing.Point(213, 70);
            this.Kutu_YeniUyeSifre.Name = "Kutu_YeniUyeSifre";
            this.Kutu_YeniUyeSifre.Size = new System.Drawing.Size(134, 20);
            this.Kutu_YeniUyeSifre.TabIndex = 38;
            this.Kutu_YeniUyeSifre.TextChanged += new System.EventHandler(this.Kutu_YeniUyeSifre_TextChanged);
            // 
            // Kutu_UyeEkleEpostaAciklama
            // 
            this.Kutu_UyeEkleEpostaAciklama.Location = new System.Drawing.Point(157, 44);
            this.Kutu_UyeEkleEpostaAciklama.Name = "Kutu_UyeEkleEpostaAciklama";
            this.Kutu_UyeEkleEpostaAciklama.Size = new System.Drawing.Size(50, 21);
            this.Kutu_UyeEkleEpostaAciklama.TabIndex = 37;
            this.Kutu_UyeEkleEpostaAciklama.Text = "Eposta:";
            this.Kutu_UyeEkleEpostaAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_YeniUyeEposta
            // 
            this.Kutu_YeniUyeEposta.Location = new System.Drawing.Point(213, 44);
            this.Kutu_YeniUyeEposta.Name = "Kutu_YeniUyeEposta";
            this.Kutu_YeniUyeEposta.Size = new System.Drawing.Size(134, 20);
            this.Kutu_YeniUyeEposta.TabIndex = 36;
            this.Kutu_YeniUyeEposta.TextChanged += new System.EventHandler(this.Kutu_YeniUyeEposta_TextChanged);
            // 
            // Kutu_UyeEkleSoyadAciklama
            // 
            this.Kutu_UyeEkleSoyadAciklama.Location = new System.Drawing.Point(157, 122);
            this.Kutu_UyeEkleSoyadAciklama.Name = "Kutu_UyeEkleSoyadAciklama";
            this.Kutu_UyeEkleSoyadAciklama.Size = new System.Drawing.Size(50, 21);
            this.Kutu_UyeEkleSoyadAciklama.TabIndex = 35;
            this.Kutu_UyeEkleSoyadAciklama.Text = "SoyAd:";
            this.Kutu_UyeEkleSoyadAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_YeniUyeSoyad
            // 
            this.Kutu_YeniUyeSoyad.Location = new System.Drawing.Point(212, 122);
            this.Kutu_YeniUyeSoyad.Name = "Kutu_YeniUyeSoyad";
            this.Kutu_YeniUyeSoyad.Size = new System.Drawing.Size(134, 20);
            this.Kutu_YeniUyeSoyad.TabIndex = 34;
            this.Kutu_YeniUyeSoyad.TextChanged += new System.EventHandler(this.Kutu_YeniUyeSoyad_TextChanged);
            // 
            // Kutu_UyeEkleAdAciklama
            // 
            this.Kutu_UyeEkleAdAciklama.Location = new System.Drawing.Point(157, 95);
            this.Kutu_UyeEkleAdAciklama.Name = "Kutu_UyeEkleAdAciklama";
            this.Kutu_UyeEkleAdAciklama.Size = new System.Drawing.Size(50, 21);
            this.Kutu_UyeEkleAdAciklama.TabIndex = 33;
            this.Kutu_UyeEkleAdAciklama.Text = "Ad:";
            this.Kutu_UyeEkleAdAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_YeniUyeAd
            // 
            this.Kutu_YeniUyeAd.Location = new System.Drawing.Point(213, 96);
            this.Kutu_YeniUyeAd.Name = "Kutu_YeniUyeAd";
            this.Kutu_YeniUyeAd.Size = new System.Drawing.Size(134, 20);
            this.Kutu_YeniUyeAd.TabIndex = 32;
            this.Kutu_YeniUyeAd.TextChanged += new System.EventHandler(this.Kutu_YeniUyeAd_TextChanged);
            // 
            // ListeKutu_YeniUyeIlce
            // 
            this.ListeKutu_YeniUyeIlce.BackColor = System.Drawing.SystemColors.Window;
            this.ListeKutu_YeniUyeIlce.FormattingEnabled = true;
            this.ListeKutu_YeniUyeIlce.Location = new System.Drawing.Point(409, 71);
            this.ListeKutu_YeniUyeIlce.Name = "ListeKutu_YeniUyeIlce";
            this.ListeKutu_YeniUyeIlce.Size = new System.Drawing.Size(151, 21);
            this.ListeKutu_YeniUyeIlce.Sorted = true;
            this.ListeKutu_YeniUyeIlce.TabIndex = 31;
            this.ListeKutu_YeniUyeIlce.Tag = "İl/İlçe/Semt";
            this.ListeKutu_YeniUyeIlce.SelectedValueChanged += new System.EventHandler(this.ListeKutu_YeniUyeIlce_SelectedValueChanged);
            // 
            // ListeKutu_YeniUyeIl
            // 
            this.ListeKutu_YeniUyeIl.BackColor = System.Drawing.SystemColors.Window;
            this.ListeKutu_YeniUyeIl.FormattingEnabled = true;
            this.ListeKutu_YeniUyeIl.Location = new System.Drawing.Point(409, 44);
            this.ListeKutu_YeniUyeIl.Name = "ListeKutu_YeniUyeIl";
            this.ListeKutu_YeniUyeIl.Size = new System.Drawing.Size(150, 21);
            this.ListeKutu_YeniUyeIl.Sorted = true;
            this.ListeKutu_YeniUyeIl.TabIndex = 30;
            this.ListeKutu_YeniUyeIl.Tag = "İl/İlçe/Semt";
            this.ListeKutu_YeniUyeIl.SelectedValueChanged += new System.EventHandler(this.ListeKutu_YeniUyeIl_SelectedValueChanged);
            // 
            // ResimKutusu_YeniUyeResim
            // 
            this.ResimKutusu_YeniUyeResim.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ResimKutusu_YeniUyeResim.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ResimKutusu_YeniUyeResim.ErrorImage = ((System.Drawing.Image)(resources.GetObject("ResimKutusu_YeniUyeResim.ErrorImage")));
            this.ResimKutusu_YeniUyeResim.Location = new System.Drawing.Point(10, 23);
            this.ResimKutusu_YeniUyeResim.Margin = new System.Windows.Forms.Padding(7);
            this.ResimKutusu_YeniUyeResim.Name = "ResimKutusu_YeniUyeResim";
            this.ResimKutusu_YeniUyeResim.Padding = new System.Windows.Forms.Padding(7);
            this.ResimKutusu_YeniUyeResim.Size = new System.Drawing.Size(137, 137);
            this.ResimKutusu_YeniUyeResim.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ResimKutusu_YeniUyeResim.TabIndex = 2;
            this.ResimKutusu_YeniUyeResim.TabStop = false;
            this.ResimKutusu_YeniUyeResim.Click += new System.EventHandler(this.ResimKutusu_YeniUyeResim_Click);
            this.ResimKutusu_YeniUyeResim.MouseEnter += new System.EventHandler(this.ResimKutusu_YeniUyeResim_MouseEnter);
            // 
            // Grup_UyeAramaIslemleri
            // 
            this.Grup_UyeAramaIslemleri.Controls.Add(this.Buton_AramaFiltreleriniTemizle);
            this.Grup_UyeAramaIslemleri.Controls.Add(this.Kutu_AdArama);
            this.Grup_UyeAramaIslemleri.Controls.Add(this.Kutu_UyeEpostaAramaAciklamasi);
            this.Grup_UyeAramaIslemleri.Controls.Add(this.Kutu_UyeAdAramaAciklamasi);
            this.Grup_UyeAramaIslemleri.Controls.Add(this.Kutu_EpostaArama);
            this.Grup_UyeAramaIslemleri.Controls.Add(this.Kutu_SoyadArama);
            this.Grup_UyeAramaIslemleri.Controls.Add(this.Kutu_UyeSoyadAramaAciklamasi);
            this.Grup_UyeAramaIslemleri.Location = new System.Drawing.Point(12, 12);
            this.Grup_UyeAramaIslemleri.Name = "Grup_UyeAramaIslemleri";
            this.Grup_UyeAramaIslemleri.Size = new System.Drawing.Size(890, 55);
            this.Grup_UyeAramaIslemleri.TabIndex = 34;
            this.Grup_UyeAramaIslemleri.TabStop = false;
            this.Grup_UyeAramaIslemleri.Text = "Üye Arama İşlemleri";
            // 
            // Kutu_UyeEpostaAramaAciklamasi
            // 
            this.Kutu_UyeEpostaAramaAciklamasi.Location = new System.Drawing.Point(97, 21);
            this.Kutu_UyeEpostaAramaAciklamasi.Name = "Kutu_UyeEpostaAramaAciklamasi";
            this.Kutu_UyeEpostaAramaAciklamasi.Size = new System.Drawing.Size(50, 21);
            this.Kutu_UyeEpostaAramaAciklamasi.TabIndex = 50;
            this.Kutu_UyeEpostaAramaAciklamasi.Text = "Eposta:";
            this.Kutu_UyeEpostaAramaAciklamasi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_EpostaArama
            // 
            this.Kutu_EpostaArama.Location = new System.Drawing.Point(153, 21);
            this.Kutu_EpostaArama.Name = "Kutu_EpostaArama";
            this.Kutu_EpostaArama.Size = new System.Drawing.Size(134, 20);
            this.Kutu_EpostaArama.TabIndex = 49;
            this.Kutu_EpostaArama.TextChanged += new System.EventHandler(this.Kutu_EpostaArama_TextChanged);
            // 
            // Kutu_UyeSoyadAramaAciklamasi
            // 
            this.Kutu_UyeSoyadAramaAciklamasi.Location = new System.Drawing.Point(515, 22);
            this.Kutu_UyeSoyadAramaAciklamasi.Name = "Kutu_UyeSoyadAramaAciklamasi";
            this.Kutu_UyeSoyadAramaAciklamasi.Size = new System.Drawing.Size(50, 21);
            this.Kutu_UyeSoyadAramaAciklamasi.TabIndex = 48;
            this.Kutu_UyeSoyadAramaAciklamasi.Text = "SoyAd:";
            this.Kutu_UyeSoyadAramaAciklamasi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_SoyadArama
            // 
            this.Kutu_SoyadArama.Location = new System.Drawing.Point(570, 22);
            this.Kutu_SoyadArama.Name = "Kutu_SoyadArama";
            this.Kutu_SoyadArama.Size = new System.Drawing.Size(134, 20);
            this.Kutu_SoyadArama.TabIndex = 47;
            this.Kutu_SoyadArama.TextChanged += new System.EventHandler(this.Kutu_SoyadArama_TextChanged);
            // 
            // Kutu_UyeAdAramaAciklamasi
            // 
            this.Kutu_UyeAdAramaAciklamasi.Location = new System.Drawing.Point(302, 20);
            this.Kutu_UyeAdAramaAciklamasi.Name = "Kutu_UyeAdAramaAciklamasi";
            this.Kutu_UyeAdAramaAciklamasi.Size = new System.Drawing.Size(50, 21);
            this.Kutu_UyeAdAramaAciklamasi.TabIndex = 46;
            this.Kutu_UyeAdAramaAciklamasi.Text = "Ad:";
            this.Kutu_UyeAdAramaAciklamasi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_AdArama
            // 
            this.Kutu_AdArama.Location = new System.Drawing.Point(358, 21);
            this.Kutu_AdArama.Name = "Kutu_AdArama";
            this.Kutu_AdArama.Size = new System.Drawing.Size(134, 20);
            this.Kutu_AdArama.TabIndex = 45;
            this.Kutu_AdArama.TextChanged += new System.EventHandler(this.Kutu_AdArama_TextChanged);
            // 
            // Buton_AramaFiltreleriniTemizle
            // 
            this.Buton_AramaFiltreleriniTemizle.Location = new System.Drawing.Point(734, 12);
            this.Buton_AramaFiltreleriniTemizle.Name = "Buton_AramaFiltreleriniTemizle";
            this.Buton_AramaFiltreleriniTemizle.Size = new System.Drawing.Size(96, 36);
            this.Buton_AramaFiltreleriniTemizle.TabIndex = 43;
            this.Buton_AramaFiltreleriniTemizle.Text = "Filtreleri Temizle";
            this.Buton_AramaFiltreleriniTemizle.UseVisualStyleBackColor = true;
            this.Buton_AramaFiltreleriniTemizle.Click += new System.EventHandler(this.Buton_AramaFiltreleriniTemizle_Click);
            // 
            // AdminEkrani
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 800);
            this.Controls.Add(this.Grup_UyeAramaIslemleri);
            this.Controls.Add(this.Grup_UyeEklemeIslemi);
            this.Controls.Add(this.Kutu_Cikti);
            this.Controls.Add(this.Grup_UyeIslemleri);
            this.Controls.Add(this.Grup_VeriTabaniIslemleri);
            this.Controls.Add(this.VeriTablosu_VT);
            this.Name = "AdminEkrani";
            this.Text = "AdminEkrani";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminEkrani_FormClosing);
            this.Load += new System.EventHandler(this.AdminEkrani_Load);
            ((System.ComponentModel.ISupportInitialize)(this.VeriTablosu_VT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResimKutusu_SecilenUye)).EndInit();
            this.Grup_VeriTabaniIslemleri.ResumeLayout(false);
            this.Grup_UyeIslemleri.ResumeLayout(false);
            this.Grup_UyeIslemleri.PerformLayout();
            this.Grup_UyeEklemeIslemi.ResumeLayout(false);
            this.Grup_UyeEklemeIslemi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResimKutusu_YeniUyeResim)).EndInit();
            this.Grup_UyeAramaIslemleri.ResumeLayout(false);
            this.Grup_UyeAramaIslemleri.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView VeriTablosu_VT;
        private System.Windows.Forms.Button Buton_Guncelle;
        private System.Windows.Forms.PictureBox ResimKutusu_SecilenUye;
        private System.Windows.Forms.Button Buton_Sil;
        private System.Windows.Forms.Button Buton_Düzenle;
        private System.Windows.Forms.Button Buton_Kaydet;
        private System.Windows.Forms.GroupBox Grup_VeriTabaniIslemleri;
        private System.Windows.Forms.GroupBox Grup_UyeIslemleri;
        private System.Windows.Forms.ComboBox ListeKutu_Ilce;
        private System.Windows.Forms.ComboBox ListeKutu_Il;
        private System.Windows.Forms.CheckBox Tik_SoruSor;
        private System.Windows.Forms.Label Kutu_Cikti;
        private System.Windows.Forms.Label Kutu_VTICikti;
        private System.Windows.Forms.GroupBox Grup_UyeEklemeIslemi;
        private System.Windows.Forms.Button Buton_YeniUyeEkle;
        private System.Windows.Forms.Label Kutu_UyeEkleIlceAciklama;
        private System.Windows.Forms.Label Kutu_UyeEkleIlAciklama;
        private System.Windows.Forms.Label Kutu_UyeEkleSifreAciklama;
        private System.Windows.Forms.TextBox Kutu_YeniUyeSifre;
        private System.Windows.Forms.Label Kutu_UyeEkleEpostaAciklama;
        private System.Windows.Forms.TextBox Kutu_YeniUyeEposta;
        private System.Windows.Forms.Label Kutu_UyeEkleSoyadAciklama;
        private System.Windows.Forms.TextBox Kutu_YeniUyeSoyad;
        private System.Windows.Forms.Label Kutu_UyeEkleAdAciklama;
        private System.Windows.Forms.TextBox Kutu_YeniUyeAd;
        private System.Windows.Forms.ComboBox ListeKutu_YeniUyeIlce;
        private System.Windows.Forms.ComboBox ListeKutu_YeniUyeIl;
        private System.Windows.Forms.PictureBox ResimKutusu_YeniUyeResim;
        private System.Windows.Forms.CheckBox Tik_YeniUyeAdminMi;
        private System.Windows.Forms.GroupBox Grup_UyeAramaIslemleri;
        private System.Windows.Forms.TextBox Kutu_AdArama;
        private System.Windows.Forms.Label Kutu_UyeEpostaAramaAciklamasi;
        private System.Windows.Forms.Label Kutu_UyeAdAramaAciklamasi;
        private System.Windows.Forms.TextBox Kutu_EpostaArama;
        private System.Windows.Forms.TextBox Kutu_SoyadArama;
        private System.Windows.Forms.Label Kutu_UyeSoyadAramaAciklamasi;
        private System.Windows.Forms.Button Buton_AramaFiltreleriniTemizle;
    }
}