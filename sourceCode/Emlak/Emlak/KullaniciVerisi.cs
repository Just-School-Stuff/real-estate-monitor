﻿/*
    * Copyright (c) 2018, MUHAMMED SAID BILGEHAN
    * All rights reserved.

    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
    * 1. Redistributions of source code must retain the above copyright
    *    notice, this list of conditions and the following disclaimer.
    * 2. Redistributions in binary form must reproduce the above copyright
    *    notice, this list of conditions and the following disclaimer in the
    *    documentation and/or other materials provided with the distribution.
    * 3. All advertising materials mentioning features or use of this software
    *    must take permission from MUHAMMED SAID BILGEHAN and must display the 
    *	   following acknowledgement:
    *    This product includes software developed by the MUHAMMED SAID BILGEHAN.
    * 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
    *    names of its contributors may be used to endorse or promote products
    *    derived from this software without specific prior written permission.

    * THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
    * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    * DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
    * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    * (INCLUDING NEGLİGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBLITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.IO;

namespace Emlak
{
    public static class KullaniciVerisi
    {
        private static string SonGirisDosyaKonumu = @"./SonGiris.txt";

        public static string Anahtar = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\msaid\source\repos\Emlak\Emlak\EmlakVT.mdf;Integrated Security=True";
        
        private static SqlConnection Bag;
        private static string TUye = "Tablo_Uye";
        private static string TKonum = "Tablo_Konum";
        private static string TIl = "Tablo_Il";
        private static string TIlce = "Tablo_Ilce";

        private static string KullaniciEposta;
        private static string KullaniciAd;
        private static string KullaniciSoyad;
        private static byte[] KullaniciResim;
        private static int KullaniciIl;
        private static int KullaniciIlce;

        private static DataTable IlListesi = null;
        private static DataTable IlceListesi = null;

        public static bool ProfilOlustur(string Eposta)
        {
            if (!ProfilKontrol(Eposta))
            {
                ProfilTemizle();
                KullaniciEposta = Eposta;
                try
                {
                    using (Bag = new SqlConnection(Anahtar))
                    {
                        Bag.Open();
                        SqlCommand BilgiIstek = new SqlCommand(SQLKomut.Listele($"{TUye}, {TKonum}", $"{TUye}.Uye_Ad, {TUye}.Uye_Soyad, {TUye}.Uye_Resim, {TKonum}.IlNo, {TKonum}.IlceNo", $"{TUye}.Uye_EPosta='{KullaniciEposta}' and {TUye}.Uye_EPosta={TKonum}.Uye_Eposta"), Bag);
                        SqlDataReader Bilgi = BilgiIstek.ExecuteReader();

                        Bilgi.Read();

                        KullaniciAd = Bilgi.GetString(0);
                        KullaniciSoyad = Bilgi.GetString(1);
                        KullaniciResim = Bilgi.GetSqlBytes(2).Buffer;
                        KullaniciIl = Bilgi.GetInt32(3);
                        KullaniciIlce = Bilgi.GetInt32(4);

                        Bilgi.Close();
                    }
                }
                catch (SqlException VT_HATA)
                {
                    System.Windows.Forms.MessageBox.Show(VT_HATA.Message, "0014 VeriTabanı Hatası Oluştu!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
                catch (Exception HATA)
                {
                    System.Windows.Forms.MessageBox.Show(HATA.Message, "0015 Hata Oluştu!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
                return true;
            }
            else return false;
        }

        public static bool ProfilGuncelle()
        {
            if (KullaniciEposta != null)
            {
                try
                {
                    using (Bag = new SqlConnection(Anahtar))
                    {
                        Bag.Open();
                        SqlCommand BilgiIstek = new SqlCommand(SQLKomut.Listele($"{TUye}, {TKonum}", $"{TUye}.Uye_Ad, {TUye}.Uye_Soyad, {TUye}.Uye_Resim, {TKonum}.IlNo, {TKonum}.IlceNo", $"{TKonum}.Uye_Eposta='{KullaniciEposta}' and {TUye}.Uye_EPosta='{KullaniciEposta}'"), Bag);
                        SqlDataReader Bilgi = BilgiIstek.ExecuteReader();

                        Bilgi.Read();
                        KullaniciAd = Bilgi.GetString(0);
                        KullaniciSoyad = Bilgi.GetString(1);
                        KullaniciResim = Bilgi.GetSqlBytes(2).Buffer;
                        KullaniciIl = Bilgi.GetInt32(3);
                        KullaniciIlce = Bilgi.GetInt32(4);
                    }
                }
                catch (SqlException VT_HATA)
                {
                    System.Windows.Forms.MessageBox.Show(VT_HATA.Message, "0016 VeriTabanı Hatası Oluştu!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
                catch (Exception HATA)
                {
                    System.Windows.Forms.MessageBox.Show(HATA.Message, "0017 Hata Oluştu!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void ProfilTemizle()
        {
            KullaniciEposta = null;
            KullaniciAd = null;
            KullaniciSoyad = null;
            KullaniciResim = null;
            KullaniciIl = 0;
            KullaniciIlce = 0;
        }

        public static bool ProfilSil(string KULLANICI_EPOSTA, string KULLANICI_SIFRESI)
        {
            if (SifreKontrol(KULLANICI_EPOSTA, KULLANICI_SIFRESI))
            {
                try
                {
                    using (Bag = new SqlConnection(Anahtar))
                    {
                        Bag.Open();
                        SqlCommand UyeSil = new SqlCommand(SQLKomut.Sil($"{TUye}", $"Uye_EPosta='{KullaniciEposta}' and Uye_Sifre='{KULLANICI_SIFRESI}'"), Bag);
                        SqlCommand KonumSil = new SqlCommand(SQLKomut.Sil($"{TKonum}", $"Uye_Eposta='{KullaniciEposta}'"), Bag);

                        KonumSil.ExecuteNonQuery();
                        UyeSil.ExecuteNonQuery();

                        UyeSil.Dispose();
                        KonumSil.Dispose();

                        ProfilTemizle();
                    }
                }
                catch (SqlException VT_HATA)
                {
                    System.Windows.Forms.MessageBox.Show(VT_HATA.Message, "0018 VeriTabanı Hatası Oluştu", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
                catch (Exception HATA)
                {
                    System.Windows.Forms.MessageBox.Show(HATA.Message, "0019 Hata Oluştu", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }

                return true;
            }
            else return false;
        }

        public static bool SifreKontrol(string KULLANICI_EPOSTA, string KULLANICI_SIFRESI)
        {
            if (ProfilKontrol(KULLANICI_EPOSTA))
                try
                {
                    using (Bag = new SqlConnection(Anahtar))
                    {
                        SqlCommand SifreKontrol = new SqlCommand(SQLKomut.Listele(TUye, "Uye_Ad", $"Uye_EPosta='{KullaniciEposta}' and Uye_Sifre='{KULLANICI_SIFRESI}'"), Bag);

                        Bag.Open();
                        SqlDataReader Sonuc = SifreKontrol.ExecuteReader();
                        Sonuc.Read();

                        if(Sonuc.HasRows)
                        {
                            return true;
                        }
                        else return false;
                    }
                }
                catch (SqlException VT_HATA)
                {
                    System.Windows.Forms.MessageBox.Show(VT_HATA.Message, "0026 VeriTabanı Hatası Oluştu!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
                catch (Exception HATA)
                {
                    System.Windows.Forms.MessageBox.Show(HATA.Message, "0027 Hatası Oluştu!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
            else return false;
        }

        public static bool ProfilKontrol()
        {
            if
                (
                KullaniciEposta != null &&
                KullaniciAd != null &&
                KullaniciSoyad != null &&
                KullaniciResim != null &&
                KullaniciIl != 0 &&
                KullaniciIlce != 0
                )
            {
                return true;
            }
            else return false;
        }
        public static bool ProfilKontrol(string EPOSTA)
        {
            if (KullaniciEposta == EPOSTA)
            {
                return ProfilKontrol();
            }
            else return false;
        }

        public static object ProfilBilgisi(char SECENEK)
        {
            if (SECENEK == 'e')
                if(KullaniciEposta != null)
                    if (KullaniciEposta.Length > 0)
                        return KullaniciEposta;
                    else return null;
                else return null;
            else
            {
                if (ProfilKontrol())
                {
                    switch (SECENEK)
                    {
                        case 'a':
                            return KullaniciAd;
                        case 's':
                            return KullaniciSoyad;
                        case 'I':
                            return KullaniciIl;
                        case 'i':
                            return KullaniciIlce;
                        default:
                            return null;
                    }
                }
                else return null;
            }
        }
        public static object[] ProfilBilgisi(params char[] SECENEK)
        {
            List<object> cikti = new List<object>();
            if (ProfilKontrol())
            {
                foreach(char SEC in SECENEK)
                {
                    switch (SEC)
                    {
                        case 'e':
                            cikti.Add(KullaniciEposta);
                            break;
                        case 'a':
                            cikti.Add(KullaniciAd);
                            break;
                        case 's':
                            cikti.Add(KullaniciSoyad);
                            break;
                        case 'I':
                            cikti.Add(KullaniciIl);
                            break;
                        case 'i':
                            cikti.Add(KullaniciIlce);
                            break;
                    }
                }
            }
            return cikti.ToArray();
        }

        public static byte[] ProfilResmi()
        {
            if (ProfilKontrol()) return KullaniciResim;
            else return null;
        }

        public static bool SonGirisYaz()
        {
            try
            {
                using (FileStream SonGirisDosyasi = new FileStream(SonGirisDosyaKonumu, FileMode.Create))
                {
                    using(StreamWriter SonGiris = new StreamWriter(SonGirisDosyasi))
                    {
                        SonGiris.Write(KullaniciEposta);
                    }
                }
                return true;
            }
            catch(IOException OY_HATA)
            {
                System.Windows.Forms.MessageBox.Show(OY_HATA.Message, "0020 Son Giriş Kaydı Dosya Okuma Yazma Hatası", System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Error);
                return false;
            }
            catch(Exception HATA)
            {
                System.Windows.Forms.MessageBox.Show(HATA.Message, "0021 Son Giriş Kaydı Hatası", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return false;
            }
        }
        public static bool SonGirisOku()
        {
            if (File.Exists(SonGirisDosyaKonumu))
            {
                try
                {
                    using (FileStream SonGirisDosyasi = new FileStream(SonGirisDosyaKonumu, FileMode.Open))
                    {
                        using (StreamReader SonGiris = new StreamReader(SonGirisDosyasi))
                        {
                            KullaniciEposta = SonGiris.ReadLine();
                            if (KullaniciEposta == null) return false;
                        }
                    }
                    return true;
                }
                catch (IOException OY_HATA)
                {
                    System.Windows.Forms.MessageBox.Show(OY_HATA.Message, "0022Son Giriş Kaydı Dosya Okuma Yazma Hatası", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
                catch (Exception HATA)
                {
                    System.Windows.Forms.MessageBox.Show(HATA.Message, "0023 Son Giriş Kaydı Hatası", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool ProfilKurtarma(string KULLANICI_EPOSTA, string KULLANICI_AD, string KULLANICI_SOYAD, int KULLANICI_IL, int KULLANICI_ILCE)
        {
            try
            {
                using (Bag = new SqlConnection(Anahtar))
                {
                    SqlCommand ProfilKontrol = new SqlCommand(SQLKomut.Listele(TUye, TKonum, $"{TUye}.Uye_EPosta", "Uye_EPosta", "Uye_Eposta", $"{TUye}.Uye_EPosta='{KULLANICI_EPOSTA}' and {TUye}.Uye_Ad='{KULLANICI_AD}' and {TUye}.Uye_Soyad='{KULLANICI_SOYAD}' and {TKonum}.IlNo={KULLANICI_IL} and {TKonum}.IlceNo={KULLANICI_ILCE}"), Bag);

                    Bag.Open();
                    SqlDataReader KontrolSonuc = ProfilKontrol.ExecuteReader();
                    KontrolSonuc.Read();

                    if (KontrolSonuc.HasRows)
                        return true;
                    else
                        return false;
                }
            }
            catch (SqlException VT_HATA)
            {
                System.Windows.Forms.MessageBox.Show(VT_HATA.Message, "0028 VeriTabanı Hatası Oluştu!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return false;
            }
            catch (Exception HATA)
            {
                System.Windows.Forms.MessageBox.Show(HATA.Message, "0029 Hata Oluştu!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool KonumListesiOlustur()
        {
            if (!KonumListesiKontrol())
            {
                try
                {
                    using (Bag = new SqlConnection(Anahtar))
                    {
                        if (!KonumListesiKontrol(true))
                        {
                            using (SqlDataAdapter Iller = new SqlDataAdapter(SQLKomut.Listele(TIl), Bag))
                            {
                                IlListesi = new DataTable();
                                Iller.Fill(IlListesi);
                            }
                        }
                        if (!KonumListesiKontrol(false))
                        {
                            using (SqlDataAdapter Ilceler = new SqlDataAdapter(SQLKomut.Listele(TIlce), Bag))
                            {
                                IlceListesi = new DataTable();
                                Ilceler.Fill(IlceListesi);
                            }
                        }

                        return true;
                    }
                }
                catch (SqlException VT_HATA)
                {
                    System.Windows.Forms.MessageBox.Show(VT_HATA.Message, "0030 VeriTabanı Hatası Oluştu!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
                catch (Exception HATA)
                {
                    System.Windows.Forms.MessageBox.Show(HATA.Message, "0031 Hata Oluştu!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
            }
            else return false;
        }
        public static bool KonumListesiKontrol(bool secim)
        {
            if (secim)
            {
                if (IlListesi != null)
                    if (IlListesi.ExtendedProperties.Count > 0) return true;
                    else return false;
                else return false;
            }
            else
            {
                if (IlceListesi != null)
                    if (IlceListesi.ExtendedProperties.Count > 0) return true;
                    else return false;
                else return false;
            }
        }
        public static bool KonumListesiKontrol()
        {
            if(IlListesi != null && IlceListesi != null)
                if (IlListesi.Columns.Count > 0 && IlceListesi.Columns.Count > 0) return true;
                else return false;
            else return false;

        }
        public static DataTable KonumListesi(bool secim)
        {
            if (secim)
            {
                return IlListesi;
            }
            else
            {
                return IlceListesi;
            }
        }
        public static bool AdminMi()
        {
            try
            {
                using (Bag = new SqlConnection(Anahtar))
                {
                    using (SqlCommand AdminKontrol = new SqlCommand(SQLKomut.Listele(TUye, "Uye_AdminMi", $"Uye_EPosta='{KullaniciEposta}' and Uye_AdminMi=1"), Bag))
                    {
                        Bag.Open();
                        SqlDataReader Sonuc = AdminKontrol.ExecuteReader();
                        if (Sonuc.HasRows) return true;
                        else return false;
                    }
                }
            }
            catch(Exception HATA)
            {
                System.Windows.Forms.MessageBox.Show($"Admin Kontrolünde Hata Meydana Geldi. Lütfen tekrar giriş yapınız. \n\t {HATA}", "0033 Hata Meydana Geldi!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool AnahtarOku()
        {
            string AnahtarDosyaYolu = "./Anahtar.txt";
            if (File.Exists(AnahtarDosyaYolu))
            {
                try
                {
                    using (FileStream AnahtarDosyasi = new FileStream(AnahtarDosyaYolu, FileMode.Open))
                    {
                        Anahtar = new StreamReader(AnahtarDosyasi).ReadLine();
                    }
                    return true;
                }
                catch (IOException OY_HATA)
                {
                    System.Windows.Forms.MessageBox.Show(OY_HATA.Message, "0034 Dosya Okuma Yazma Hatası Meydana Geldi!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
                catch (Exception HATA)
                {
                    System.Windows.Forms.MessageBox.Show(HATA.Message, "0035 Hata Meydana Geldi!", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
            }
            else return false;
        }
    }
}
