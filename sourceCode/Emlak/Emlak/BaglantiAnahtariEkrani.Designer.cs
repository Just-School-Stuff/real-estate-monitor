﻿namespace Emlak
{
    partial class BaglantiAnahtariEkrani
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaglantiAnahtariEkrani));
            this.Kutu_BaglantiAnahtari = new System.Windows.Forms.TextBox();
            this.Kutu_Aciklama = new System.Windows.Forms.Label();
            this.Buton_KontrolEt = new System.Windows.Forms.Button();
            this.Buton_IptalEt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Kutu_BaglantiAnahtari
            // 
            this.Kutu_BaglantiAnahtari.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Kutu_BaglantiAnahtari.Location = new System.Drawing.Point(93, 154);
            this.Kutu_BaglantiAnahtari.Name = "Kutu_BaglantiAnahtari";
            this.Kutu_BaglantiAnahtari.Size = new System.Drawing.Size(499, 26);
            this.Kutu_BaglantiAnahtari.TabIndex = 0;
            // 
            // Kutu_Aciklama
            // 
            this.Kutu_Aciklama.AutoEllipsis = true;
            this.Kutu_Aciklama.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.Kutu_Aciklama.Location = new System.Drawing.Point(12, 9);
            this.Kutu_Aciklama.Name = "Kutu_Aciklama";
            this.Kutu_Aciklama.Size = new System.Drawing.Size(641, 142);
            this.Kutu_Aciklama.TabIndex = 1;
            this.Kutu_Aciklama.Text = resources.GetString("Kutu_Aciklama.Text");
            this.Kutu_Aciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Buton_KontrolEt
            // 
            this.Buton_KontrolEt.Location = new System.Drawing.Point(414, 186);
            this.Buton_KontrolEt.Name = "Buton_KontrolEt";
            this.Buton_KontrolEt.Size = new System.Drawing.Size(106, 61);
            this.Buton_KontrolEt.TabIndex = 2;
            this.Buton_KontrolEt.Text = "Kontrol Et";
            this.Buton_KontrolEt.UseVisualStyleBackColor = true;
            this.Buton_KontrolEt.Click += new System.EventHandler(this.Buton_KontrolEt_Click);
            // 
            // Buton_IptalEt
            // 
            this.Buton_IptalEt.Location = new System.Drawing.Point(166, 186);
            this.Buton_IptalEt.Name = "Buton_IptalEt";
            this.Buton_IptalEt.Size = new System.Drawing.Size(106, 61);
            this.Buton_IptalEt.TabIndex = 3;
            this.Buton_IptalEt.Text = "İptal Et";
            this.Buton_IptalEt.UseVisualStyleBackColor = true;
            this.Buton_IptalEt.Click += new System.EventHandler(this.Buton_IptalEt_Click);
            // 
            // BaglantiAnahtariEkrani
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 255);
            this.Controls.Add(this.Buton_IptalEt);
            this.Controls.Add(this.Buton_KontrolEt);
            this.Controls.Add(this.Kutu_Aciklama);
            this.Controls.Add(this.Kutu_BaglantiAnahtari);
            this.MaximizeBox = false;
            this.Name = "BaglantiAnahtariEkrani";
            this.Text = "Baglantı Anahtarı";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Kutu_BaglantiAnahtari;
        private System.Windows.Forms.Label Kutu_Aciklama;
        private System.Windows.Forms.Button Buton_KontrolEt;
        private System.Windows.Forms.Button Buton_IptalEt;
    }
}