﻿/*
    * Copyright (c) 2018, MUHAMMED SAID BILGEHAN
    * All rights reserved.

    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
    * 1. Redistributions of source code must retain the above copyright
    *    notice, this list of conditions and the following disclaimer.
    * 2. Redistributions in binary form must reproduce the above copyright
    *    notice, this list of conditions and the following disclaimer in the
    *    documentation and/or other materials provided with the distribution.
    * 3. All advertising materials mentioning features or use of this software
    *    must take permission from MUHAMMED SAID BILGEHAN and must display the 
    *	   following acknowledgement:
    *    This product includes software developed by the MUHAMMED SAID BILGEHAN.
    * 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
    *    names of its contributors may be used to endorse or promote products
    *    derived from this software without specific prior written permission.

    * THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
    * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    * DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
    * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    * (INCLUDING NEGLİGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBLITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace Emlak
{
    static class SQLKomut
    {
        //-----------
        //  Ekle
        //-----------
        static public string Ekle(string TABLO, bool ADMIN, string EPOSTA, string SIFRE, string AD, string SOYAD)
        {
            return $"insert {TABLO} values ('{ADMIN}', '{EPOSTA}', '{SIFRE}', '{AD}', '{SOYAD}', @RESIM)";
        }
        static public string Ekle(string TABLO, string EPOSTA, int ILNO, int ILCENO)
        {
            return $"insert {TABLO} values ('{EPOSTA}', {ILNO}, {ILCENO})";
        }

        //-----------
        //  Listele
        //-----------
        static public string Listele(string TABLO)
        {
            return $"select * from {TABLO}";
        }
        static public string Listele(string TABLO, string FILITRE)
        {
            return $"select {FILITRE} from {TABLO}";
        }
        static public string Listele(string TABLO, string FILITRE, string SART)
        {
            return $"select {FILITRE} from {TABLO} where {SART}";
        }
        static public string Listele(string TABLO1, string TABLO2, string FILITRE, string TABLO1BAGLANTIELEMANI, string TABLO2BAGLANTIELEMANI)
        {
            return $"select {FILITRE} from {TABLO1} inner join {TABLO2} on {TABLO1}.{TABLO1BAGLANTIELEMANI}={TABLO2}.{TABLO2BAGLANTIELEMANI}";
        }
        static public string Listele(string TABLO1, string TABLO2, string FILITRE, string TABLO1BAGLANTIELEMANI, string TABLO2BAGLANTIELEMANI, string SART)
        {
            return $"select {FILITRE} from {TABLO1} inner join {TABLO2} on {TABLO1}.{TABLO1BAGLANTIELEMANI}={TABLO2}.{TABLO2BAGLANTIELEMANI} where {SART}";
        }

        //-----------
        //  Guncelle
        //-----------
        static public string Guncelle(string TABLO, string KOLON)
        {
            return $"update {TABLO} set {KOLON}";
        }
        static public string Guncelle(string TABLO, string KOLON, string SART)
        {
            return $"update {TABLO} set {KOLON} where {SART}";
        }
        static public string Guncelle(string TABLO, string KOLON, string SART, bool RESIM)
        {
            if (RESIM)
                return $"update {TABLO} set {KOLON}, Uye_Resim=@RESIM where {SART}";
            else
                return Guncelle(TABLO, KOLON, SART);
        }
        static public string ResimGuncelle(string TABLO, string SART, bool RESIM)
        {
            return $"update {TABLO} set Uye_Resim=@RESIM where {SART}";
        }

        //-----------
        //  Sil
        //-----------
        static public string Sil(string TABLO, string SART)
        {
            return $"delete from {TABLO} where {SART}";
        }

        //--------------------
        //  Tablo Islemleri
        //--------------------
        static public string FKOzellikEkle(string TABLO, string TABLODAKI, string TABLO_KOLON, string TABLODAKI_KOLON)
        {
            return $"Alter Table {TABLO} Add CONSTRAINT [FK_{TABLO}_{TABLODAKI}] FOREIGN KEY ([{TABLO_KOLON}]) REFERENCES [dbo].[{TABLODAKI}] ([{TABLODAKI_KOLON}])";
        }
        static public string FKOzellikSil(string TABLO, string TABLODAKI)
        {
            return $"Alter Table {TABLO} Drop Constraint FK_{TABLO}_{TABLODAKI}";
        }

    }
}
