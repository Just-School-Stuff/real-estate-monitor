﻿namespace Emlak
{
    partial class ProfilKurtarmaEkrani
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Kutu_Eposta = new System.Windows.Forms.TextBox();
            this.Kutu_EpostaAciklama = new System.Windows.Forms.Label();
            this.Kutu_AdAciklama = new System.Windows.Forms.Label();
            this.Kutu_Ad = new System.Windows.Forms.TextBox();
            this.Kutu_SoyadAciklama = new System.Windows.Forms.Label();
            this.Kutu_Soyad = new System.Windows.Forms.TextBox();
            this.Kutu_KonumAciklama = new System.Windows.Forms.Label();
            this.Kutu_SifreAciklama = new System.Windows.Forms.Label();
            this.Kutu_YeniSifre = new System.Windows.Forms.TextBox();
            this.Buton_KontrolEt = new System.Windows.Forms.Button();
            this.Kutu_Yonlendirme = new System.Windows.Forms.Label();
            this.Buton_Kaydet = new System.Windows.Forms.Button();
            this.ListeKutu_Ilce = new System.Windows.Forms.ComboBox();
            this.ListeKutu_Il = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(430, 73);
            this.label1.TabIndex = 0;
            this.label1.Text = "Profilinizin Şifresini kurtarmak için email adresiniz ile beraber Adınızı, Soyadı" +
    "nızı ve Konumunuzu Girmeniz Gerekmektedir";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_Eposta
            // 
            this.Kutu_Eposta.Location = new System.Drawing.Point(92, 133);
            this.Kutu_Eposta.Name = "Kutu_Eposta";
            this.Kutu_Eposta.Size = new System.Drawing.Size(139, 20);
            this.Kutu_Eposta.TabIndex = 1;
            // 
            // Kutu_EpostaAciklama
            // 
            this.Kutu_EpostaAciklama.AutoSize = true;
            this.Kutu_EpostaAciklama.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Kutu_EpostaAciklama.Location = new System.Drawing.Point(15, 131);
            this.Kutu_EpostaAciklama.Name = "Kutu_EpostaAciklama";
            this.Kutu_EpostaAciklama.Size = new System.Drawing.Size(71, 20);
            this.Kutu_EpostaAciklama.TabIndex = 2;
            this.Kutu_EpostaAciklama.Text = "Eposta:";
            this.Kutu_EpostaAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_AdAciklama
            // 
            this.Kutu_AdAciklama.AutoSize = true;
            this.Kutu_AdAciklama.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Kutu_AdAciklama.Location = new System.Drawing.Point(50, 157);
            this.Kutu_AdAciklama.Name = "Kutu_AdAciklama";
            this.Kutu_AdAciklama.Size = new System.Drawing.Size(36, 20);
            this.Kutu_AdAciklama.TabIndex = 4;
            this.Kutu_AdAciklama.Text = "Ad:";
            this.Kutu_AdAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_Ad
            // 
            this.Kutu_Ad.Location = new System.Drawing.Point(92, 159);
            this.Kutu_Ad.Name = "Kutu_Ad";
            this.Kutu_Ad.Size = new System.Drawing.Size(139, 20);
            this.Kutu_Ad.TabIndex = 3;
            // 
            // Kutu_SoyadAciklama
            // 
            this.Kutu_SoyadAciklama.AutoSize = true;
            this.Kutu_SoyadAciklama.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Kutu_SoyadAciklama.Location = new System.Drawing.Point(22, 183);
            this.Kutu_SoyadAciklama.Name = "Kutu_SoyadAciklama";
            this.Kutu_SoyadAciklama.Size = new System.Drawing.Size(64, 20);
            this.Kutu_SoyadAciklama.TabIndex = 6;
            this.Kutu_SoyadAciklama.Text = "Soyad:";
            this.Kutu_SoyadAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_Soyad
            // 
            this.Kutu_Soyad.Location = new System.Drawing.Point(92, 185);
            this.Kutu_Soyad.Name = "Kutu_Soyad";
            this.Kutu_Soyad.Size = new System.Drawing.Size(139, 20);
            this.Kutu_Soyad.TabIndex = 5;
            // 
            // Kutu_KonumAciklama
            // 
            this.Kutu_KonumAciklama.AutoSize = true;
            this.Kutu_KonumAciklama.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Kutu_KonumAciklama.Location = new System.Drawing.Point(15, 211);
            this.Kutu_KonumAciklama.Name = "Kutu_KonumAciklama";
            this.Kutu_KonumAciklama.Size = new System.Drawing.Size(69, 20);
            this.Kutu_KonumAciklama.TabIndex = 8;
            this.Kutu_KonumAciklama.Text = "Konum:";
            this.Kutu_KonumAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_SifreAciklama
            // 
            this.Kutu_SifreAciklama.AutoSize = true;
            this.Kutu_SifreAciklama.Enabled = false;
            this.Kutu_SifreAciklama.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Kutu_SifreAciklama.Location = new System.Drawing.Point(288, 157);
            this.Kutu_SifreAciklama.Name = "Kutu_SifreAciklama";
            this.Kutu_SifreAciklama.Size = new System.Drawing.Size(116, 20);
            this.Kutu_SifreAciklama.TabIndex = 10;
            this.Kutu_SifreAciklama.Text = "Yeni Şifreniz:";
            this.Kutu_SifreAciklama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Kutu_YeniSifre
            // 
            this.Kutu_YeniSifre.Enabled = false;
            this.Kutu_YeniSifre.Location = new System.Drawing.Point(276, 180);
            this.Kutu_YeniSifre.Name = "Kutu_YeniSifre";
            this.Kutu_YeniSifre.Size = new System.Drawing.Size(139, 20);
            this.Kutu_YeniSifre.TabIndex = 9;
            // 
            // Buton_KontrolEt
            // 
            this.Buton_KontrolEt.Location = new System.Drawing.Point(121, 267);
            this.Buton_KontrolEt.Name = "Buton_KontrolEt";
            this.Buton_KontrolEt.Size = new System.Drawing.Size(74, 36);
            this.Buton_KontrolEt.TabIndex = 11;
            this.Buton_KontrolEt.Text = "Kontrol Et";
            this.Buton_KontrolEt.UseVisualStyleBackColor = true;
            this.Buton_KontrolEt.Click += new System.EventHandler(this.Buton_KontrolEt_Click);
            // 
            // Kutu_Yonlendirme
            // 
            this.Kutu_Yonlendirme.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Kutu_Yonlendirme.Location = new System.Drawing.Point(16, 82);
            this.Kutu_Yonlendirme.Name = "Kutu_Yonlendirme";
            this.Kutu_Yonlendirme.Size = new System.Drawing.Size(426, 48);
            this.Kutu_Yonlendirme.TabIndex = 12;
            this.Kutu_Yonlendirme.Text = "Kontrol Edebilmemiz için Tüm Alanları Doldurmanız Gerekmektedir.";
            this.Kutu_Yonlendirme.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Kutu_Yonlendirme.Visible = false;
            // 
            // Buton_Kaydet
            // 
            this.Buton_Kaydet.Enabled = false;
            this.Buton_Kaydet.Location = new System.Drawing.Point(301, 237);
            this.Buton_Kaydet.Name = "Buton_Kaydet";
            this.Buton_Kaydet.Size = new System.Drawing.Size(74, 36);
            this.Buton_Kaydet.TabIndex = 13;
            this.Buton_Kaydet.Text = "Kaydet";
            this.Buton_Kaydet.UseVisualStyleBackColor = true;
            this.Buton_Kaydet.Click += new System.EventHandler(this.Buton_Kaydet_Click);
            // 
            // ListeKutu_Ilce
            // 
            this.ListeKutu_Ilce.BackColor = System.Drawing.SystemColors.Window;
            this.ListeKutu_Ilce.FormattingEnabled = true;
            this.ListeKutu_Ilce.Location = new System.Drawing.Point(91, 240);
            this.ListeKutu_Ilce.Name = "ListeKutu_Ilce";
            this.ListeKutu_Ilce.Size = new System.Drawing.Size(140, 21);
            this.ListeKutu_Ilce.Sorted = true;
            this.ListeKutu_Ilce.TabIndex = 31;
            this.ListeKutu_Ilce.Tag = "İl/İlçe/Semt";
            // 
            // ListeKutu_Il
            // 
            this.ListeKutu_Il.BackColor = System.Drawing.SystemColors.Window;
            this.ListeKutu_Il.FormattingEnabled = true;
            this.ListeKutu_Il.Location = new System.Drawing.Point(92, 213);
            this.ListeKutu_Il.Name = "ListeKutu_Il";
            this.ListeKutu_Il.Size = new System.Drawing.Size(139, 21);
            this.ListeKutu_Il.Sorted = true;
            this.ListeKutu_Il.TabIndex = 30;
            this.ListeKutu_Il.Tag = "İl/İlçe/Semt";
            this.ListeKutu_Il.SelectedIndexChanged += new System.EventHandler(this.ListeKutu_Il_SelectedIndexChanged);
            // 
            // ProfilKurtarmaEkrani
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 339);
            this.Controls.Add(this.ListeKutu_Ilce);
            this.Controls.Add(this.ListeKutu_Il);
            this.Controls.Add(this.Buton_Kaydet);
            this.Controls.Add(this.Kutu_Yonlendirme);
            this.Controls.Add(this.Buton_KontrolEt);
            this.Controls.Add(this.Kutu_SifreAciklama);
            this.Controls.Add(this.Kutu_YeniSifre);
            this.Controls.Add(this.Kutu_KonumAciklama);
            this.Controls.Add(this.Kutu_SoyadAciklama);
            this.Controls.Add(this.Kutu_Soyad);
            this.Controls.Add(this.Kutu_AdAciklama);
            this.Controls.Add(this.Kutu_Ad);
            this.Controls.Add(this.Kutu_EpostaAciklama);
            this.Controls.Add(this.Kutu_Eposta);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProfilKurtarmaEkrani";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Profil Kurtarma";
            this.Load += new System.EventHandler(this.ProfilKurtarmaEkrani_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Kutu_Eposta;
        private System.Windows.Forms.Label Kutu_EpostaAciklama;
        private System.Windows.Forms.Label Kutu_AdAciklama;
        private System.Windows.Forms.TextBox Kutu_Ad;
        private System.Windows.Forms.Label Kutu_SoyadAciklama;
        private System.Windows.Forms.TextBox Kutu_Soyad;
        private System.Windows.Forms.Label Kutu_KonumAciklama;
        private System.Windows.Forms.Label Kutu_SifreAciklama;
        private System.Windows.Forms.TextBox Kutu_YeniSifre;
        private System.Windows.Forms.Button Buton_KontrolEt;
        private System.Windows.Forms.Label Kutu_Yonlendirme;
        private System.Windows.Forms.Button Buton_Kaydet;
        private System.Windows.Forms.ComboBox ListeKutu_Ilce;
        private System.Windows.Forms.ComboBox ListeKutu_Il;
    }
}