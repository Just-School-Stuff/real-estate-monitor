﻿namespace Emlak
{
    partial class ProfilEkrani
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProfilEkrani));
            this.Kutu_ProfilResim = new System.Windows.Forms.PictureBox();
            this.Kutu_ProfilEposta = new System.Windows.Forms.TextBox();
            this.Kutu_ProfilAd = new System.Windows.Forms.TextBox();
            this.Kutu_ProfilSifre = new System.Windows.Forms.TextBox();
            this.Buton_ResimDegistir = new System.Windows.Forms.Button();
            this.Buton_ProfilKaydet = new System.Windows.Forms.Button();
            this.Buton_Varsayılanlar = new System.Windows.Forms.Button();
            this.Buton_EpostaVars = new System.Windows.Forms.Button();
            this.IconListesi = new System.Windows.Forms.ImageList(this.components);
            this.Buton_AdVars = new System.Windows.Forms.Button();
            this.Buton_SoyadVars = new System.Windows.Forms.Button();
            this.Buton_IlVars = new System.Windows.Forms.Button();
            this.Buton_ResimVars = new System.Windows.Forms.Button();
            this.Buton_Geri = new System.Windows.Forms.Button();
            this.Buton_ProfiliSil = new System.Windows.Forms.Button();
            this.Kutu_Cikti = new System.Windows.Forms.Label();
            this.ListeKutu_Ilce = new System.Windows.Forms.ComboBox();
            this.ListeKutu_Il = new System.Windows.Forms.ComboBox();
            this.Kutu_ProfilSoyad = new System.Windows.Forms.TextBox();
            this.Buton_IlceVars = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_ProfilResim)).BeginInit();
            this.SuspendLayout();
            // 
            // Kutu_ProfilResim
            // 
            this.Kutu_ProfilResim.Location = new System.Drawing.Point(115, 12);
            this.Kutu_ProfilResim.Name = "Kutu_ProfilResim";
            this.Kutu_ProfilResim.Size = new System.Drawing.Size(126, 126);
            this.Kutu_ProfilResim.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Kutu_ProfilResim.TabIndex = 0;
            this.Kutu_ProfilResim.TabStop = false;
            // 
            // Kutu_ProfilEposta
            // 
            this.Kutu_ProfilEposta.BackColor = System.Drawing.SystemColors.Control;
            this.Kutu_ProfilEposta.Location = new System.Drawing.Point(85, 174);
            this.Kutu_ProfilEposta.Name = "Kutu_ProfilEposta";
            this.Kutu_ProfilEposta.Size = new System.Drawing.Size(183, 20);
            this.Kutu_ProfilEposta.TabIndex = 1;
            this.Kutu_ProfilEposta.TextChanged += new System.EventHandler(this.Kutu_ProfilEposta_TextChanged);
            // 
            // Kutu_ProfilAd
            // 
            this.Kutu_ProfilAd.BackColor = System.Drawing.SystemColors.Control;
            this.Kutu_ProfilAd.Location = new System.Drawing.Point(86, 226);
            this.Kutu_ProfilAd.Name = "Kutu_ProfilAd";
            this.Kutu_ProfilAd.Size = new System.Drawing.Size(183, 20);
            this.Kutu_ProfilAd.TabIndex = 2;
            this.Kutu_ProfilAd.TextChanged += new System.EventHandler(this.Kutu_ProfilAd_TextChanged);
            // 
            // Kutu_ProfilSifre
            // 
            this.Kutu_ProfilSifre.BackColor = System.Drawing.SystemColors.Control;
            this.Kutu_ProfilSifre.Location = new System.Drawing.Point(86, 200);
            this.Kutu_ProfilSifre.Name = "Kutu_ProfilSifre";
            this.Kutu_ProfilSifre.Size = new System.Drawing.Size(183, 20);
            this.Kutu_ProfilSifre.TabIndex = 5;
            this.Kutu_ProfilSifre.TextChanged += new System.EventHandler(this.Kutu_ProfilSifre_TextChanged);
            // 
            // Buton_ResimDegistir
            // 
            this.Buton_ResimDegistir.Location = new System.Drawing.Point(131, 144);
            this.Buton_ResimDegistir.Name = "Buton_ResimDegistir";
            this.Buton_ResimDegistir.Size = new System.Drawing.Size(94, 24);
            this.Buton_ResimDegistir.TabIndex = 6;
            this.Buton_ResimDegistir.Text = "Resmi Değiştir";
            this.Buton_ResimDegistir.UseVisualStyleBackColor = true;
            this.Buton_ResimDegistir.Click += new System.EventHandler(this.Buton_ResimDegistir_Click);
            // 
            // Buton_ProfilKaydet
            // 
            this.Buton_ProfilKaydet.Location = new System.Drawing.Point(174, 333);
            this.Buton_ProfilKaydet.Name = "Buton_ProfilKaydet";
            this.Buton_ProfilKaydet.Size = new System.Drawing.Size(94, 51);
            this.Buton_ProfilKaydet.TabIndex = 7;
            this.Buton_ProfilKaydet.Text = "Değişiklikleri Kaydet";
            this.Buton_ProfilKaydet.UseVisualStyleBackColor = true;
            this.Buton_ProfilKaydet.Click += new System.EventHandler(this.Buton_ProfilKaydet_Click);
            // 
            // Buton_Varsayılanlar
            // 
            this.Buton_Varsayılanlar.Location = new System.Drawing.Point(85, 332);
            this.Buton_Varsayılanlar.Name = "Buton_Varsayılanlar";
            this.Buton_Varsayılanlar.Size = new System.Drawing.Size(83, 52);
            this.Buton_Varsayılanlar.TabIndex = 8;
            this.Buton_Varsayılanlar.Text = "Varsayılanlara Döndür";
            this.Buton_Varsayılanlar.UseVisualStyleBackColor = true;
            this.Buton_Varsayılanlar.Click += new System.EventHandler(this.Buton_Varsayılanlar_Click);
            // 
            // Buton_EpostaVars
            // 
            this.Buton_EpostaVars.FlatAppearance.BorderSize = 0;
            this.Buton_EpostaVars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_EpostaVars.ImageIndex = 0;
            this.Buton_EpostaVars.ImageList = this.IconListesi;
            this.Buton_EpostaVars.Location = new System.Drawing.Point(274, 174);
            this.Buton_EpostaVars.Name = "Buton_EpostaVars";
            this.Buton_EpostaVars.Size = new System.Drawing.Size(24, 20);
            this.Buton_EpostaVars.TabIndex = 9;
            this.Buton_EpostaVars.UseVisualStyleBackColor = true;
            this.Buton_EpostaVars.Click += new System.EventHandler(this.Buton_EpostaVars_Click);
            // 
            // IconListesi
            // 
            this.IconListesi.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IconListesi.ImageStream")));
            this.IconListesi.TransparentColor = System.Drawing.Color.Transparent;
            this.IconListesi.Images.SetKeyName(0, "history_button.png");
            // 
            // Buton_AdVars
            // 
            this.Buton_AdVars.FlatAppearance.BorderSize = 0;
            this.Buton_AdVars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_AdVars.ImageIndex = 0;
            this.Buton_AdVars.ImageList = this.IconListesi;
            this.Buton_AdVars.Location = new System.Drawing.Point(274, 226);
            this.Buton_AdVars.Name = "Buton_AdVars";
            this.Buton_AdVars.Size = new System.Drawing.Size(24, 20);
            this.Buton_AdVars.TabIndex = 11;
            this.Buton_AdVars.UseVisualStyleBackColor = true;
            this.Buton_AdVars.Click += new System.EventHandler(this.Buton_AdVars_Click);
            // 
            // Buton_SoyadVars
            // 
            this.Buton_SoyadVars.FlatAppearance.BorderSize = 0;
            this.Buton_SoyadVars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_SoyadVars.ImageIndex = 0;
            this.Buton_SoyadVars.ImageList = this.IconListesi;
            this.Buton_SoyadVars.Location = new System.Drawing.Point(274, 252);
            this.Buton_SoyadVars.Name = "Buton_SoyadVars";
            this.Buton_SoyadVars.Size = new System.Drawing.Size(24, 20);
            this.Buton_SoyadVars.TabIndex = 12;
            this.Buton_SoyadVars.UseVisualStyleBackColor = true;
            this.Buton_SoyadVars.Click += new System.EventHandler(this.Buton_SoyadVars_Click);
            // 
            // Buton_IlVars
            // 
            this.Buton_IlVars.FlatAppearance.BorderSize = 0;
            this.Buton_IlVars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_IlVars.ImageIndex = 0;
            this.Buton_IlVars.ImageList = this.IconListesi;
            this.Buton_IlVars.Location = new System.Drawing.Point(274, 277);
            this.Buton_IlVars.Name = "Buton_IlVars";
            this.Buton_IlVars.Size = new System.Drawing.Size(24, 20);
            this.Buton_IlVars.TabIndex = 13;
            this.Buton_IlVars.UseVisualStyleBackColor = true;
            this.Buton_IlVars.Click += new System.EventHandler(this.Buton_IlVars_Click);
            // 
            // Buton_ResimVars
            // 
            this.Buton_ResimVars.FlatAppearance.BorderSize = 0;
            this.Buton_ResimVars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_ResimVars.ImageIndex = 0;
            this.Buton_ResimVars.ImageList = this.IconListesi;
            this.Buton_ResimVars.Location = new System.Drawing.Point(275, 62);
            this.Buton_ResimVars.Name = "Buton_ResimVars";
            this.Buton_ResimVars.Size = new System.Drawing.Size(24, 20);
            this.Buton_ResimVars.TabIndex = 14;
            this.Buton_ResimVars.UseVisualStyleBackColor = true;
            this.Buton_ResimVars.Click += new System.EventHandler(this.Buton_ResimVars_Click);
            // 
            // Buton_Geri
            // 
            this.Buton_Geri.Location = new System.Drawing.Point(12, 12);
            this.Buton_Geri.Name = "Buton_Geri";
            this.Buton_Geri.Size = new System.Drawing.Size(54, 40);
            this.Buton_Geri.TabIndex = 15;
            this.Buton_Geri.Text = "Geri";
            this.Buton_Geri.UseVisualStyleBackColor = true;
            this.Buton_Geri.Click += new System.EventHandler(this.Buton_Geri_Click);
            // 
            // Buton_ProfiliSil
            // 
            this.Buton_ProfiliSil.Location = new System.Drawing.Point(300, 12);
            this.Buton_ProfiliSil.Name = "Buton_ProfiliSil";
            this.Buton_ProfiliSil.Size = new System.Drawing.Size(54, 40);
            this.Buton_ProfiliSil.TabIndex = 16;
            this.Buton_ProfiliSil.Text = "Profili Sil";
            this.Buton_ProfiliSil.UseVisualStyleBackColor = true;
            this.Buton_ProfiliSil.Click += new System.EventHandler(this.Buton_ProfiliSil_Click);
            // 
            // Kutu_Cikti
            // 
            this.Kutu_Cikti.Location = new System.Drawing.Point(85, 391);
            this.Kutu_Cikti.Name = "Kutu_Cikti";
            this.Kutu_Cikti.Size = new System.Drawing.Size(183, 39);
            this.Kutu_Cikti.TabIndex = 17;
            this.Kutu_Cikti.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ListeKutu_Ilce
            // 
            this.ListeKutu_Ilce.BackColor = System.Drawing.SystemColors.Window;
            this.ListeKutu_Ilce.FormattingEnabled = true;
            this.ListeKutu_Ilce.Location = new System.Drawing.Point(84, 305);
            this.ListeKutu_Ilce.Name = "ListeKutu_Ilce";
            this.ListeKutu_Ilce.Size = new System.Drawing.Size(184, 21);
            this.ListeKutu_Ilce.Sorted = true;
            this.ListeKutu_Ilce.TabIndex = 29;
            this.ListeKutu_Ilce.Tag = "İl/İlçe/Semt";
            this.ListeKutu_Ilce.SelectedIndexChanged += new System.EventHandler(this.ListeKutu_Ilce_SelectedIndexChanged);
            // 
            // ListeKutu_Il
            // 
            this.ListeKutu_Il.BackColor = System.Drawing.SystemColors.Window;
            this.ListeKutu_Il.FormattingEnabled = true;
            this.ListeKutu_Il.Location = new System.Drawing.Point(85, 278);
            this.ListeKutu_Il.Name = "ListeKutu_Il";
            this.ListeKutu_Il.Size = new System.Drawing.Size(183, 21);
            this.ListeKutu_Il.Sorted = true;
            this.ListeKutu_Il.TabIndex = 28;
            this.ListeKutu_Il.Tag = "İl/İlçe/Semt";
            this.ListeKutu_Il.SelectedIndexChanged += new System.EventHandler(this.ListeKutu_Il_SelectedIndexChanged);
            // 
            // Kutu_ProfilSoyad
            // 
            this.Kutu_ProfilSoyad.BackColor = System.Drawing.SystemColors.Control;
            this.Kutu_ProfilSoyad.Location = new System.Drawing.Point(86, 252);
            this.Kutu_ProfilSoyad.Name = "Kutu_ProfilSoyad";
            this.Kutu_ProfilSoyad.Size = new System.Drawing.Size(183, 20);
            this.Kutu_ProfilSoyad.TabIndex = 3;
            this.Kutu_ProfilSoyad.TextChanged += new System.EventHandler(this.Kutu_ProfilSoyad_TextChanged);
            // 
            // Buton_IlceVars
            // 
            this.Buton_IlceVars.FlatAppearance.BorderSize = 0;
            this.Buton_IlceVars.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_IlceVars.ImageIndex = 0;
            this.Buton_IlceVars.ImageList = this.IconListesi;
            this.Buton_IlceVars.Location = new System.Drawing.Point(274, 304);
            this.Buton_IlceVars.Name = "Buton_IlceVars";
            this.Buton_IlceVars.Size = new System.Drawing.Size(24, 20);
            this.Buton_IlceVars.TabIndex = 30;
            this.Buton_IlceVars.UseVisualStyleBackColor = true;
            this.Buton_IlceVars.Click += new System.EventHandler(this.Buton_IlceVars_Click);
            // 
            // ProfilEkrani
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 429);
            this.Controls.Add(this.Buton_IlceVars);
            this.Controls.Add(this.ListeKutu_Ilce);
            this.Controls.Add(this.ListeKutu_Il);
            this.Controls.Add(this.Kutu_Cikti);
            this.Controls.Add(this.Buton_ProfiliSil);
            this.Controls.Add(this.Buton_Geri);
            this.Controls.Add(this.Buton_ResimVars);
            this.Controls.Add(this.Buton_IlVars);
            this.Controls.Add(this.Buton_SoyadVars);
            this.Controls.Add(this.Buton_AdVars);
            this.Controls.Add(this.Buton_EpostaVars);
            this.Controls.Add(this.Buton_Varsayılanlar);
            this.Controls.Add(this.Buton_ProfilKaydet);
            this.Controls.Add(this.Buton_ResimDegistir);
            this.Controls.Add(this.Kutu_ProfilSifre);
            this.Controls.Add(this.Kutu_ProfilSoyad);
            this.Controls.Add(this.Kutu_ProfilAd);
            this.Controls.Add(this.Kutu_ProfilEposta);
            this.Controls.Add(this.Kutu_ProfilResim);
            this.Name = "ProfilEkrani";
            this.Text = "AdminPaneliEkrani";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProfilEkrani_FormClosing);
            this.Load += new System.EventHandler(this.ProfilEkrani_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Kutu_ProfilResim)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Kutu_ProfilResim;
        private System.Windows.Forms.TextBox Kutu_ProfilEposta;
        private System.Windows.Forms.TextBox Kutu_ProfilAd;
        private System.Windows.Forms.TextBox Kutu_ProfilSifre;
        private System.Windows.Forms.Button Buton_ResimDegistir;
        private System.Windows.Forms.Button Buton_ProfilKaydet;
        private System.Windows.Forms.Button Buton_Varsayılanlar;
        private System.Windows.Forms.Button Buton_EpostaVars;
        private System.Windows.Forms.ImageList IconListesi;
        private System.Windows.Forms.Button Buton_AdVars;
        private System.Windows.Forms.Button Buton_SoyadVars;
        private System.Windows.Forms.Button Buton_IlVars;
        private System.Windows.Forms.Button Buton_ResimVars;
        private System.Windows.Forms.Button Buton_Geri;
        private System.Windows.Forms.Button Buton_ProfiliSil;
        private System.Windows.Forms.Label Kutu_Cikti;
        private System.Windows.Forms.ComboBox ListeKutu_Ilce;
        private System.Windows.Forms.ComboBox ListeKutu_Il;
        private System.Windows.Forms.TextBox Kutu_ProfilSoyad;
        private System.Windows.Forms.Button Buton_IlceVars;
    }
}