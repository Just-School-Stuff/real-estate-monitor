﻿CREATE TABLE [dbo].[Tablo_Konum]
(
	[Konum] NVARCHAR(50) NOT NULL PRIMARY KEY, 
    [Uye_Eposta] NVARCHAR(50) NOT NULL, 
    [Uye_No] INT NOT NULL, 
    CONSTRAINT [FK_Tablo_Konum<Tablo_Uye-Uye_Eposta] FOREIGN KEY ([Uye_Eposta]) REFERENCES [Tablo_Uye]([Uye_Eposta]), 
	CONSTRAINT [FK_Tablo_Konum<Tablo_Uye-Uye_No] FOREIGN KEY ([Uye_No]) REFERENCES [Tablo_Uye]([Uye_No]) 
)
