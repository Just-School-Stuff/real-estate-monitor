﻿CREATE TABLE [dbo].[Tablo_Uye]
( 
    [Uye_No] INT NOT NULL,
	[Uye_EPosta] NVARCHAR(50) NOT NULL PRIMARY KEY, 
    [Uye_Sifre] NCHAR(25) NOT NULL, 
    [Uye_Ad] NCHAR(10) NOT NULL, 
    [Uye_Soyad] NCHAR(15) NOT NULL, 
    [Uye_Resim] IMAGE NOT NULL
)
