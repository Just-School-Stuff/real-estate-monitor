﻿/*
    * Copyright (c) 2018, MUHAMMED SAID BILGEHAN
    * All rights reserved.

    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
    * 1. Redistributions of source code must retain the above copyright
    *    notice, this list of conditions and the following disclaimer.
    * 2. Redistributions in binary form must reproduce the above copyright
    *    notice, this list of conditions and the following disclaimer in the
    *    documentation and/or other materials provided with the distribution.
    * 3. All advertising materials mentioning features or use of this software
    *    must take permission from MUHAMMED SAID BILGEHAN and must display the 
    *	   following acknowledgement:
    *    This product includes software developed by the MUHAMMED SAID BILGEHAN.
    * 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
    *    names of its contributors may be used to endorse or promote products
    *    derived from this software without specific prior written permission.

    * THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
    * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    * DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
    * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    * (INCLUDING NEGLİGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBLITY OF SUCH DAMAGE.
 */

using System;
using System.IO;
using System.Windows.Forms;

namespace Emlak
{
    public partial class ProfilSilEkrani : Form
    {
        public ProfilSilEkrani()
        {
            InitializeComponent();
        }

        private void Buton_Iptal_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Buton_Sil_Click(object sender, EventArgs e)
        {
            if(KullaniciVerisi.ProfilSil(Kutu_Eposta.Text, Kutu_Sifre.Text))
            {
                try
                {
                    File.Delete("./SonGiris.txt");
                }
                finally
                {
                    MessageBox.Show("Profil Başarıyla Silinmiştir!", "Profil Silindi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
            }

            else
                MessageBox.Show("Hatalı Eposta veya Şifre, Lütfen Tekrar Deneyiniz!", "Profil Silinemedi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
