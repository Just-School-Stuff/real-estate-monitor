﻿namespace Emlak
{
    partial class Form_AnaMenu
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_AnaMenu));
            this.Buton_ProfilEkrani = new System.Windows.Forms.Button();
            this.IconListesi = new System.Windows.Forms.ImageList(this.components);
            this.Buton_Cikis = new System.Windows.Forms.Button();
            this.Kutu_Eposta = new System.Windows.Forms.Label();
            this.Buton_AdminEkrani = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Buton_ProfilEkrani
            // 
            this.Buton_ProfilEkrani.AutoSize = true;
            this.Buton_ProfilEkrani.BackColor = System.Drawing.Color.Transparent;
            this.Buton_ProfilEkrani.FlatAppearance.BorderSize = 0;
            this.Buton_ProfilEkrani.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_ProfilEkrani.ImageIndex = 0;
            this.Buton_ProfilEkrani.ImageList = this.IconListesi;
            this.Buton_ProfilEkrani.Location = new System.Drawing.Point(12, 12);
            this.Buton_ProfilEkrani.Name = "Buton_ProfilEkrani";
            this.Buton_ProfilEkrani.Size = new System.Drawing.Size(38, 38);
            this.Buton_ProfilEkrani.TabIndex = 0;
            this.Buton_ProfilEkrani.UseVisualStyleBackColor = false;
            this.Buton_ProfilEkrani.Click += new System.EventHandler(this.Buton_ProfilEkrani_Click);
            // 
            // IconListesi
            // 
            this.IconListesi.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IconListesi.ImageStream")));
            this.IconListesi.TransparentColor = System.Drawing.Color.Transparent;
            this.IconListesi.Images.SetKeyName(0, "Profil.png");
            this.IconListesi.Images.SetKeyName(1, "PowerButton.png");
            this.IconListesi.Images.SetKeyName(2, "Info_Simple_bw.svg.png");
            // 
            // Buton_Cikis
            // 
            this.Buton_Cikis.AutoSize = true;
            this.Buton_Cikis.BackColor = System.Drawing.Color.Transparent;
            this.Buton_Cikis.FlatAppearance.BorderSize = 0;
            this.Buton_Cikis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_Cikis.ImageIndex = 1;
            this.Buton_Cikis.ImageList = this.IconListesi;
            this.Buton_Cikis.Location = new System.Drawing.Point(361, 12);
            this.Buton_Cikis.Name = "Buton_Cikis";
            this.Buton_Cikis.Size = new System.Drawing.Size(38, 38);
            this.Buton_Cikis.TabIndex = 1;
            this.Buton_Cikis.UseVisualStyleBackColor = false;
            this.Buton_Cikis.Click += new System.EventHandler(this.Buton_Cikis_Click);
            // 
            // Kutu_Eposta
            // 
            this.Kutu_Eposta.Location = new System.Drawing.Point(56, 12);
            this.Kutu_Eposta.Name = "Kutu_Eposta";
            this.Kutu_Eposta.Size = new System.Drawing.Size(299, 38);
            this.Kutu_Eposta.TabIndex = 2;
            // 
            // Buton_AdminEkrani
            // 
            this.Buton_AdminEkrani.AutoSize = true;
            this.Buton_AdminEkrani.BackColor = System.Drawing.Color.Transparent;
            this.Buton_AdminEkrani.Enabled = false;
            this.Buton_AdminEkrani.FlatAppearance.BorderSize = 0;
            this.Buton_AdminEkrani.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Buton_AdminEkrani.ImageIndex = 2;
            this.Buton_AdminEkrani.ImageList = this.IconListesi;
            this.Buton_AdminEkrani.Location = new System.Drawing.Point(12, 56);
            this.Buton_AdminEkrani.Name = "Buton_AdminEkrani";
            this.Buton_AdminEkrani.Size = new System.Drawing.Size(38, 38);
            this.Buton_AdminEkrani.TabIndex = 3;
            this.Buton_AdminEkrani.UseVisualStyleBackColor = false;
            this.Buton_AdminEkrani.Visible = false;
            this.Buton_AdminEkrani.Click += new System.EventHandler(this.Buton_AdminEkrani_Click);
            // 
            // Form_AnaMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 309);
            this.Controls.Add(this.Buton_AdminEkrani);
            this.Controls.Add(this.Kutu_Eposta);
            this.Controls.Add(this.Buton_Cikis);
            this.Controls.Add(this.Buton_ProfilEkrani);
            this.MaximizeBox = false;
            this.Name = "Form_AnaMenu";
            this.Text = "Ana Menü";
            this.Load += new System.EventHandler(this.Form_AnaMenu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Buton_ProfilEkrani;
        private System.Windows.Forms.ImageList IconListesi;
        private System.Windows.Forms.Button Buton_Cikis;
        private System.Windows.Forms.Label Kutu_Eposta;
        private System.Windows.Forms.Button Buton_AdminEkrani;
    }
}

