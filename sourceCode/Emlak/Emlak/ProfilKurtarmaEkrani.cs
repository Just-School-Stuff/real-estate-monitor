﻿/*
    * Copyright (c) 2018, MUHAMMED SAID BILGEHAN
    * All rights reserved.

    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
    * 1. Redistributions of source code must retain the above copyright
    *    notice, this list of conditions and the following disclaimer.
    * 2. Redistributions in binary form must reproduce the above copyright
    *    notice, this list of conditions and the following disclaimer in the
    *    documentation and/or other materials provided with the distribution.
    * 3. All advertising materials mentioning features or use of this software
    *    must take permission from MUHAMMED SAID BILGEHAN and must display the 
    *	   following acknowledgement:
    *    This product includes software developed by the MUHAMMED SAID BILGEHAN.
    * 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
    *    names of its contributors may be used to endorse or promote products
    *    derived from this software without specific prior written permission.

    * THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
    * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    * DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
    * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    * (INCLUDING NEGLİGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBLITY OF SUCH DAMAGE.
 */

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Emlak
{
    public partial class ProfilKurtarmaEkrani : Form
    {
        private readonly string B = KullaniciVerisi.Anahtar;
        private readonly string T = "Tablo_Uye";
        DataTable IlListesi;
        DataTable IlceListesi;

        public ProfilKurtarmaEkrani()
        {
            InitializeComponent();
        }

        private void ProfilKurtarmaEkrani_Load(object sender, EventArgs e)
        {
            KonumAl();
        }

        private void KonumAl()
        {
            if (KullaniciVerisi.KonumListesiKontrol())
            {
                IlceListesi = KullaniciVerisi.KonumListesi(false);
                ListeKutu_Ilce.Sorted = false;
                ListeKutu_Ilce.DataSource = IlceListesi;
                ListeKutu_Ilce.DisplayMember = "Ilce";
                ListeKutu_Ilce.ValueMember = "IlceNo";

                IlListesi = KullaniciVerisi.KonumListesi(true);
                ListeKutu_Il.DisplayMember = "Il";
                ListeKutu_Il.ValueMember = "IlNo";
                ListeKutu_Il.Sorted = false;
                ListeKutu_Il.DataSource = IlListesi;

                IlceListesi.DefaultView.RowFilter = $"Bagli_IlNo = {ListeKutu_Il.SelectedValue}";
            }
            else
            {
                if (KullaniciVerisi.KonumListesiOlustur()) KonumAl();
                else MessageBox.Show(this, "Bir hatadan dolay konum listesi alınamadı. Lütfen daha sonra tekrar deneyiniz.", "0032 Konum Listesi Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Buton_KontrolEt_Click(object sender, EventArgs e)
        {
            if (KullaniciVerisi.ProfilKurtarma(Kutu_Eposta.Text, Kutu_Ad.Text, Kutu_Soyad.Text, (int)ListeKutu_Il.SelectedValue, (int)ListeKutu_Ilce.SelectedValue))
            {
                Kutu_YeniSifre.Enabled = true;
                Kutu_SifreAciklama.Enabled = true;
                Kutu_Yonlendirme.Visible = true;
                Kutu_Yonlendirme.Text = "Yeni Şifrenizi Girebilirsiniz";
                Buton_Kaydet.Enabled = true;

                Kutu_Ad.Enabled = false;
                Kutu_Soyad.Enabled = false;
                Kutu_Eposta.Enabled = false;
                ListeKutu_Il.Enabled = false;
                ListeKutu_Ilce.Enabled = false;
                Buton_KontrolEt.Enabled = false;
            }
            else
            {
                Kutu_Yonlendirme.Text = "Yanlış veya eksik bilgi girdiniz. Lütfen girilen bilgilerin eksiksiz ve doğru olduğunundan emin olunuz";
                Kutu_Yonlendirme.Visible = true;
                Kutu_YeniSifre.Enabled = false;
                Kutu_SifreAciklama.Enabled = false;
                Buton_Kaydet.Enabled = false;
            }
        }

        private void Buton_Kaydet_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection Bag = new SqlConnection(B))
                {
                    SqlCommand SifreGuncelle = new SqlCommand(SQLKomut.Guncelle(T, $"Uye_Sifre='{Kutu_YeniSifre.Text}'", $"Uye_EPosta='{Kutu_Eposta.Text}'"), Bag);
                    SqlCommand SifreGuncelleKontrol = new SqlCommand(SQLKomut.Listele(T, "Uye_Sifre", $"Uye_Sifre='{Kutu_YeniSifre.Text}' and Uye_EPosta='{Kutu_Eposta.Text}'"), Bag);

                    Bag.Open();
                    SifreGuncelle.ExecuteNonQuery();
                    SifreGuncelle.Dispose();

                    using (SqlDataReader SifreKontrol = SifreGuncelleKontrol.ExecuteReader())
                    {
                        if (SifreKontrol.HasRows)
                            MessageBox.Show("Şifreniz Başarıyla Güncellenmiştir. Artık yeni şifreniz ile giriş yapabilirsiniz.", "Şifre Güncelleme Başarılı!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else
                            MessageBox.Show("Şifreniz güncellenirken bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.", "Şifre Güncelleme Başarısız!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    SifreGuncelleKontrol.Dispose();
                    Close();
                }
            }
            catch(SqlException VT_HATA)
            {
                MessageBox.Show(VT_HATA.Message, "0031 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception HATA)
            {
                MessageBox.Show(HATA.Message, "0031 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ListeKutu_Il_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                IlceListesi.DefaultView.RowFilter = $"Bagli_IlNo = {ListeKutu_Il.SelectedValue}";
            }
            catch
            {

            }
        }
    }
}
