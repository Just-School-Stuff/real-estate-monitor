﻿/*
    * Copyright (c) 2018, MUHAMMED SAID BILGEHAN
    * All rights reserved.

    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
    * 1. Redistributions of source code must retain the above copyright
    *    notice, this list of conditions and the following disclaimer.
    * 2. Redistributions in binary form must reproduce the above copyright
    *    notice, this list of conditions and the following disclaimer in the
    *    documentation and/or other materials provided with the distribution.
    * 3. All advertising materials mentioning features or use of this software
    *    must take permission from MUHAMMED SAID BILGEHAN and must display the 
    *	   following acknowledgement:
    *    This product includes software developed by the MUHAMMED SAID BILGEHAN.
    * 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
    *    names of its contributors may be used to endorse or promote products
    *    derived from this software without specific prior written permission.

    * THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
    * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    * DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
    * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    * (INCLUDING NEGLİGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBLITY OF SUCH DAMAGE.
 */

using System;
using System.Windows.Forms;

namespace Emlak
{

    public partial class Form_AnaMenu : Form
    {
        private ProfilEkrani PE = null;

        private string Giris_Eposta;

        public Form_AnaMenu()
        {
            InitializeComponent();
        }

        private void Form_AnaMenu_Load(object sender, EventArgs e)
        {
            if (KullaniciVerisi.AdminMi())
            {
                Buton_AdminEkrani.Enabled = true;
                Buton_AdminEkrani.Visible = true;
            }
            else
            {
                Buton_AdminEkrani.Enabled = false;
                Buton_AdminEkrani.Visible = false;
            }
            Giris_Eposta = KullaniciVerisi.ProfilBilgisi('e').ToString();
            Kutu_Eposta.Text = $"{Giris_Eposta} olarak giriş yapıldı.";
        }

        private void Buton_ProfilEkrani_Click(object sender, EventArgs e)
        {
            ProfilEkraniAc(sender, e);
        }

        private void ProfilEkraniAc(object sender, EventArgs e)
        {
            PE = new ProfilEkrani();
            PE.ShowDialog(this);
            if (!KullaniciVerisi.ProfilKontrol(Giris_Eposta))
                Buton_Cikis_Click(sender, e);
        }

        private void Buton_Cikis_Click(object sender, EventArgs e)
        {
            KullaniciVerisi.ProfilTemizle();
            Close();
        }

        private void Buton_AdminEkrani_Click(object sender, EventArgs e)
        {
            AdminEkraniAc();
        }
        private void AdminEkraniAc()
        {
            AdminEkrani AE = new AdminEkrani();
            AE.ShowDialog(this);
        }
    }
}
