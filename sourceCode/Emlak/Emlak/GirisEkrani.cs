﻿/*
    * Copyright (c) 2018, MUHAMMED SAID BILGEHAN
    * All rights reserved.

    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
    * 1. Redistributions of source code must retain the above copyright
    *    notice, this list of conditions and the following disclaimer.
    * 2. Redistributions in binary form must reproduce the above copyright
    *    notice, this list of conditions and the following disclaimer in the
    *    documentation and/or other materials provided with the distribution.
    * 3. All advertising materials mentioning features or use of this software
    *    must take permission from MUHAMMED SAID BILGEHAN and must display the
    *	   following acknowledgement:
    *    This product includes software developed by the MUHAMMED SAID BILGEHAN.
    * 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
    *    names of its contributors may be used to endorse or promote products
    *    derived from this software without specific prior written permission.

    * THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
    * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    * DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
    * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    * (INCLUDING NEGLİGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBLITY OF SUCH DAMAGE.
 */

using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Emlak
{
    public partial class Form_GirisEkrani : Form
    {
        public Form_GirisEkrani()
        {
            InitializeComponent();
        }
        private Form_AnaMenu AnaMenu = null;

        private readonly string T = "Tablo_Uye";
        private readonly string TT = "Tablo_Konum";
        private string B
        {
            get
            {
                return KullaniciVerisi.Anahtar;
            }
        }
        private string varsUyeEposta;
        private string varsUyeSifre;
        private string varsKayitEposta;
        private string varsKayitSifre;
        private string varsKayitAd;
        private string varsKayitSoyad;

        private DataTable IlListesi;
        private DataTable IlceListesi;

        private bool Kontrol_Acilis = false;

        private bool Kontrol_Admin = false;

        private bool Kontrol_GirisEposta = false;
        private bool Kontrol_GirisSifre = false;

        private bool Kontrol_GirisVTEposta = false;
        private bool Kontrol_GirisVTSifre = false;

        private bool Kontrol_KayitEposta = false;
        private bool Kontrol_KayitSifre = false;
        private bool Kontrol_KayitAd = false;
        private bool Kontrol_KayitSoyad = false;
        private bool Kontrol_KayitIl = false;
        private bool Kontrol_KayitIlce = false;
        private bool Kontrol_KayitResmi = false;

        private void Form_GirisEkrani_Load(object sender, EventArgs e)
        {
            KullaniciVerisi.AnahtarOku();

            string hataTutucu = string.Empty;
            try
            {
                using (SqlConnection bag = new SqlConnection(B))
                {
                    bag.Open();
                    Kontrol_Acilis = true;
                }
            }
            catch (Exception HATA)
            {
                hataTutucu = HATA.ToString();
                //MessageBox.Show($"{HATA}", "0013 VeriTabanı Bağlantısında Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                while(hataTutucu != string.Empty)
                {
                    if (BaglantiAnahtariAl() == DialogResult.OK)
                        try
                        {
                            using (SqlConnection bag = new SqlConnection(B))
                            {
                                bag.Open();
                                hataTutucu = string.Empty;
                                Kontrol_Acilis = true;
                            }
                        }
                        catch (Exception HATA)
                        {
                            hataTutucu = HATA.ToString();
                            MessageBox.Show($"{HATA}", "0013 VeriTabanı Bağlantısında Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    else
                    {
                        Kontrol_Acilis = false;
                        break;
                    }
                }
            }
            if (Kontrol_Acilis)
            {
                KonumAl();

                varsUyeEposta = Kutu_UyeEposta.Text;
                varsUyeSifre = Kutu_UyeSifre.Text;
                varsKayitEposta = Kutu_KayitEposta.Text;
                varsKayitSifre = Kutu_KayitSifre.Text;
                varsKayitAd = Kutu_KayitAd.Text;
                varsKayitSoyad = Kutu_KayitSoyad.Text;

                Buton_GirisSekmesi.Enabled = true;
                Buton_UyeOlSekmesi.Enabled = true;
                Barindir();

                if (KullaniciVerisi.SonGirisOku())
                {
                    Kutu_UyeEposta.Text = KullaniciVerisi.ProfilBilgisi('e').ToString();
                    GirisKontrol(true);
                }
            }
            else
            {
                Dispose(true);
                Close();
            }
        }

        private char[] barindirSembol = new char[(48 - 33) + (65 - 58) + (97 - 91) + (127-123)];      // 47-33 ve 64-58 ve 97-91 ve 126-123 arasi 32
        private char[] barindirSayi = new char[(58-48)];                                                // 57-48 arasi 10 eleman
        private char[] barindirKucukHarf = new char[(91 - 65)];                                         // 90-65 arasi 26 eleman
        private char[] barindirBuyukHarf = new char[(123 - 97)];                                        // 122-97 arasi 26 eleman
        private char[] barindirma = new char[31];
        private void Barindir()
         {
            int sayacSembol = 0;
            for (int sayac = 33; sayac <= 126; sayac++)  // 33-126 arasi Ascii Table karakterleri
            {
                if ( ((33 <= sayac && sayac <= 47)) || (58 <= sayac && sayac <= 64) || (91 <= sayac && sayac <= 96) || (123 <= sayac && sayac <= 126))
                {
                    barindirSembol[sayacSembol] = (char)sayac;
                    sayacSembol++;
                }
                if (48 <= sayac && sayac <= 57) barindirSayi[sayac - 48] = (char)sayac;
                if (65 <= sayac && sayac <= 90) barindirBuyukHarf[sayac - 65] = (char)sayac;
                if (97 <= sayac && sayac <= 122) barindirKucukHarf[sayac - 97] = (char)sayac;
            }
            int sayac2 = 0; // barindirma[] Sayaci
            for (int sayac = 32; sayac < 127; sayac++)  // 32-122 arasi Ascii Table karakterleri
            {
                if (sayac != 46) // 46 = '.'
                {
                    if (sayac == 48) sayac = 58;
                    if (sayac == 64) sayac = 91;
                    if (sayac == 97) sayac = 123;
                    barindirma[sayac2] = (char)sayac;
                    sayac2++;
                }
            }
        }

        private DialogResult BaglantiAnahtariAl()
        {
            BaglantiAnahtariEkrani BAE = new BaglantiAnahtariEkrani();

            return BAE.ShowDialog(this);
        }

        private void KonumAl()
        {
            if (KullaniciVerisi.KonumListesiKontrol())
            {
                IlListesi = KullaniciVerisi.KonumListesi(true);
                ListeKutu_Il.DisplayMember = "Il";
                ListeKutu_Il.ValueMember = "IlNo";
                ListeKutu_Il.Sorted = false;
                ListeKutu_Il.DataSource = IlListesi;

                IlceListesi = KullaniciVerisi.KonumListesi(false);
                IlceListesi.DefaultView.RowFilter = $"Bagli_IlNo = {ListeKutu_Il.SelectedValue}";
                ListeKutu_Ilce.DisplayMember = "Ilce";
                ListeKutu_Ilce.ValueMember = "IlceNo";
                ListeKutu_Ilce.Sorted = false;
                ListeKutu_Ilce.DataSource = IlceListesi;
            }
            else
            {
                if(KullaniciVerisi.KonumListesiOlustur())   KonumAl();
                else MessageBox.Show(this, "Bir hatadan dolay konum listesi alınamadı. Lütfen daha sonra tekrar deneyiniz.", "0032 Konum Listesi Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //-----------------
        //  Giris Sekmesi
        //-----------------
        private void Kutu_UyeEposta_Enter(object sender, EventArgs e)
        {
            if(Kutu_UyeEposta.Text == varsUyeEposta)
            {
                Kutu_UyeEposta.Clear();
            }
        }
        private void Kutu_UyeEposta_Leave(object sender, EventArgs e)
        {
            if (Kutu_UyeEposta.Text == "")  Kutu_UyeEposta.Text = varsUyeEposta;
            GirisKontrol(true);
        }

        private void Kutu_UyeSifre_Enter(object sender, EventArgs e)
        {
            if (Kutu_UyeSifre.Text == varsUyeSifre)
            {
                Kutu_UyeSifre.Clear();
                SifreAyarla(true);
            }
        }
        private void Kutu_UyeSifre_Leave(object sender, EventArgs e)
        {
            SifreAyarla(false);
            GirisKontrol(false);
        }
        private void SifreAyarla(bool kontrol)
        {
            if (kontrol)
            {
                Kutu_UyeSifre.MaxLength = 25;
                Kutu_UyeSifre.PasswordChar = ' ';
            }
            else
            {
                if(Kutu_UyeSifre.Text == String.Empty)
                {
                    Kutu_UyeSifre.PasswordChar = '\0';
                    Kutu_UyeSifre.Text = varsUyeSifre;
                }
            }
        }

        private void GirisKontrol(bool knt)
        {
            if (knt)    // Eposta
            {
                if (
                    Kutu_UyeEposta.Text.Contains("@") &&
                    Kutu_UyeEposta.Text.Contains(".com") &&
                    (Kutu_UyeEposta.Text.IndexOfAny(barindirBuyukHarf) != -1 || Kutu_UyeEposta.Text.IndexOfAny(barindirKucukHarf) != -1) &&
                    Kutu_UyeEposta.Text.IndexOfAny(barindirma) == -1
                    )
                {
                    Kontrol_GirisEposta = true;
                    Kutu_EpostaUyariResmi.Visible = false;
                    Kutu_EpostaUyari.Text = null;
                    Kutu_EpostaUyariBilgiResmi.Visible = false;
                }
                else
                {
                    Kontrol_GirisEposta = false;
                    Kutu_EpostaUyariResmi.Visible = true;
                    Kutu_EpostaUyariBilgiResmi.Visible = true;
                    Kutu_EpostaUyari.Text = "Eposta adresinizin örnekteki gibi olduğundan emin olun: uye@email.com \n\n";
                }
            }
            else    // Sifre
            {
                if (
                    Kutu_UyeSifre.Text.Count() >= 10 &&
                    (Kutu_UyeSifre.Text.IndexOfAny(barindirBuyukHarf) != -1 || Kutu_UyeSifre.Text.IndexOfAny(barindirKucukHarf) != -1) &&
                    (Kutu_UyeSifre.Text.IndexOfAny(barindirSembol) != -1 || Kutu_UyeSifre.Text.Contains('@') || Kutu_UyeSifre.Text.Contains('.')) &&
                    Kutu_UyeSifre.Text.IndexOfAny(barindirSayi) != -1
                    )
                {
                    Kontrol_GirisSifre = true;
                    Kutu_SifreUyariResmi.Visible = false;
                    Kutu_SifreUyari.Text = null;
                    Kutu_SifreUyariBilgiResmi.Visible = false;
                }
                else
                {
                    Kontrol_GirisSifre = false;
                    Kutu_SifreUyariResmi.Visible = true;
                    Kutu_SifreUyariBilgiResmi.Visible = true;
                    Kutu_SifreUyari.Text = " Şifreniz en az 10 karakterli ve içerisinde: \n En az bir Sayi, bir Sembol ve bir karakter \n bulundurduğundan emin olun \n\n";
                }
                GirisButonuKontrol();
            }
        }

        private void Kutu_UyeEposta_TextChanged(object sender, EventArgs e)
        {
            GirisKontrol(true);
        }

        private void Kutu_UyeSifre_TextChanged(object sender, EventArgs e)
        {
            GirisKontrol(false);
        }

        private void GirisButonuKontrol()
        {
            if (Kontrol_GirisSifre && Kontrol_GirisEposta) Buton_Giris.Enabled = true;
            else
            {
                Buton_Giris.Enabled = false;
            }
        }

        private void Buton_UyeSifreGorunurluk_Click(object sender, EventArgs e)
        {
            if (Kutu_UyeSifre.PasswordChar == '\0') Kutu_UyeSifre.PasswordChar = ' ';
            else Kutu_UyeSifre.PasswordChar = '\0';
        }

        private void Buton_OtoGirisDoldur_Click(object sender, EventArgs e)
        {
            Kutu_UyeEposta.Text = "msb@email.com";
            Kutu_UyeSifre.Text = "123456.qwe";
            GirisKontrol(true);
            GirisKontrol(false);
        }

        private void Buton_Giris_Click(object sender, EventArgs e)
        {
            if (Kontrol_GirisSifre && Kontrol_GirisEposta)
            {
                try
                {
                    using (SqlConnection bag = new SqlConnection(B))
                    {
                        SqlCommand Eposta_Esle = new SqlCommand(SQLKomut.Listele(T, "Uye_EPosta", $"Uye_EPosta='{Kutu_UyeEposta.Text}'"), bag);

                        bag.Open();
                        SqlDataReader Eposta_Eslesme = Eposta_Esle.ExecuteReader();
                        Eposta_Eslesme.Read();

                        if (Eposta_Eslesme.HasRows)
                        {
                            Eposta_Eslesme.Close();
                            Eposta_Esle.Dispose();

                            Kontrol_GirisVTEposta = true;   // Eposta Eslesmesi Bulundu

                            try
                            {
                                using (bag)
                                {
                                    SqlCommand Sifre_Esle = new SqlCommand(SQLKomut.Listele(T, "Uye_Ad", $"Uye_EPosta='{Kutu_UyeEposta.Text}' and Uye_Sifre='{Kutu_UyeSifre.Text}'"), bag);

                                    SqlDataReader Sifre_Eslesme = Sifre_Esle.ExecuteReader();
                                    Sifre_Eslesme.Read();

                                    if (Sifre_Eslesme.HasRows) Kontrol_GirisVTSifre = true;   // Sifre Eslesmesi Bulundu
                                    else
                                    {
                                        Kontrol_GirisVTSifre = false;
                                        MessageBox.Show(this, "Eposta adresi veya Şifre hatalı. Lütfen bilgileri kontrol ettikten sonra tekrar deneyiniz.", "Hatalı Eposta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                    Sifre_Eslesme.Close();
                                    Sifre_Esle.Dispose();
                                }
                            }
                            catch (SqlException VT_HATA)
                            {
                                MessageBox.Show($"{VT_HATA}", "0001 VeriTabanı Bağlantısında Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Kontrol_GirisVTSifre = false;
                            }
                            catch (Exception HATA)
                            {
                                MessageBox.Show($"{HATA}", "0002 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Kontrol_GirisVTSifre = false;
                            }
                        }
                        else
                        {
                            Kontrol_GirisVTEposta = false;
                            Kontrol_GirisVTSifre = false;
                            MessageBox.Show(this, "Eposta adresi bulunamadı. Eposta adresini kontrol edebilir ya da Üye olmayı deneyebilirsiniz.", "Hatalı Eposta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                catch (SqlException VT_HATA)
                {
                    Kontrol_GirisVTEposta = false;
                    Kontrol_GirisVTSifre = false;
                    MessageBox.Show($"{VT_HATA}", "0003 VeriTabanı Bağlantısında Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception HATA)
                {
                    if (HATA.HResult == -2146233079) // EPosta bulunamadigi icin bos tabloda veri okuma hatasi
                    {
                        Kontrol_GirisVTEposta = false;
                        Kontrol_GirisVTSifre = false;
                        MessageBox.Show("Epostanız sistemde bulunmuyor, üye olmayı ya da Epostanızı kontrol etmeyi deneyebilirsiniz.", "Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        Kontrol_GirisVTEposta = false;
                        Kontrol_GirisVTSifre = false;
                        MessageBox.Show($"{HATA}", "0004 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                if (Kontrol_GirisVTEposta)
                {   // Hatali girdilerde DDoS veya hirsizlik korumasi eklenebilir.
                    if (Kontrol_GirisVTSifre)
                        AnaMenuEkrani();
                }
            }
        }

        private void AnaMenuEkrani()
        {
            using (AnaMenu = new Form_AnaMenu())
            {
                this.Hide();
                if (KullaniciVerisi.ProfilOlustur(Kutu_UyeEposta.Text))
                {
                    KullaniciVerisi.SonGirisYaz();

                    AnaMenu.ShowDialog(this);
                    KullaniciVerisi.ProfilTemizle();
                    this.Show();
                    Kutu_BaglantiYazisi.Text = "Çıkış Yapıldı";
                    Kutu_BaglantiOnayResmi.Visible = false;
                }
                else
                {
                    Kutu_BaglantiYazisi.Text = "Hata Oluştu";
                    Kutu_BaglantiOnayResmi.Visible = false;
                }
            }
        }

        //-----------------
        //  Kayit Sekmesi
        //-----------------
        private void Kutu_KayitEposta_Enter(object sender, EventArgs e)
        {
            if (Kutu_KayitEposta.Text == varsKayitEposta)
            {
                Kutu_KayitEposta.Clear();
            }
        }
        private void Kutu_KayitEposta_Leave(object sender, EventArgs e)
        {
            if (Kutu_KayitEposta.Text == "") Kutu_KayitEposta.Text = varsKayitEposta;
            else KayitKontrol(true);

        }

        private void Kutu_KayitSifre_Enter(object sender, EventArgs e)
        {
            if (Kutu_KayitSifre.Text == varsUyeSifre)
            {
                Kutu_KayitSifre.Clear();
                KayitSifreAyarla(true);
            }
        }
        private void Kutu_KayitSifre_Leave(object sender, EventArgs e)
        {
            KayitSifreAyarla(false);
            KayitKontrol(false);
        }
        private void KayitKontrol(bool knt)
        {
            if (knt)    // Eposta
            {
                if (
                    Kutu_KayitEposta.Text.Contains("@") &&
                    Kutu_KayitEposta.Text.Contains(".com") &&
                    (Kutu_KayitEposta.Text.IndexOfAny(barindirBuyukHarf) != -1 || Kutu_KayitEposta.Text.IndexOfAny(barindirKucukHarf) != -1) &&
                    Kutu_KayitEposta.Text.IndexOfAny(barindirma) == -1
                    )
                {
                    try
                    {
                        using (SqlConnection bag = new SqlConnection(B))
                        {
                            SqlCommand esle = new SqlCommand(SQLKomut.Listele(T, "Uye_EPosta", $"Uye_EPosta={Kutu_KayitEposta.Text}"), bag);

                            bag.Open();
                            SqlDataReader eslesme = esle.ExecuteReader();

                            if (eslesme.ToString() == Kutu_KayitEposta.Text)   // Eslesme Bulundu
                            {
                                Kutu_KayitEpostaResmi.Visible = true;
                            }
                        }
                    }
                    catch (SqlException VT_HATA)
                    {
                        if (VT_HATA.ErrorCode == -2146232060)   // Eslesme bulunmadi
                        {
                            Kutu_KayitEpostaOnayResmi.Visible = true;
                        }
                        else
                        {
                            MessageBox.Show($"{VT_HATA}", "0005 VeriTabanı Hatası Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception HATA) { MessageBox.Show($"{HATA}", "0006 VeriTabanı Bağlantısında Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error); }

                    Kontrol_KayitEposta = true;
                    Kutu_KayitUyarisi.Text = String.Empty;
                }
                else
                {
                    Kontrol_KayitEposta = false;
                    Kutu_KayitEpostaOnayResmi.Visible = false;
                    Kutu_KayitUyarisi.Text = "Eposta adresinizin örnekteki gibi olduğundan emin olun: uye@email.com \n\n";
                }
            }
            else    // Sifre
            {
                if (
                    Kutu_KayitSifre.Text.Count() >= 10 &&
                    (Kutu_KayitSifre.Text.IndexOfAny(barindirBuyukHarf) != -1 || Kutu_KayitSifre.Text.IndexOfAny(barindirKucukHarf) != -1) &&
                    (Kutu_KayitSifre.Text.IndexOfAny(barindirSembol) != -1 || Kutu_KayitSifre.Text.Contains('@') || Kutu_KayitSifre.Text.Contains('.')) &&
                    Kutu_KayitSifre.Text.IndexOfAny(barindirSayi) != -1
                    )
                {
                    Kontrol_KayitSifre = true;
                    Kutu_KayitSifreOnayResmi.Visible = true;
                    Kutu_KayitUyarisi.Text = String.Empty;
                }
                else
                {
                    Kontrol_KayitSifre = false;
                    Kutu_KayitSifreOnayResmi.Visible = false;
                    Kutu_KayitUyarisi.Text = "Şifreniz en az 10 karakterli ve içerisinde: \n En az bir Sayi, bir Sembol ve bir karakter \n bulundurduğundan emin olun \n\n";
                }
            }
        }

        private void Buton_KayitSifreGorunurluk_Click(object sender, EventArgs e)
        {
            if (Kutu_KayitSifre.PasswordChar == '\0') Kutu_KayitSifre.PasswordChar = ' ';
            else Kutu_KayitSifre.PasswordChar = '\0';
        }
        private void KayitSifreAyarla(bool kontrol)
        {
            if (kontrol)
            {
                Kutu_KayitSifre.MaxLength = 25;
                Kutu_KayitSifre.PasswordChar = ' ';
            }
            else
            {
                if(Kutu_KayitSifre.Text == String.Empty)
                {
                    Kutu_KayitSifre.PasswordChar = '\0';
                    Kutu_KayitSifre.Text = varsKayitSifre;
                }
            }
        }
        private void Kutu_KayitAd_Enter(object sender, EventArgs e)
        {
            if (Kutu_KayitAd.Text == varsKayitAd) Kutu_KayitAd.Text = String.Empty;
        }
        private void Kutu_KayitAd_Leave(object sender, EventArgs e)
        {
            if (Kutu_KayitAd.Text == String.Empty)
            {
                Kutu_KayitAd.Text = varsKayitAd;
                Kontrol_KayitAd = false;
                Kutu_KayitAdOnayResmi.Visible = false;
                Kutu_KayitUyarisi.Text = "Ad Kutusunu Doldurmalısınız";
            }
            else
            {
                Kutu_KayitUyarisi.Text = String.Empty;
                Kutu_KayitAdOnayResmi.Visible = true;
                Kontrol_KayitAd = true;
            }
        }

        private void Kutu_KayitSoyad_Enter(object sender, EventArgs e)
        {
            if (Kutu_KayitSoyad.Text == varsKayitSoyad) Kutu_KayitSoyad.Text = String.Empty;
        }
        private void Kutu_KayitSoyad_Leave(object sender, EventArgs e)
        {
            if (Kutu_KayitSoyad.Text == String.Empty)
            {
                Kutu_KayitSoyad.Text = varsKayitSoyad;
                Kontrol_KayitSoyad = false;
                Kutu_KayitSoyadOnayResmi.Visible = false;
                Kutu_KayitUyarisi.Text = "Soyad Kutusunu Doldurmalısınız";
            }
            else
            {
                Kutu_KayitUyarisi.Text = String.Empty;
                Kutu_KayitSoyadOnayResmi.Visible = true;
                Kontrol_KayitSoyad = true;
            }
        }

        private void Buton_KayitResmiSec_Click(object sender, EventArgs e)
        {
            OpenFileDialog KayitResmiSec = new OpenFileDialog();
            string ResimYolu;

            KayitResmiSec.Multiselect = false;
            KayitResmiSec.Title = "Üyelik Resmi Seç";
            KayitResmiSec.AddExtension = false;
            KayitResmiSec.DereferenceLinks = true;
            KayitResmiSec.Filter = "Resim Dosyası|*.png;*.jpg;*.jpeg;";

            DialogResult ResimSecimi = KayitResmiSec.ShowDialog(this);

            if(ResimSecimi == DialogResult.OK)
            {
                ResimYolu = KayitResmiSec.FileName;
                try
                {
                    Kutu_KayitResmi.ImageLocation = ResimYolu;
                    Kutu_KayitResmi.Load();
                    Kontrol_KayitResmi = true;
                    Kutu_KayitResmiOnayResmi.Visible = true;
                }
                catch (IOException OY_HATA)
                {
                    Kutu_KayitResmiOnayResmi.Visible = false;
                    MessageBox.Show(OY_HATA.ToString(), "0007 Dosya Okuma Yazma Hatası Oluştu!",MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception HATA)
                {
                    Kutu_KayitResmiOnayResmi.Visible = false;
                    MessageBox.Show(HATA.ToString(), "0008 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                Kutu_KayitResmiOnayResmi.Visible = false;
            }
        }

        private void Buton_UyeOl_Click(object sender, EventArgs e)
        {
            if(Kontrol_KayitEposta && Kontrol_KayitSifre && Kontrol_KayitAd && Kontrol_KayitSoyad && Kontrol_KayitIl && Kontrol_KayitIlce && Kontrol_KayitResmi)
            {
                try
                {
                    using (SqlConnection bag = new SqlConnection(B))
                    {
                        using(FileStream KayitResmiDosyasi = new FileStream(Kutu_KayitResmi.ImageLocation, FileMode.Open, FileAccess.Read))
                        {
                            using (BinaryReader KayitResmiBinary = new BinaryReader(KayitResmiDosyasi))
                            {
                                byte[] KayitResmi = KayitResmiBinary.ReadBytes((int)KayitResmiDosyasi.Length);

                                SqlCommand Uye_Ekle = new SqlCommand(SQLKomut.Ekle(T, Kontrol_Admin, Kutu_KayitEposta.Text, Kutu_KayitSifre.Text, Kutu_KayitAd.Text, Kutu_KayitSoyad.Text), bag);
                                Uye_Ekle.Parameters.Add(new SqlParameter("@RESIM", KayitResmi));

                                SqlCommand Konum_Ekle = new SqlCommand(SQLKomut.Ekle(TT, Kutu_KayitEposta.Text, (int)ListeKutu_Il.SelectedValue, (int)ListeKutu_Ilce.SelectedValue), bag);

                                bag.Open();
                                Uye_Ekle.ExecuteNonQuery();
                                Konum_Ekle.ExecuteNonQuery();

                                Uye_Ekle.Dispose();
                                Konum_Ekle.Dispose();

                                KayitResmiBinary.Close();
                            }
                            KayitResmiDosyasi.Close();
                        }


                        DialogResult cevap = MessageBox.Show("Üyelik işlemi başarıyla tamamlandı! Üye Giriş Sekmesinden giriş yapabilirsiniz.","Üyelik İşlemi Tamamlandı!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (cevap == DialogResult.OK) { Buton_GirisSekmesi_Click(sender, e); }
                        Kutu_UyeEposta.Text = Kutu_KayitEposta.Text;
                    }
                }
                catch (SqlException VT_HATA)
                {
                    if(VT_HATA.ErrorCode == -2146232060)
                    {
                        MessageBox.Show($"Eposta adresiniz zaten kayıtlı gözüküyor. Şifrenizi hatırlamıyorsanız Profil kurtarma sayfamızdan yeni şifre edinebilirsiniz.", "Kayıtlı Eposta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                        MessageBox.Show($"{VT_HATA}", "0009 VeriTabanı Bağlantısında Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception HATA) { MessageBox.Show($"{HATA}", "0010 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error); }

            }
            else
            {
                Kutu_KayitUyarisi.Text = "Eksik veya Hatalı girdi var, lütfen bilgilerinizi tekrar kontrol ediniz.";
            }
        }

        private void Buton_OtoKayitDoldur_Click(object sender, EventArgs e)
        {
            Kutu_KayitEposta.Text = "msb@email.com";
            Kutu_KayitSifre.Text = "123456.qwe";
            Kutu_KayitAd.Text = "Said";
            Kutu_KayitSoyad.Text = "Bilg";
            ListeKutu_Il.SelectedValue = 34;
            IlceListesi.DefaultView.RowFilter = $"Bagli_IlNo = {ListeKutu_Il.SelectedValue}";
            ListeKutu_Ilce.SelectedIndex = 11;

            Kutu_KayitResmi.ImageLocation = "C:\\Users\\msaid\\Desktop\\İkonlar\\Resimler\\trash-black-ikon.png";
            Kutu_KayitResmi.Load();

            Kontrol_KayitEposta = true;
            Kontrol_KayitSifre = true;
            Kontrol_KayitAd = true;
            Kontrol_KayitSoyad = true;
            Kontrol_KayitIl = true;
            Kontrol_KayitIlce = true;
            Kontrol_KayitResmi = true;
            Kontrol_Admin = true;
        }

        private void Buton_UyeOlSekmesi_Click(object sender, EventArgs e)
        {
            girisSekmeleri.SelectTab(1);
        }

        private void Buton_GirisSekmesi_Click(object sender, EventArgs e)
        {
            girisSekmeleri.SelectTab(0);
        }

        private void Buton_UyeKontrol_Click(object sender, EventArgs e)
        {
            if (Kontrol_KayitEposta)
            {
                try
                {
                    using (SqlConnection bag = new SqlConnection(B) )
                    {
                        SqlCommand bul = new SqlCommand(SQLKomut.Listele(T, "Uye_Ad, Uye_SoyAD, Uye_Resim", $"Uye_EPosta='{Kutu_KayitEposta.Text}'"), bag);

                        bag.Open();
                        SqlDataReader sonuc = bul.ExecuteReader();

                        sonuc.Read();

                        Kutu_KayitSifre.Text = ":P";
                        Kutu_KayitSifre.PasswordChar = ' ';

                        Kutu_KayitAd.Text = sonuc.GetString(0);

                        Kutu_KayitSoyad.Text = sonuc.GetString(1);

                        Kutu_KayitResmi.Image = Image.FromStream( sonuc.GetStream(2) );
                        Kutu_KayitEpostaResmi.Visible = false;
                    }
                }
                catch (SqlException VT_HATA) { MessageBox.Show(VT_HATA.ToString(), "0011 HATA OLUŞTU!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                catch (Exception HATA) { MessageBox.Show(HATA.ToString(), "0012 HATA OLUŞTU!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            else
            {
                Kutu_KayitEpostaResmi.Visible = true;
            }
        }

        private void Buton_ProfilKurtar_Click(object sender, EventArgs e)
        {
            ProfilKurtarmaEkrani PK = new ProfilKurtarmaEkrani();
            PK.ShowDialog(this);
        }

        private void Tik_Admin_CheckedChanged(object sender, EventArgs e)
        {
            if(Tik_Admin.CheckState == CheckState.Checked)
            {
                Kontrol_Admin = true;
            }
            else
            {
                Kontrol_Admin = false;
            }
        }

        private void ListeKutu_Il_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                IlceListesi.DefaultView.RowFilter = $"Bagli_IlNo = {ListeKutu_Il.SelectedValue}";
                Kontrol_KayitIl = true;
                Kutu_KayitIlOnayResmi.Visible = true;
            }
            catch
            {

            }
        }

        private void ListeKutu_Ilce_SelectedIndexChanged(object sender, EventArgs e)
        {
            Kontrol_KayitIlce = true;
            Kutu_KayitIlceOnayResmi.Visible = true;
        }
    }
}
