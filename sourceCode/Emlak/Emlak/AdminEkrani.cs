﻿/*
    * Copyright (c) 2018, MUHAMMED SAID BILGEHAN
    * All rights reserved.

    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
    * 1. Redistributions of source code must retain the above copyright
    *    notice, this list of conditions and the following disclaimer.
    * 2. Redistributions in binary form must reproduce the above copyright
    *    notice, this list of conditions and the following disclaimer in the
    *    documentation and/or other materials provided with the distribution.
    * 3. All advertising materials mentioning features or use of this software
    *    must take permission from MUHAMMED SAID BILGEHAN and must display the 
    *	   following acknowledgement:
    *    This product includes software developed by the MUHAMMED SAID BILGEHAN.
    * 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
    *    names of its contributors may be used to endorse or promote products
    *    derived from this software without specific prior written permission.

    * THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
    * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    * DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
    * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    * (INCLUDING NEGLİGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBLITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace Emlak
{
    public struct GeciciResim
    {
        public string Eposta;
        public byte[] Resim;

        public GeciciResim(string EPOSTA, byte[] RESIM)
        {
            Eposta = EPOSTA;
            Resim = RESIM;
        }
    }
    public struct GeciciKonum
    {
        public string Eposta;
        public int Il;
        public int Ilce;

        public GeciciKonum(string EPOSTA, int IL, int ILCE)
        {
            Eposta = EPOSTA;
            Il = IL;
            Ilce = ILCE;
        }
    }

    public partial class AdminEkrani : Form
    {
        private SqlConnection Bag = new SqlConnection(KullaniciVerisi.Anahtar);
        private string T = "Tablo_Uye";
        private string TT = "Tablo_Konum";

        private SqlDataAdapter Uye_Adaptoru = new SqlDataAdapter();
        private SqlDataAdapter Konum_Adaptoru = new SqlDataAdapter();
        private DataSet VeriTablolari = new DataSet();
        private BindingSource BaglayiciKaynak = new BindingSource();
        private SqlCommandBuilder UyeKomutOlusturucu = new SqlCommandBuilder();
        private SqlCommandBuilder KonumKomutOlusturucu = new SqlCommandBuilder();

        private SqlCommand Listele_Uye;
        private SqlCommand Listele_Konum;

        private DataTable IlListesi;
        private DataTable IlceListesi;
        private DataTable YeniUyeIlListesi;
        private DataTable YeniUyeIlceListesi;
        private DataTable IlAramaListesi;
        private DataTable IlceAramaListesi;

        private List<GeciciResim> GResim = new List<GeciciResim>();
        private List<GeciciKonum> GKonum = new List<GeciciKonum>();
        private List<int> SilinenListesi = new List<int>();

        string YeniUyeResimDosyasi;

        private bool Kontrol_ResimDegisti = false;
        private bool Kontrol_UyeBilgileriDegisti = false;
        private bool Kontrol_IlDegisti = false;
        private bool Kontrol_IlceDegisti = false;

        private bool Kontrol_YeniUyeEposta = false;
        private bool Kontrol_YeniUyeSifre = false;
        private bool Kontrol_YeniUyeAd = false;
        private bool Kontrol_YeniUyeSoyad = false;
        private bool Kontrol_YeniUyeResim = false;
        private bool Kontrol_YeniUyeIl = false;
        private bool Kontrol_YeniUyeIlce = false;
        private bool Kontrol_YeniUyeAdmin = false;

        private int? varsIl = null;
        private int? varsIlce = null;

        public AdminEkrani()
        {
            InitializeComponent();
        }

        private void AdminEkrani_Load(object sender, EventArgs e)
        {
            KonumAl();
            ListeKutu_Il.SelectedIndex = 1;
            ListeKutu_Ilce.SelectedIndex = 1;
            
            YeniUyeKonumAl();
            ListeKutu_YeniUyeIl.SelectedIndex = 1;
            ListeKutu_YeniUyeIlce.SelectedIndex = 1;


            VeriTablolari.Tables.Add("UyeListesi");
            VeriTablolari.Tables.Add("KonumListesi");

            try
            {
                Bag.Open();

                Listele_Uye = new SqlCommand(SQLKomut.Listele(T) + " order by Uye_Sira", Bag);
                Listele_Konum = new SqlCommand(SQLKomut.Listele(TT) + " order by Uye_Sira", Bag);

                Uye_Adaptoru.SelectCommand = Listele_Uye;
                Konum_Adaptoru.SelectCommand = Listele_Konum;

                UyeKomutOlusturucu = new SqlCommandBuilder(Uye_Adaptoru);
                UyeKomutOlusturucu.SetAllValues = false;
                UyeKomutOlusturucu.GetUpdateCommand(true);
                UyeKomutOlusturucu.GetInsertCommand(true);
                UyeKomutOlusturucu.GetDeleteCommand(true);


                KonumKomutOlusturucu = new SqlCommandBuilder(Konum_Adaptoru);
                KonumKomutOlusturucu.SetAllValues = false;
                KonumKomutOlusturucu.GetUpdateCommand(true);
                KonumKomutOlusturucu.GetInsertCommand(true);
                KonumKomutOlusturucu.GetDeleteCommand(true);

                Uye_Adaptoru.Fill(VeriTablolari, "UyeListesi");
                Konum_Adaptoru.Fill(VeriTablolari, "KonumListesi");

                VeriTablolari.Tables["UyeListesi"].Columns[3].ColumnMapping = MappingType.Hidden;   // Resim
                VeriTablolari.Tables["UyeListesi"].Columns[6].ColumnMapping = MappingType.Hidden;   // Resim

                /*
                VeriTablolari.Tables["KonumListesi"].Columns[0].ColumnMapping = MappingType.Hidden; // Sira
                VeriTablolari.Tables["KonumListesi"].Columns[1].ColumnMapping = MappingType.Hidden;   // Eposta
                */

                BaglayiciKaynak.DataSource = VeriTablolari.Tables["UyeListesi"];
                VeriTablosu_VT.DataSource = BaglayiciKaynak;

                ListeKutu_Il.SelectedValue = VeriTablolari.Tables["KonumListesi"].Rows[VeriTablosu_VT.SelectedRows[0].Index].Field<int>(2);
                ListeKutu_Ilce.SelectedValue = VeriTablolari.Tables["KonumListesi"].Rows[VeriTablosu_VT.SelectedRows[0].Index].Field<int>(3);

                VeriTablosu_VT.SelectedRows[0].Selected = true;
                VeriTablosu_VT_SelectionChanged(sender, e);

                ListeKutu_Il_SelectedIndexChanged(sender, e);
                ListeKutu_Ilce_SelectedValueChanged(sender, e);

                Kontrol_IlDegisti = false;
                Kontrol_IlceDegisti = false;
                Kontrol_YeniUyeIl = false;
                Kontrol_YeniUyeIlce = false;

                Kutu_Cikti.Text = "Bağlanti Başarılı!";
                VeriTablosu_VT.Columns["Uye_Sira"].ReadOnly = true;
            }
            catch (SqlException VT_HATA)
            {
                MessageBox.Show(VT_HATA.Message, "0036 VeriTabanı Hatası Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Kutu_Cikti.Text = "0036 VeriTabanı Hatası Oluştu";
            }
            catch (Exception HATA)
            {
                MessageBox.Show(HATA.Message, "0037 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Kutu_Cikti.Text = "0037 Hata Oluştu";
            }
            finally
            {
                if (Bag.State == ConnectionState.Open) Bag.Close();
            }
        }

        private void VeriTablosu_VT_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            ResimAl();
            EskiKonumAl();
        }

        private void ResimKutusu_SecilenUye_MouseEnter(object sender, EventArgs e)
        {
            ResimKutusu_SecilenUye.Cursor = Cursors.Hand;
        }

        private void ResimKutusu_SecilenUye_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog ResimSec = new OpenFileDialog())
                {
                    ResimSec.Filter = "Resim Dosyaları|*.jpg;*.jpeg;*.png";
                    ResimSec.AddExtension = false;
                    ResimSec.Multiselect = false;

                    if (ResimSec.ShowDialog(this) == DialogResult.OK)
                    {
                        GeciciResim Secilen = new GeciciResim(VeriTablolari.Tables["UyeListesi"].Rows[VeriTablosu_VT.SelectedRows[0].Index].ItemArray[2].ToString(), File.ReadAllBytes(ResimSec.FileName));

                        int sayac = 0;
                        foreach (GeciciResim karsilastir in GResim)
                        {
                            if (karsilastir.Eposta == Secilen.Eposta)
                            {
                                GResim[sayac] = Secilen;
                                break;
                            }
                            else sayac++;
                        }

                        if (sayac == GResim.Count)
                            GResim.Add(Secilen);

                        MemoryStream ResimDosyasi = new MemoryStream(File.ReadAllBytes(ResimSec.FileName));
                        ResimKutusu_SecilenUye.Image = Image.FromStream(ResimDosyasi);
                        Kontrol_ResimDegisti = true;
                    }
                }
            }
            catch (Exception HATA)
            {
                MessageBox.Show(this, HATA.Message, "Resim Seçimi Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void VeriTablosu_VT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (VeriTablolari.HasChanges())
            {
                Kontrol_UyeBilgileriDegisti = true;
            }
            else
            {
                Kontrol_UyeBilgileriDegisti = false;
            }
        }

        private void AdminEkrani_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DegisiklikKontrol())
            {
                if (MessageBox.Show("Bazı değişiklikler yaptığınızı görüyoruz. Kaydetmeden çıkmak istediğinize emin misiniz?", "Kaydetmek istemez misiniz?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }

        private void Buton_Düzenle_Click(object sender, EventArgs e)
        {
            if (VeriTablosu_VT.ReadOnly)
            {
                VeriTablosu_VT.ReadOnly = false;
                VeriTablosu_VT.RowTemplate.ReadOnly = false;
                VeriTablosu_VT.AllowUserToAddRows = true;
                VeriTablosu_VT.AllowUserToDeleteRows = true;
                ResimKutusu_SecilenUye.Enabled = true;
                ListeKutu_Il.Enabled = true;
                ListeKutu_Ilce.Enabled = true;
                Buton_Sil.Enabled = true;
                Kutu_Cikti.Text = "Düzenleme Modu Aktif!";

            }
            else
            {
                VeriTablosu_VT.ReadOnly = true;
                VeriTablosu_VT.RowTemplate.ReadOnly = true;
                VeriTablosu_VT.AllowUserToAddRows = false;
                VeriTablosu_VT.AllowUserToDeleteRows = false;
                ResimKutusu_SecilenUye.Enabled = false;
                ListeKutu_Il.Enabled = false;
                ListeKutu_Ilce.Enabled = false;
                Buton_Sil.Enabled = false;
                Kutu_Cikti.Text = "Düzenleme Modu Pasif";
            }
            VeriTablosu_VT.Columns["Uye_Sira"].ReadOnly = true;
        }

        private void Buton_Kaydet_Click(object sender, EventArgs e)
        {
            int satir = 0;
            try
            {
                satir = VeriTablosu_VT.SelectedRows[0].Index;
            }
            catch
            {
                satir = 0;
            }
            if (DegisiklikKontrol() || YeniUyeEkleKontrol())
            {
                DataGridViewDataErrorContexts VeriTablosuHata = new DataGridViewDataErrorContexts();

                VeriTablosu_VT.EndEdit(VeriTablosuHata);
                BaglayiciKaynak.EndEdit();

                if (VeriTablosuHata == 0)
                {
                    SqlCommand GuvenlikKapat = new SqlCommand(SQLKomut.FKOzellikSil(TT, T), Bag);
                    SqlCommand GuvenlikAc = new SqlCommand(SQLKomut.FKOzellikEkle(TT, T, "Uye_Eposta", "Uye_EPosta"), Bag);
                    try
                    {
                        Bag.Open();
                        try
                        {
                            GuvenlikKapat.ExecuteNonQuery();
                        }
                        catch (Exception HATA) { }
                        Bag.Close();

                        EpostaKontrol();
                        KDegisiklikKaydet();
                        UyeSil();

                        Bag.Open();
                        try
                        {
                            Uye_Adaptoru.Update(VeriTablolari, "UyeListesi");
                            Konum_Adaptoru.Update(VeriTablolari, "KonumListesi");
                        }
                        catch (Exception HATA) { }
                        Bag.Close();

                        Bag.Open();
                        try
                        {
                            GuvenlikAc.ExecuteNonQuery();
                        }
                        catch (Exception HATA) { }
                        Bag.Close();

                        GuvenlikKapat.Dispose();
                        GuvenlikAc.Dispose();
                        Kutu_Cikti.Text = "Kaydetme İşlemi Başarılı!";

                    }
                    catch (SqlException VT_HATA)
                    {
                        MessageBox.Show(VT_HATA.Message, "0038 VeriTabanı Hatası Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Kutu_Cikti.Text = "0038 VeriTabanı Hatası Oluştu!";
                    }
                    catch (Exception HATA)
                    {
                        MessageBox.Show(HATA.Message, "0039 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Kutu_Cikti.Text = "0039 Hata Oluştu!";
                    }
                    finally
                    {
                        if (Kontrol_ResimDegisti)
                        {
                            try
                            {
                                if (Bag.State == ConnectionState.Closed) Bag.Open();

                                foreach (GeciciResim gecici in GResim)
                                {
                                    SqlCommand ResimGuncelle = new SqlCommand(SQLKomut.ResimGuncelle(T, $"Uye_EPosta='{gecici.Eposta}'", true), Bag);
                                    ResimGuncelle.Parameters.Add(new SqlParameter("@RESIM", gecici.Resim));

                                    ResimGuncelle.ExecuteNonQuery();
                                }
                                GResim.Clear();
                            }
                            catch (SqlException VT_HATA)
                            {
                                MessageBox.Show(VT_HATA.Message, "0042 VeriTabanı Hatası Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Kutu_Cikti.Text = "0042 VeriTabanı Hatası Oluştu!";
                            }
                            catch (Exception HATA)
                            {
                                MessageBox.Show(HATA.Message, "0043 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Kutu_Cikti.Text = "0043 Hata Oluştu!";
                            }
                        }
                        GKonum.Clear();
                        SilinenListesi.Clear();
                        try
                        {
                            GuvenlikKapat.ExecuteNonQuery();
                        }
                        catch { }
                        try
                        {
                            GuvenlikAc.ExecuteNonQuery();
                        }
                        catch { }

                        Buton_Guncelle_Click(sender, e);
                        if (Bag.State == ConnectionState.Open) Bag.Close();

                        try
                        {
                            VeriTablosu_VT.Rows[satir].Selected = true;
                        }
                        catch { }
                        ResimAl();
                        Kontrolsifirla();
                    }
                }
                else
                {
                    MessageBox.Show(this, VeriTablosuHata.ToString(), "Eşitleme Hatası Meydana Geldi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Buton_Guncelle_Click(object sender, EventArgs e)
        {
            VeriTablolari.Clear();

            Uye_Adaptoru.Fill(VeriTablolari, "UyeListesi");
            Konum_Adaptoru.Fill(VeriTablolari, "KonumListesi");

            ListeKutu_Il.SelectedValue = VeriTablolari.Tables["KonumListesi"].Rows[VeriTablosu_VT.SelectedRows[0].Index].Field<int>(2);
            ListeKutu_Ilce.SelectedValue = VeriTablolari.Tables["KonumListesi"].Rows[VeriTablosu_VT.SelectedRows[0].Index].Field<int>(3);

            ResimAl();
            Kontrolsifirla();
            GResim.Clear();
            GKonum.Clear();
            SilinenListesi.Clear();
            Kutu_VTICikti.Text = "Güncelleme Başarılı!";
        }

        private void ListeKutu_Il_SelectedIndexChanged(object sender, EventArgs e)
        {
            varsKonumAl();
            try
            {
                string eposta = VeriTablosu_VT.SelectedRows[0].Cells["Uye_EPosta"].Value.ToString();
                int? il = (int?)ListeKutu_Il.SelectedValue;

                IlceListesi.DefaultView.RowFilter = $"Bagli_IlNo = {ListeKutu_Il.SelectedValue}";
                if (varsIl != il && varsIl != null && il != null)
                {
                    ListeKutu_Ilce.SelectedIndex = 1;
                    int? ilce = (int?)ListeKutu_Ilce.SelectedValue;
                    if(ilce != null)
                    {
                        Kontrol_IlDegisti = true;
                        ListeyeKonumEkle(eposta, (int)il, (int)ilce);
                    }
                    else
                        Kontrol_IlDegisti = false;
                }
                else
                    Kontrol_IlDegisti = false;
            }
            catch { }
        }
        private void ListeKutu_Ilce_SelectedValueChanged(object sender, EventArgs e)
        {
            varsKonumAl();
            try
            {
                string eposta = VeriTablosu_VT.SelectedRows[0].Cells["Uye_EPosta"].Value.ToString();
                int? il = (int?)ListeKutu_Il.SelectedValue;
                int? ilce = (int?)ListeKutu_Ilce.SelectedValue;

                if (varsIlce != ilce && varsIlce != null && il != null && ilce != null)
                {
                    Kontrol_IlceDegisti = true;
                    ListeyeKonumEkle(eposta, (int)il, (int)ilce);
                }
                else
                    Kontrol_IlceDegisti = false;
            }
            catch { }
        }

        private void Buton_Sil_Click(object sender, EventArgs e)
        {
            if (Tik_SoruSor.Checked)
                if (MessageBox.Show(this, "Seçili Üyeyi SİLMEK istediğinize emin misiniz?", "Üye Silme İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    UyeSilHazirlik();
                else
                    Kutu_Cikti.Text = "Üye Silme İşlemi İptal Edildi.";
            else
                UyeSilHazirlik();
        }

        private void VeriTablosu_VT_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                ListeKutu_Il.SelectedValue = VeriTablolari.Tables["KonumListesi"].Rows[VeriTablosu_VT.SelectedRows[0].Index].Field<int>(2);
                ListeKutu_Ilce.SelectedValue = VeriTablolari.Tables["KonumListesi"].Rows[VeriTablosu_VT.SelectedRows[0].Index].Field<int>(3);
                varsKonumAl();
                EskiKonumAl();
            }
            catch
            {
                ListeKutu_Il.SelectedIndex = 1;
                ListeKutu_Ilce.SelectedIndex = 1;
            }
        }

        private void ResimKutusu_YeniUyeResim_MouseEnter(object sender, EventArgs e)
        {
            ResimKutusu_YeniUyeResim.Cursor = Cursors.Hand;
        }
        private void ResimKutusu_YeniUyeResim_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog ResimSec = new OpenFileDialog())
                {
                    ResimSec.Filter = "Resim Dosyaları|*.jpg;*.jpeg;*.png";
                    ResimSec.AddExtension = false;
                    ResimSec.Multiselect = false;

                    if (ResimSec.ShowDialog(this) == DialogResult.OK)
                    {
                        YeniUyeResimDosyasi = ResimSec.FileName;
                        Image ResimDosyasi = Image.FromStream(new MemoryStream(File.ReadAllBytes(YeniUyeResimDosyasi)));

                        ResimKutusu_YeniUyeResim.Image = ResimDosyasi;

                        Kontrol_YeniUyeResim = true;
                    }
                    else
                        Kontrol_YeniUyeResim = false;
                }
            }
            catch
            {
                Kontrol_YeniUyeResim = false;
                ResimKutusu_YeniUyeResim.Image = ResimKutusu_SecilenUye.ErrorImage;
            }
        }
    
        private void Buton_YeniUyeEkle_Click(object sender, EventArgs e)
        {
            if (YeniUyeEkleKontrol())
            {
                try
                {
                    SqlCommand EpostaKontrol = new SqlCommand(SQLKomut.Listele(T, "Uye_EPosta", $"Uye_EPosta='{Kutu_YeniUyeEposta.Text}'"), Bag);
                    Bag.Open();
                    SqlDataReader Eslesme = EpostaKontrol.ExecuteReader();
                    if (!Eslesme.HasRows)
                    {
                        Bag.Close();

                        object[] YeniUyeBilgileri = new object[7];
                        YeniUyeBilgileri[0] = Kontrol_YeniUyeAdmin;
                        YeniUyeBilgileri[1] = null;
                        YeniUyeBilgileri[2] = Kutu_YeniUyeEposta.Text;
                        YeniUyeBilgileri[3] = Kutu_YeniUyeSifre.Text;
                        YeniUyeBilgileri[4] = Kutu_YeniUyeAd.Text;
                        YeniUyeBilgileri[5] = Kutu_YeniUyeSoyad.Text;
                        YeniUyeBilgileri[6] = File.ReadAllBytes(YeniUyeResimDosyasi);

                        object[] YeniUyeKonumBilgileri = new object[4];
                        YeniUyeKonumBilgileri[0] = null;
                        YeniUyeKonumBilgileri[1] = YeniUyeBilgileri[2];
                        YeniUyeKonumBilgileri[2] = ListeKutu_YeniUyeIl.SelectedValue;
                        YeniUyeKonumBilgileri[3] = ListeKutu_YeniUyeIlce.SelectedValue;

                        DataRow YeniUye = VeriTablolari.Tables["UyeListesi"].NewRow();
                        YeniUye.ItemArray = YeniUyeBilgileri;

                        DataRow YeniUyeKonumu = VeriTablolari.Tables["KonumListesi"].NewRow();
                        YeniUyeKonumu.ItemArray = YeniUyeKonumBilgileri;

                        VeriTablolari.Tables["UyeListesi"].Rows.Add(YeniUye);
                        VeriTablolari.Tables["KonumListesi"].Rows.Add(YeniUyeKonumu);
                        
                        Kutu_VTICikti.Text = "Yeni Üye Eklendi!";
                    }
                    else
                        Kutu_YeniUyeEposta.BackColor = Color.Red;
                }
                catch (SqlException VT_HATA)
                {
                    MessageBox.Show(VT_HATA.Message, "0044 VeriTabanı Hatası Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Kutu_Cikti.Text = "0044 VeriTabanı Hatası Oluştu!";
                }
                catch (Exception HATA)
                {
                    MessageBox.Show(HATA.Message, "0045 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Kutu_Cikti.Text = "0045 Hata Oluştu!";
                }
                finally
                {
                    if (Bag.State == ConnectionState.Open) Bag.Close();
                }
            }
        }

        private void Kutu_YeniUyeEposta_TextChanged(object sender, EventArgs e)
        {
            if (Kutu_YeniUyeEposta.Text != string.Empty)
                Kontrol_YeniUyeEposta = true;
            else
                Kontrol_YeniUyeEposta = false;
        }

        private void Kutu_YeniUyeSifre_TextChanged(object sender, EventArgs e)
        {
            if (Kutu_YeniUyeSifre.Text != string.Empty)
                Kontrol_YeniUyeSifre = true;
            else
                Kontrol_YeniUyeSifre = false;
        }

        private void Kutu_YeniUyeAd_TextChanged(object sender, EventArgs e)
        {
            if (Kutu_YeniUyeAd.Text != string.Empty)
                Kontrol_YeniUyeAd = true;
            else
                Kontrol_YeniUyeAd = false;
        }

        private void Kutu_YeniUyeSoyad_TextChanged(object sender, EventArgs e)
        {
            if (Kutu_YeniUyeSoyad.Text != string.Empty)
                Kontrol_YeniUyeSoyad = true;
            else
                Kontrol_YeniUyeSoyad = false;
        }

        private void ListeKutu_YeniUyeIl_SelectedValueChanged(object sender, EventArgs e)
        {
            Kontrol_YeniUyeIl = true;
            try
            {
                YeniUyeIlceListesi.DefaultView.RowFilter = $"Bagli_IlNo = {ListeKutu_YeniUyeIl.SelectedValue}";
            }
            catch { }
        }

        private void ListeKutu_YeniUyeIlce_SelectedValueChanged(object sender, EventArgs e)
        {
            Kontrol_YeniUyeIlce = true;
        }

        private void Tik_YeniUyeAdminMi_CheckedChanged(object sender, EventArgs e)
        {
            if (Tik_YeniUyeAdminMi.Checked) Kontrol_YeniUyeAdmin = true;
            else Kontrol_YeniUyeAdmin = false;
        }
        
        private void Kutu_EpostaArama_TextChanged(object sender, EventArgs e)
        {
            VeriTablolari.Tables["UyeListesi"].DefaultView.RowFilter = string.Format($"Uye_EPosta LIKE '%{Kutu_EpostaArama.Text}%'");
        }

        private void Kutu_AdArama_TextChanged(object sender, EventArgs e)
        {
            VeriTablolari.Tables["UyeListesi"].DefaultView.RowFilter = string.Format($"Uye_Ad LIKE '%{Kutu_AdArama.Text}%'");
        }

        private void Kutu_SoyadArama_TextChanged(object sender, EventArgs e)
        {
            VeriTablolari.Tables["UyeListesi"].DefaultView.RowFilter = string.Format($"Uye_Soyad LIKE '%{Kutu_SoyadArama.Text}%'");
        }

        private void EpostaKontrol()
        {
            try
            {
                for (int sayac = 0; sayac < VeriTablolari.Tables["KonumListesi"].Rows.Count; sayac++)
                {
                    bool silinmisMi = false;
                    foreach (int Silinen in SilinenListesi)
                    {
                        if (Silinen == sayac)
                        {
                            silinmisMi = true;
                            break;
                        }
                    }
                    if (!silinmisMi)
                    {
                        VeriTablolari.Tables["KonumListesi"].Rows[sayac].SetField(1, VeriTablolari.Tables["UyeListesi"].Rows[sayac].Field<string>(2));
                    }

                }
            }
            catch
            {

            }
        }
        private void KDegisiklikKaydet()
        {
            try
            {
                for (int sayac = 0; sayac < VeriTablolari.Tables["KonumListesi"].Rows.Count; sayac++)
                {
                    bool silinmisMi = false;
                    foreach(int Silinen in SilinenListesi)
                    {
                        if (Silinen == sayac)
                        {
                            silinmisMi = true;
                            break;
                        }
                    }
                    if (!silinmisMi)
                    {
                        string UyeEposta = VeriTablolari.Tables["KonumListesi"].Rows[sayac].Field<string>(1);
                        foreach (GeciciKonum karsilastir in GKonum)
                        {
                            if (karsilastir.Eposta == UyeEposta)
                            {
                                VeriTablolari.Tables["KonumListesi"].Rows[sayac].SetField(2, karsilastir.Il);
                                VeriTablolari.Tables["KonumListesi"].Rows[sayac].SetField(3, karsilastir.Ilce);
                            }
                        }
                    }
                }
            }
            catch { }
            
        }
        private void UyeSilHazirlik()
        {
            try
            {
                int SilinecekSira = VeriTablosu_VT.SelectedRows[0].Index;
                SilinenListesi.Add(SilinecekSira);

                KDegisiklikKaydet();

                object[] UyeTemizle = new object[7];
                object[] KonumTemizle = new object[4];
                byte[] TemizResim = new byte[0];

                UyeTemizle[0] = false;
                UyeTemizle[1] = -1;
                UyeTemizle[2] = VeriTablolari.Tables["UyeListesi"].Rows[SilinecekSira].ItemArray[2];
                UyeTemizle[3] = string.Empty;
                UyeTemizle[4] = "SİLİNECEK";
                UyeTemizle[5] = "SİLİNECEK";
                UyeTemizle[6] = TemizResim;

                KonumTemizle[0] = -1;
                KonumTemizle[1] = VeriTablolari.Tables["KonumListesi"].Rows[SilinecekSira].ItemArray[1];
                KonumTemizle[2] = -1;
                KonumTemizle[3] = -1;

                VeriTablolari.Tables["UyeListesi"].Rows[SilinecekSira].ItemArray = UyeTemizle;
                VeriTablolari.Tables["KonumListesi"].Rows[SilinecekSira].ItemArray = KonumTemizle;

                Kutu_Cikti.Text = "Üye Silme İşlemi Başarılı!";
            }
            catch (SqlException VT_HATA)
            {
                MessageBox.Show(VT_HATA.Message, "0040 VeriTabanı Hatası Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Kutu_Cikti.Text = "0040 VeriTabanı Hatası Oluştu!";
            }
            catch (Exception HATA)
            {
                MessageBox.Show(HATA.Message, "0041 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Kutu_Cikti.Text = "0041 Hata Oluştu!";
            }
            finally
            {
                if (Bag.State == ConnectionState.Open) Bag.Close();
            }
        }
        private void UyeSil()
        {
            try
            {
                foreach(int SilinecekSira in SilinenListesi)
                {
                    VeriTablolari.Tables["KonumListesi"].Rows[SilinecekSira].Delete();
                    VeriTablolari.Tables["UyeListesi"].Rows[SilinecekSira].Delete();
                }
            }
            catch (SqlException VT_HATA)
            {
                MessageBox.Show(VT_HATA.Message, "0040 VeriTabanı Hatası Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Kutu_Cikti.Text = "0040 VeriTabanı Hatası Oluştu!";
            }
            catch (Exception HATA)
            {
                MessageBox.Show(HATA.Message, "0041 Hata Oluştu!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Kutu_Cikti.Text = "0041 Hata Oluştu!";
            }
            finally
            {
                if (Bag.State == ConnectionState.Open) Bag.Close();
            }
        }
        private void Kontrolsifirla()
        {
            Kontrol_IlceDegisti = false;
            Kontrol_IlDegisti = false;
            Kontrol_ResimDegisti = false;
            Kontrol_UyeBilgileriDegisti = false;
        }
        private bool ListeyeKonumEkle(string Eposta, int Il, int Ilce)
        {
            int sayac = 0;
            foreach (GeciciKonum konum in GKonum)
            {
                if (konum.Eposta == Eposta) break;
                sayac++;
            }

            if (sayac == GKonum.Count)
            {
                GKonum.Add(new GeciciKonum(Eposta, Il, Ilce));
                return true;
            }
            else
            {
                GeciciKonum BilgiGuncelle = new GeciciKonum(Eposta, Il, Ilce);
                GKonum[sayac] = BilgiGuncelle;
                return false;
            }
        }
        private void EskiKonumAl()
        {
            try
            {
                int SecilenUye = VeriTablosu_VT.SelectedCells[0].RowIndex;
                int sayac = 0;
                string SecilenEposta = VeriTablosu_VT.SelectedCells[2].Value.ToString();

                foreach (GeciciKonum karsilastir in GKonum)
                {
                    if (karsilastir.Eposta == SecilenEposta)
                    {
                        ListeKutu_Il.SelectedValue = karsilastir.Il;
                        ListeKutu_Ilce.SelectedValue = karsilastir.Ilce;
                        break;
                    }
                    else sayac++;
                }
            }
            catch (Exception HATA) { }
        }
        private void ResimAl()
        {
            try
            {
                int.TryParse(VeriTablosu_VT.SelectedCells[1].Value.ToString(), out int SecilenUye);
                int sayac = 0;
                string SecilenEposta = VeriTablolari.Tables["UyeListesi"].Rows[VeriTablosu_VT.SelectedRows[0].Index].ItemArray[2].ToString();


                foreach (GeciciResim karsilastir in GResim)
                {
                    if (karsilastir.Eposta == SecilenEposta)
                    {
                        ResimKutusu_SecilenUye.Image = Image.FromStream(new MemoryStream(karsilastir.Resim));
                        break;
                    }
                    else sayac++;
                }
                if (sayac == GResim.Count)
                    ResimKutusu_SecilenUye.Image = Image.FromStream(new MemoryStream((byte[])VeriTablolari.Tables["UyeListesi"].Select($"Uye_Sira={SecilenUye}", "Uye_Sira")[0].ItemArray[6]));

            }
            catch (Exception HATA) { ResimKutusu_SecilenUye.Image = ResimKutusu_SecilenUye.ErrorImage; }
        }
        private void varsKonumAl()
        {
            try
            {
                varsIl = VeriTablolari.Tables["KonumListesi"].Rows[VeriTablosu_VT.SelectedRows[0].Index].Field<int>(2);
                varsIlce = VeriTablolari.Tables["KonumListesi"].Rows[VeriTablosu_VT.SelectedRows[0].Index].Field<int>(3);
            }
            catch
            {
                varsIl = null;
                varsIlce = null;
            }
        }
        private void KonumAl()
        {
            if (KullaniciVerisi.KonumListesiKontrol())
            {
                IlListesi = KullaniciVerisi.KonumListesi(true);
                ListeKutu_Il.DisplayMember = "Il";
                ListeKutu_Il.ValueMember = "IlNo";
                ListeKutu_Il.Sorted = false;
                ListeKutu_Il.DataSource = IlListesi;

                IlceListesi = KullaniciVerisi.KonumListesi(false);
                IlceListesi.DefaultView.RowFilter = $"Bagli_IlNo = {ListeKutu_Il.SelectedValue}";
                ListeKutu_Ilce.DisplayMember = "Ilce";
                ListeKutu_Ilce.ValueMember = "IlceNo";
                ListeKutu_Ilce.Sorted = false;
                ListeKutu_Ilce.DataSource = IlceListesi;
            }
            else
            {
                if (KullaniciVerisi.KonumListesiOlustur()) KonumAl();
                else MessageBox.Show(this, "Bir hatadan dolay konum listesi alınamadı. Lütfen daha sonra tekrar deneyiniz.", "0032 Konum Listesi Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private bool DegisiklikKontrol()
        {
            if (Kontrol_ResimDegisti || Kontrol_UyeBilgileriDegisti || Kontrol_IlDegisti || Kontrol_IlceDegisti || VeriTablolari.HasChanges())
                return true;
            else
                return false;
        }
        private bool YeniUyeEkleKontrol()
        {
            if (!Kontrol_YeniUyeEposta)
                Kutu_YeniUyeEposta.BackColor = Color.Red;
            else
                Kutu_YeniUyeEposta.BackColor = Color.White;

            if (!Kontrol_YeniUyeSifre)
                Kutu_YeniUyeSifre.BackColor = Color.Red;
            else
                Kutu_YeniUyeSifre.BackColor = Color.White;

            if (!Kontrol_YeniUyeAd)
                Kutu_YeniUyeAd.BackColor = Color.Red;
            else
                Kutu_YeniUyeAd.BackColor = Color.White;

            if (!Kontrol_YeniUyeSoyad)
                Kutu_YeniUyeSoyad.BackColor = Color.Red;
            else
                Kutu_YeniUyeSoyad.BackColor = Color.White;

            if (!Kontrol_YeniUyeResim)
                ResimKutusu_YeniUyeResim.BackColor = Color.Red;
            else
                ResimKutusu_YeniUyeResim.BackColor = Color.White;

            if (!Kontrol_YeniUyeIl)
                ListeKutu_YeniUyeIl.BackColor = Color.Red;
            else
                ListeKutu_YeniUyeIl.BackColor = Color.White;

            if (!Kontrol_YeniUyeIlce)
                ListeKutu_YeniUyeIlce.BackColor = Color.Red;
            else
                ListeKutu_YeniUyeIlce.BackColor = Color.White;
            if (Kontrol_YeniUyeEposta && Kontrol_YeniUyeSifre && Kontrol_YeniUyeAd && Kontrol_YeniUyeSoyad && Kontrol_YeniUyeResim && Kontrol_YeniUyeIl && Kontrol_YeniUyeIlce)
            {
                return true;
            }
            else
                return false;
        }
        private void YeniUyeKonumAl()
        {
            if (KullaniciVerisi.KonumListesiKontrol())
            {
                YeniUyeIlListesi = KullaniciVerisi.KonumListesi(true).Copy();
                ListeKutu_YeniUyeIl.DisplayMember = "Il";
                ListeKutu_YeniUyeIl.ValueMember = "IlNo";
                ListeKutu_YeniUyeIl.Sorted = false;
                ListeKutu_YeniUyeIl.DataSource = YeniUyeIlListesi;

                YeniUyeIlceListesi = KullaniciVerisi.KonumListesi(false).Copy();
                YeniUyeIlceListesi.DefaultView.RowFilter = $"Bagli_IlNo = {ListeKutu_YeniUyeIl.SelectedValue}";
                ListeKutu_YeniUyeIlce.DisplayMember = "Ilce";
                ListeKutu_YeniUyeIlce.ValueMember = "IlceNo";
                ListeKutu_YeniUyeIlce.Sorted = false;
                ListeKutu_YeniUyeIlce.DataSource = YeniUyeIlceListesi;
            }
            else
            {
                if (KullaniciVerisi.KonumListesiOlustur()) YeniUyeKonumAl();
                else MessageBox.Show(this, "Bir hatadan dolay konum listesi alınamadı. Lütfen daha sonra tekrar deneyiniz.", "0032 Konum Listesi Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Buton_AramaFiltreleriniTemizle_Click(object sender, EventArgs e)
        {
            Kutu_EpostaArama.Text = string.Empty;
            Kutu_AdArama.Text = string.Empty;
            Kutu_SoyadArama.Text = string.Empty;
        }
    }
}
